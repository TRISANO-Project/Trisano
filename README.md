TriSano®
========

*TriSano® Community Edition* is a freely downloadable open source
product. Highly configurable and comprehensive, it provides case and
outbreak management, surveillance and analytics for global public
health. Open Source developers are amongst the most experienced in the
industry and open source products are setting new industry standards.

TriSano was originally developed by the Collaborative Software Foundation.

## Contact 

*Commonwealth Informatics*
*info@commoninf.com*

## Contributors

See CONTRIBUTORS file.

## License

See COPYING file.

NOTE: In order to comply with the license, you must maintain the Source link in the footer of the
application via the config.yml setting or another mechanism. If you maintain a derivative
work of TriSano®, you must update the Source link to point to the derivative work.
If the instance of TriSano® running the derivative work is not publicly available, you must
maintain a web page that points to the current source code that is publicly available on the
internet. Failure to comply may result in termination of your license per the terms of the license.

### License and Copyright Information regarding Third-Party Software

#### Dependent

TriSano® is dependent on packages known to be compatible with the TriSano® license (See COPYING file).

<http://www.gnu.org/philosophy/license-list.html#GPLCompatibleLicenses>

#### Non-Dependent (Optional - not distributed with TriSano®)

Pentaho BI Platform and supporting Tools: See <http://www.pentaho.com/license/>
