ALTER TABLE addresses ADD COLUMN percent_gis_match INTEGER DEFAULT NULL

DROP TRIGGER ensure_lat_long ON public.addresses;
  
-- Function: public.ensure_lat_long()

CREATE OR REPLACE FUNCTION public.ensure_lat_long()
  RETURNS trigger AS
$BODY$
    import urllib
    import urllib2
    import json

    new = TD["new"]
    #if we are blacklisting this address, dont attempt to GIS it
    if(new["latitude"] == -1 and new["longitude"] == -1):
        return "MODIFY"
    
    new["latitude"]=None
    new["longitude"]=None
    new["percent_gis_match"] = 0
    try:
        url=("https://gisitin.tarrantcounty.com/WSGIS/api/Parcel911?Address="
            + urllib.quote_plus(new["street_number"] + " " + new["street_name"] 
            + " " + new["city"] + " TX " + new["postal_code"]))
        results = urllib2.urlopen(url).read()
        js = json.loads(results)
        new["percent_gis_match"] = int(js['results'][0]['candidateScore'])
        if(js['results'][0]['candidateScore'] >= 85.0):
            new["latitude"] = js['results'][0]['lat']
            new["longitude"] = js['results'][0]['lon']
    except Exception as e:
        return "MODIFY"
    return "MODIFY"    
$BODY$
  LANGUAGE plpythonu VOLATILE
  COST 100;
ALTER FUNCTION public.ensure_lat_long()
  OWNER TO trisano_rails;

CREATE TRIGGER ensure_lat_long
  BEFORE INSERT OR UPDATE
  ON public.addresses
  FOR EACH ROW
  EXECUTE PROCEDURE public.ensure_lat_long();

-- Function: public.blacklist_address_gis()
CREATE OR REPLACE FUNCTION public.blacklist_address_gis(address_id integer)
  RETURNS void AS
$BODY$
    BEGIN
        UPDATE public.addresses 
            SET latitude=-1.0, longitude=-1.0
        WHERE id=address_id;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.blacklist_address_gis(integer)
  OWNER TO trisano_rails;
