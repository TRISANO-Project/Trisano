class AddGisPercentAddresses < ActiveRecord::Migration
  def self.up
    add_column :addresses, :percent_gis_match, :integer, :default => null
  end

  def self.down
    remove_column :addresses, :percent_gis_match
  end
end
