class AddNbsToPerson < ActiveRecord::Migration
  def self.up
    add_column :people, :nbs_id, :string
  end

  def self.down
    remove_column :people, :nbs_id
  end
end
