class IncreaseGeneratePdfMappingsMatchValueSize < ActiveRecord::Migration
  def self.up
  
    # increase column size to 1M
    change_table(:generate_pdf_mappings) do |t|
      change_col_length('generate_pdf_mappings', 'match_value', 1048576)
    end
    
  end

  def self.down
    change_table(:generate_pdf_mappings) do |t|
      change_col_length('generate_pdf_mappings', 'match_value', 255)
    end
  end

  
  def self.change_col_length(table, column, length=0)
    col_tmp = column+"_tmp"
    change_table(table) do |t|
      t.string col_tmp, :limit => length
    end
	execute "UPDATE #{table} SET #{col_tmp} = #{column}"
    change_table(table) do |t|
      t.remove column
    end
    rename_column table, col_tmp, column
  end
end  
