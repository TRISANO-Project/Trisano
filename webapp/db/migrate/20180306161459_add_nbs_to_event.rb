class AddNbsToEvent < ActiveRecord::Migration
  def self.up
    add_column :events, :nbs_id, :string
  end

  def self.down
    remove_column :events, :nbs_id
  end
end
