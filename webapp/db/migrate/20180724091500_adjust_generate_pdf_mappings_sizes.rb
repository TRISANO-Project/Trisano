class AdjustGeneratePdfMappingsSizes < ActiveRecord::Migration
  def self.up
  
    change_table(:generate_pdf_mappings) do |t|
      change_col_length('generate_pdf_mappings', 'match_value', 255)
      change_col_length('generate_pdf_mappings', 'form_field_name', 1048576)
    end
    
  end

  def self.down
    change_table(:generate_pdf_mappings) do |t|
      change_col_length('generate_pdf_mappings', 'match_value', 1048576)
      change_col_length('generate_pdf_mappings', 'form_field_name', 255)
    end
  end

  
  def self.change_col_length(table, column, length=0)
    col_tmp = column+"_tmp"
    change_table(table) do |t|
      t.string col_tmp, :limit => length
    end
	execute "UPDATE #{table} SET #{col_tmp} = #{column}"
    change_table(table) do |t|
      t.remove column
    end
    rename_column table, col_tmp, column
  end
end  
