class PdfMappings < ActiveRecord::Migration 
  def self.up 
    create_table :pdf_mappings do |t| 
      t.string :template_pdf_name, :null => false 
      t.string :display_name, :null => false
      t.string :disease_name_regex, :null => false
    end 
    add_index :pdf_mappings, :template_pdf_name 
  end 
  def self.down 
    drop_table :pdf_mappings 
  end 
end 
