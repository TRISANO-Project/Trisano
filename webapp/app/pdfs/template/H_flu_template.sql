--If you are replacing all the mappings for this disease, you must first delete the existing mapping.
--This is commented out so as not be run inadvertently
--clear existing
--DELETE FROM generate_pdf_mappings WHERE template_pdf_name='H_flu';

--new
--event id
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('H_flu', 'NBS_EID', 'sql lookup', 'SELECT nbs_id FROM events WHERE id=<%=event_id%>', NULL, NULL, NULL, NULL);
--event status
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu','Confirmed', 'core', 'lhd_case_status_id', 'case', NULL, 'check_box', 'C');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu','Probable', 'core', 'lhd_case_status_id', 'case', NULL, 'check_box', 'P');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu','Not_a_case', 'core', 'lhd_case_status_id', 'case', NULL, 'check_box', 'NC');
--reporter details
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'reported_by', 'sql lookup', 
    'SELECT pe.first_name || '' '' || pe.last_name FROM events e '
      'INNER JOIN participations p ON p.event_id = e.id AND p.type = ''Reporter'' '
      'INNER JOIN people pe ON pe.entity_id = p.secondary_entity_id '
      'WHERE e.id = <%=event_id%> '
      'ORDER BY p.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'reporting_agency', 'core', 'reporting_agency|place_entity|place|name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'rep_ac', 'core', 'reporting_agency|place_entity|telephones|area_code', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'rep_phone', 'core', 'reporting_agency|place_entity|telephones|phone_number', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'rept_to_clin_dt', 'core', 'results_reported_to_clinician_date', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'Reported_to', 'core', 'investigator|best_name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'rept_to_ph_dt', 'core', 'first_reported_PH_date', NULL, NULL, NULL, NULL);

--person details
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('H_flu', 'NBS_PID', 'sql lookup', 
    'SELECT pe.nbs_id FROM people pe INNER JOIN participations pa ON pa.type = ''InterestedParty'' AND pa.event_id=<%=event_id%> AND pe.entity_id = pa.primary_entity_id', 
    NULL, NULL, NULL, NULL);

INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu','FirstName', 'core','interested_party|person_entity|person|first_name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu','LastName', 'core','interested_party|person_entity|person|last_name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu','dob', 'core','interested_party|person_entity|person|birth_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('H_flu', 'Age', 'sql lookup', 
    'SELECT CASE '
    '    WHEN pe.birth_date IS NOT NULL THEN date_part(''year'', age(pe.birth_date)) '
    '    WHEN pe.approximate_age_no_birthday IS NOT NULL AND pe.age_type_id=173 THEN pe.approximate_age_no_birthday '
    '    ELSE NULL '
    '  END '
    '  FROM participations p '
    '    INNER JOIN people pe ON pe.entity_id = p.primary_entity_id '
    '  WHERE p.type=''InterestedParty'' AND p.event_id=<%=event_id%> ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', NULL, 'core','interested_party|person_entity|person|first_name', NULL,' ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu','NamePage2', 'core','interested_party|person_entity|person|last_name', NULL, NULL, NULL, NULL);
  
--gender
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu','Female', 'core', 'interested_party|person_entity|person|birth_gender_id', 'gender', NULL, 'check_box', 'F');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu','Male', 'core', 'interested_party|person_entity|person|birth_gender_id', 'gender', NULL, 'check_box', 'M');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu','Unknown', 'core', 'interested_party|person_entity|person|birth_gender_id', 'gender', NULL, 'check_box', 'UNK');
--address
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', NULL, 'core','address|street_number', NULL, ' ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', NULL, 'core','address|street_name', NULL, ' #', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu','P_addr', 'core','address|unit_number', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu','P_city','core', 'address|city', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu','P_zip','core', 'address|postal_code', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu','P_county', 'sql lookup', 'select code_description from addresses t0 join external_codes t1 on t1.id=t0.county_id where t0.event_id=<%=event_id%>', 
  NULL, NULL, NULL, NULL);
--phone
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu','P_ac', 'core','interested_party|person_entity|telephones|area_code', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', NULL, 'core','interested_party|person_entity|telephones|phone_number', NULL, ' x', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu','P_phone', 'core','interested_party|person_entity|telephones|extension', NULL, NULL, NULL, NULL);
--parent
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu','parent_guardian', 'core','parent_guardian', NULL, NULL, NULL, NULL);

--race
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'race_w', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'W');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'race_b', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'B');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'race_a', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'A');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'race_h', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'H');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'race_ai_an', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'AI_AN');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'race_u', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'UNK');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'race_o', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'Other');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'race_other', 'adddemog', 'otherracespecify', NULL, NULL, NULL, NULL);
  
--ethnicity
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'ethnicity', 'core', 'interested_party|person_entity|person|ethnicity_id', 'ethnicity', NULL, NULL, NULL);

--clinicians
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', NULL, 'core', 'clinicians|person_entity|person|first_name', NULL, ' ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', NULL, 'core', 'clinicians|person_entity|person|middle_name', NULL, ' ', NULL, NULL);
 INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu','MD_name', 'core', 'clinicians|person_entity|person|last_name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu','MD_ac', 'core','clinicians|person_entity|telephones|area_code', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', NULL, 'core','clinicians|person_entity|telephones|phone_number', NULL, ' x', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu','MD_phone', 'core','clinicians|person_entity|telephones|extension', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', NULL, 'core', 'diagnostic_facilities|place_entity|addresses|street_number', NULL, ' ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', NULL, 'core', 'diagnostic_facilities|place_entity|addresses|street_name', NULL, ' #', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu','MD_addr', 'core', 'diagnostic_facilities|place_entity|addresses|unit_number', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', NULL, 'core', 'diagnostic_facilities|place_entity|addresses|city', NULL, ',', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', NULL, 'core', 'diagnostic_facilities|place_entity|addresses|state_id', 'state', ' ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu','MD_city_state_zip', 'core', 'diagnostic_facilities|place_entity|addresses|postal_code', NULL, NULL, NULL, NULL);

--Clinical Data
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'case_onset_date', 'core', 'disease_event|disease_onset_date', NULL, NULL, 'date_m_d_y', NULL);

INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'infect_pb', 'HFluClinical', 'InfectionType', NULL, NULL, 'check_box', 'Primary Bacteremia');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'infect_me', 'HFluClinical', 'InfectionType', NULL, NULL, 'check_box', 'Meningitis');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'infect_om', 'HFluClinical', 'InfectionType', NULL, NULL, 'check_box', 'Otitis Media');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'infect_pn', 'HFluClinical', 'InfectionType', NULL, NULL, 'check_box', 'Pneumonia');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'infect_ep', 'HFluClinical', 'InfectionType', NULL, NULL, 'check_box', 'Epiglottitis');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'infect_pe', 'HFluClinical', 'InfectionType', NULL, NULL, 'check_box', 'Peritonitis');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'infect_sa', 'HFluClinical', 'InfectionType', NULL, NULL, 'check_box', 'Septic Arthritis');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'infect_ot', 'HFluClinical', 'InfectionType', NULL, NULL, 'check_box', 'Other');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'other_infection', 'HFluClinical', 'other_description', NULL, NULL, NULL, NULL);

--hospitalization
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hospitalized', 'core','disease_event|hospitalized_id','yesno', NULL,'check_box','Y');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu','hosp_name', 'sql lookup',
  'select t1.name from (select secondary_entity_id from participations where event_id=<%=event_id%> AND type=''HospitalizationFacility'' order by id asc limit 1) t0 '
  'join places t1 on t0.secondary_entity_id=t1.entity_id', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'admitted', 'core', 'hospitalization_facilities|hospitals_participation|admission_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'discharged', 'core', 'hospitalization_facilities|hospitals_participation|discharge_date', NULL, NULL, 'date_m_d_y', NULL);

-- Patient died?
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'died', 'core', 'disease_event|died_id', 'yesno', NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu','date_died', 'core','interested_party|person_entity|person|date_of_death', NULL, NULL, 'date_m_d_y', NULL);

--Laboratory data
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'serotype', 'haemophilus_influenzae', 'SEROTYPE', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'lab_pcr', 'sql lookup', 
      'SELECT ''On'' FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND ctt.common_name=''PCR/Amplification'' ORDER BY lr.id DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'lab_culture', 'sql lookup', 
      'SELECT ''On'' FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND ctt.common_name=''Culture'' ORDER BY lr.id DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'lab_antigen_csf', 'sql lookup', 
      'SELECT ''On'' FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN external_codes ec ON ec.id = lr.specimen_source_id AND ec.code_name=''specimen'' AND ec.the_code LIKE ''CSF%'' '
        'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND ctt.common_name ILIKE ''%antigen%'' ORDER BY lr.id DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'lab_spec_bl', 'sql lookup', 
      'SELECT ''On'' FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN external_codes ec ON ec.id = lr.specimen_source_id AND ec.code_name=''specimen'' AND ec.code_description ILIKE ''%blood%'' ORDER BY lr.id DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'lab_spec_pleural', 'sql lookup', 
      'SELECT ''On'' FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN external_codes ec ON ec.id = lr.specimen_source_id AND ec.code_name=''specimen'' AND ec.code_description ILIKE ''%lung%'' ORDER BY lr.id DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'lab_spec_placenta', 'sql lookup', 
      'SELECT ''On'' FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN external_codes ec ON ec.id = lr.specimen_source_id AND ec.code_name=''specimen'' AND ec.code_description ILIKE ''%placenta%'' ORDER BY lr.id DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'lab_spec_pericardial', 'sql lookup', 
      'SELECT ''On'' FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN external_codes ec ON ec.id = lr.specimen_source_id AND ec.code_name=''specimen'' AND ec.code_description ILIKE ''%pericardial%'' ORDER BY lr.id DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'lab_spec_csf', 'sql lookup', 
      'SELECT ''On'' FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN external_codes ec ON ec.id = lr.specimen_source_id AND ec.code_name=''specimen'' AND ec.code_description ILIKE ''%csf%'' ORDER BY lr.id DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'lab_spec_peritoneal', 'sql lookup', 
      'SELECT ''On'' FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN external_codes ec ON ec.id = lr.specimen_source_id AND ec.code_name=''specimen'' AND ec.code_description ILIKE ''%peritoneal%'' ORDER BY lr.id DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'lab_spec_jo', 'sql lookup', 
      'SELECT ''On'' FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN external_codes ec ON ec.id = lr.specimen_source_id AND ec.code_name=''specimen'' AND ec.code_description ILIKE ''%joint%'' ORDER BY lr.id DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);


--Vaccination History
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'no_vacc', 'immuniz_status', 'Reason_never_vaccinated', NULL, NULL, NULL, NULL);

INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hib_1', 'immuniz_status', 'vaccine_name', NULL, NULL, 'replace,0', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'vacc_type_1', 'immuniz_status', 'vaccine_type', NULL, NULL, 'replace,0', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'vacc_man_1', 'immuniz_status', 'vaccine_manuf', NULL, NULL, 'replace,0', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'vacc_lot_1', 'immuniz_status', 'lot_number', NULL, NULL, 'replace,0', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hib_2', 'immuniz_status', 'vaccine_name', NULL, NULL, 'replace,1', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'vacc_type_2', 'immuniz_status', 'vaccine_type', NULL, NULL, 'replace,1', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'vacc_man_2', 'immuniz_status', 'vaccine_manuf', NULL, NULL, 'replace,1', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'vacc_lot_2', 'immuniz_status', 'lot_number', NULL, NULL, 'replace,1', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hib_3', 'immuniz_status', 'vaccine_name', NULL, NULL, 'replace,2', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'vacc_type_3', 'immuniz_status', 'vaccine_type', NULL, NULL, 'replace,2', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'vacc_man_3', 'immuniz_status', 'vaccine_manuf', NULL, NULL, 'replace,2', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'vacc_lot_3', 'immuniz_status', 'lot_number', NULL, NULL, 'replace,2', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hib_4', 'immuniz_status', 'vaccine_name', NULL, NULL, 'replace,3', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'vacc_type_4', 'immuniz_status', 'vaccine_type', NULL, NULL, 'replace,3', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'vacc_man_4', 'immuniz_status', 'vaccine_manuf', NULL, NULL, 'replace,3', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'vacc_lot_4', 'immuniz_status', 'lot_number', NULL, NULL, 'replace,3', NULL);


--Household contacts
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_control', 'pertussis', 'control_activities', NULL, NULL, NULL, NULL); 
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_name_1', 'sql lookup', 
    'SELECT concat(pe.first_name, '' '', pe.last_name) AS full_name FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 0'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_relation_1', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 0'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''pat_relationship'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_age_1', 'sql lookup', 
    'SELECT GREATEST(EXTRACT(YEAR FROM age(cast(pe.birth_date as date))), pe.approximate_age_no_birthday) AS age_years FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 0'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_hx_1', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 0'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''up_to_date_vax'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_hx_1', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 0'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''describe_other'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_sym_date_1', 'sql lookup', 
    'SELECT ''cough/'' || a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 0'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''cough_onset_date'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_prop_date_1', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 0'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''Prophylaxis'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_name_2', 'sql lookup', 
    'SELECT concat(pe.first_name, '' '', pe.last_name) AS full_name FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 1'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_relation_2', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 1'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''pat_relationship'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_age_2', 'sql lookup', 
    'SELECT GREATEST(EXTRACT(YEAR FROM age(cast(pe.birth_date as date))), pe.approximate_age_no_birthday) AS age_years FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 1'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_hx_2', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 1'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''up_to_date_vax'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_hx_2', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 1'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''describe_other'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_sym_date_2', 'sql lookup', 
    'SELECT ''cough/'' || a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 1'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''cough_onset_date'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_prop_date_2', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 1'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''Prophylaxis'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_name_3', 'sql lookup', 
    'SELECT concat(pe.first_name, '' '', pe.last_name) AS full_name FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 2'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_relation_3', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 2'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''pat_relationship'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_age_3', 'sql lookup', 
    'SELECT GREATEST(EXTRACT(YEAR FROM age(cast(pe.birth_date as date))), pe.approximate_age_no_birthday) AS age_years FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 2'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_hx_3', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 2'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''up_to_date_vax'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_hx_3', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 2'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''describe_other'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_sym_date_3', 'sql lookup', 
    'SELECT ''cough/'' || a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 2'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''cough_onset_date'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_prop_date_3', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 2'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''Prophylaxis'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_name_4', 'sql lookup', 
    'SELECT concat(pe.first_name, '' '', pe.last_name) AS full_name FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 3'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_relation_4', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 3'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''pat_relationship'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_age_4', 'sql lookup', 
    'SELECT GREATEST(EXTRACT(YEAR FROM age(cast(pe.birth_date as date))), pe.approximate_age_no_birthday) AS age_years FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 3'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_hx_4', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 3'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''up_to_date_vax'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_hx_4', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 3'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''describe_other'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_sym_date_4', 'sql lookup', 
    'SELECT ''cough/'' || a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 3'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''cough_onset_date'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_prop_date_4', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 3'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''Prophylaxis'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_name_5', 'sql lookup', 
    'SELECT concat(pe.first_name, '' '', pe.last_name) AS full_name FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 4'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_relation_5', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 4'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''pat_relationship'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_age_5', 'sql lookup', 
    'SELECT GREATEST(EXTRACT(YEAR FROM age(cast(pe.birth_date as date))), pe.approximate_age_no_birthday) AS age_years FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 4'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_hx_5', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 4'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''up_to_date_vax'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_hx_5', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 4'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''describe_other'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_sym_date_5', 'sql lookup', 
    'SELECT ''cough/'' || a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 4'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''cough_onset_date'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'hhc_prop_date_5', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 4'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''Prophylaxis'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);

-- comments AND notes
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'comments_notes', 'sql lookup', 
    'SELECT to_char(pe.encounter_date, ''YYYY-MM-DD''), pe.description FROM participations_encounters pe '
      'INNER JOIN events e ON e.type=''EncounterEvent'' AND e.parent_id=<%=event_id%> AND pe.id = e.participations_encounter_id '
      'ORDER BY pe.encounter_date',
    NULL, '\n', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'comments_notes', 'sql lookup', 
    'SELECT to_char(created_at, ''YYYY-MM-DD''), note FROM notes WHERE note_type=''clinical'' AND struckthrough != TRUE AND event_id=<%=event_id%> ORDER BY id',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'date_init', 'core', 'investigation_started_date', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'date_completed', 'core', 'investigation_completed_LHD_date', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'date_reported_dshs', 'core', '', NULL, NULL, 'date_now', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('H_flu', 'name_investigator', 'core', 'investigator|best_name', NULL, NULL, NULL, NULL);





