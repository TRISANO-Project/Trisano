--If you are replacing all the mappings for this disease, you must first delete the existing mapping.
--This is commented out so as not be run inadvertently
--clear existing
--DELETE FROM generate_pdf_mappings WHERE template_pdf_name='Shigella_additional';

--patient information
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Shigella_additional', 'case_name', 'core','interested_party|person_entity|person|last_name', NULL, ', ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Shigella_additional','case_name', 'core','interested_party|person_entity|person|first_name', NULL, NULL, NULL, NULL);

--additional questions
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Shigella_additional', 'any_sex', 'Shigella_Additional_Questions', 'sex_yn', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Shigella_additional', 'touch_anus', 'Shigella_Additional_Questions', 'analcontact', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Shigella_additional', 'pg_male', 'Shigella_Additional_Questions', 'partnergender', NULL, NULL,  'check_box', 'Male');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Shigella_additional', 'pg_female', 'Shigella_Additional_Questions', 'partnergender', NULL, NULL,  'check_box', 'Female');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Shigella_additional', 'pg_trans', 'Shigella_Additional_Questions', 'partnergender', NULL, NULL,  'check_box', 'Transgender');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Shigella_additional', 'pg_refused', 'Shigella_Additional_Questions', 'partnergender', NULL, NULL,  'check_box', 'Refused to answer');
