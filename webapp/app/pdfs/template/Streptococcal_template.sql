﻿--If you are replacing all the mappings for this disease, you must first delete the existing mapping.
--This is commented out so as not be run inadvertently
--clear existing
--DELETE FROM generate_pdf_mappings WHERE template_pdf_name='Streptococcal';


--patient/disease information
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Streptococcal', 'disease_name', 'sql lookup', 
    'SELECT d.disease_name FROM diseases d '
    '  INNER JOIN events e ON e.id=<%=event_id%> '
    '  INNER JOIN disease_events de ON de.disease_id = d.id AND de.event_id=e.id ', 
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Streptococcal', 'nbs_pid', 'sql lookup', 
    'SELECT pe.nbs_id FROM people pe INNER JOIN participations pa ON pa.type = ''InterestedParty'' AND pa.event_id=<%=event_id%> AND pe.entity_id = pa.primary_entity_id', 
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', NULL, 'core','interested_party|person_entity|person|last_name', NULL, ', ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal','p_name', 'core','interested_party|person_entity|person|first_name', NULL, NULL, NULL, NULL);

--address
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', NULL, 'core','address|street_number', NULL, ' ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', NULL, 'core','address|street_name', NULL, ' #', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'p_addr', 'core','address|unit_number', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'p_city', 'core', 'address|city', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal','p_county', 'sql lookup', 'select code_description from addresses t0 join external_codes t1 on t1.id=t0.county_id where t0.event_id=<%=event_id%>', 
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'p_zip', 'core', 'address|postal_code', NULL, NULL, NULL, NULL);

--phone number
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', NULL, 'core','interested_party|person_entity|telephones|area_code', NULL, '-', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', NULL, 'core','interested_party|person_entity|telephones|phone_number', NULL, ' x', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'p_phone', 'core','interested_party|person_entity|telephones|extension', NULL, NULL, NULL, NULL);

--age/gender
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'p_dob', 'core', 'interested_party|person_entity|person|birth_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Streptococcal', 'p_age', 'sql lookup',
    'SELECT CASE '
    '    WHEN pe.birth_date IS NOT NULL THEN date_part(''year'', age(pe.birth_date)) '
    '    WHEN pe.approximate_age_no_birthday IS NOT NULL AND pe.age_type_id=173 THEN pe.approximate_age_no_birthday '
    '    ELSE NULL '
    '  END '
    '  FROM participations p '
    '    INNER JOIN people pe ON pe.entity_id = p.primary_entity_id '
    '  WHERE p.type=''InterestedParty'' AND p.event_id=<%=event_id%> ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'p_gender', 'core', 'interested_party|person_entity|person|birth_gender_id', 'gender', NULL, NULL, NULL);
  
--race
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'race_w', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'W');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'race_b', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'B');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'race_a', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'A');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'race_h', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'H');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'race_ai_an', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'AI_AN');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'race_u', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'UNK');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'race_o', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'Other');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'race_other', 'adddemog', 'otherracespecify', NULL, NULL, NULL, NULL);
  
--ethnicity
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'ethnicity', 'core', 'interested_party|person_entity|person|ethnicity_id', 'ethnicity', NULL, NULL, NULL);

INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'occupation', 'core', 'interested_party|risk_factor|occupation', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'ltc', 'streptococcal', 'longtermcare', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'ltc_loc', 'streptococcal', 'longtermcarefacility', NULL, NULL, NULL, NULL);

--reporting
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'reported_by', 'sql lookup', 
    'SELECT pe.first_name || '' '' || pe.last_name FROM people pe '
      'INNER JOIN participations p ON p.type = ''Reporter'' AND pe.entity_id=p.secondary_entity_id AND p.event_id=<%=event_id%>  '
      'ORDER BY p.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'reporting_agency', 'core', 'reporting_agency|place_entity|place|name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', NULL, 'core', 'reporting_agency|place_entity|telephones|area_code', NULL, '-', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'ra_phone', 'core', 'reporting_agency|place_entity|telephones|phone_number', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'date_rept_ph', 'core', 'first_reported_PH_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'investigator', 'core', 'investigator|best_name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'date_init', 'core', 'investigation_started_date', NULL, NULL, 'date_m_d_y', NULL);

--clinical
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'physician', 'sql lookup',
    'SELECT pe.first_name || '' '' || pe.last_name FROM people pe '
    '  INNER JOIN participations p ON p.secondary_entity_id=pe.entity_id AND p.type = ''Clinician'' AND p.event_id=<%=event_id%> '
    '  ORDER BY p.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'phy_phone', 'sql lookup',
    'SELECT t.area_code || ''-'' || t.phone_number FROM telephones t '
    '  INNER JOIN participations p ON p.secondary_entity_id=t.entity_id AND p.type = ''Clinician'' AND p.event_id=<%=event_id%> '
    '  ORDER BY p.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'date_onset', 'core', 'disease_event|disease_onset_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'date_illness_end', 'additional_clinical', 'Illness_end', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'died', 'core', 'disease_event|died_id', 'yesno', NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'date_death', 'core', 'interested_party|person_entity|person|date_of_death', NULL, NULL, 'date_m_d_y', NULL);

INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'inf_bac', 'streptococcal', 'types_infection', NULL, NULL, 'check_box', 'Bacteremia/Sepsis');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'inf_pne', 'streptococcal', 'types_infection', NULL, NULL, 'check_box', 'Pneuomnia');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'inf_tox', 'streptococcal', 'types_infection', NULL, NULL, 'check_box', 'Toxic Shock Syndrome');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'inf_nec', 'streptococcal', 'types_infection', NULL, NULL, 'check_box', 'Necrotizing Fasciitis');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'inf_men', 'streptococcal', 'types_infection', NULL, NULL, 'check_box', 'Meningitis');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'inf_sin', 'streptococcal', 'types_infection', NULL, NULL, 'check_box', 'Sinusitis');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'inf_oti', 'streptococcal', 'types_infection', NULL, NULL, 'check_box', 'Otis Media');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'inf_end', 'streptococcal', 'types_infection', NULL, NULL, 'check_box', 'Endocarditis');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'inf_per', 'streptococcal', 'types_infection', NULL, NULL, 'check_box', 'Peritonitis');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'inf_sep', 'streptococcal', 'types_infection', NULL, NULL, 'check_box', 'Septic Arthritis');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'inf_oth', 'streptococcal', 'types_infection', NULL, NULL, 'check_box', 'Other');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'inf_other', 'streptococcal', 'other_infection', NULL, NULL, NULL, NULL);

--pregnant
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'pregnant', 'core', 'interested_party|risk_factor|pregnant_id', 'yesno', NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'weeks_gest', 'sql lookup', 
    'SELECT CAST((EXTRACT(DAY FROM (Now()-(prf.pregnancy_due_date-280)))+3)/7 AS INTEGER) from participations_risk_factors prf '
    '  INNER JOIN participations p ON p.id=prf.participation_id AND p.type=''InterestedParty'' AND p.event_id=<%=event_id%> ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'postpartum', 'streptococcal', 'postpartum', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'date_delivery', 'streptococcal', 'deliverydate', NULL, NULL, 'date_m_d_y', NULL);

--underlying health conditions
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'uhc', 'streptococcal', 'underlyingconditions', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'uhc_ast', 'streptococcal', 'underlyingconditionlist', NULL, NULL, 'check_box', 'Asthma');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'uhc_chr', 'streptococcal', 'underlyingconditionlist', NULL, NULL, 'check_box', 'Chronic lung disease');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'uhc_can', 'streptococcal', 'underlyingconditionlist', NULL, NULL, 'check_box', 'Cancer');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'uhc_coc', 'streptococcal', 'underlyingconditionlist', NULL, NULL, 'check_box', 'Cochlear implant');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'uhc_dia', 'streptococcal', 'underlyingconditionlist', NULL, NULL, 'check_box', 'Diabetes');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'uhc_hea', 'streptococcal', 'underlyingconditionlist', NULL, NULL, 'check_box', 'Heart disease');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'uhc_hem', 'streptococcal', 'underlyingconditionlist', NULL, NULL, 'check_box', 'Hemoglobinopathy');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'uhc_hiv', 'streptococcal', 'underlyingconditionlist', NULL, NULL, 'check_box', 'HIV');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'uhc_kid', 'streptococcal', 'underlyingconditionlist', NULL, NULL, 'check_box', 'Kidney disease');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'uhc_org', 'streptococcal', 'underlyingconditionlist', NULL, NULL, 'check_box', 'Organ transplant');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'uhc_oth', 'streptococcal', 'underlyingconditionlist', NULL, NULL, 'check_box', 'Other');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'uhc_other', 'streptococcal', 'other1', NULL, NULL, NULL, NULL);

--high risk behaviors
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'hrb', 'streptococcal', 'highrisk', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'hrb_con', 'streptococcal', 'highrisklist', NULL, NULL, 'check_box', 'Consumes raw (unpasteurized) milk/cheese');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'hrb_cur', 'streptococcal', 'highrisklist', NULL, NULL, 'check_box', 'Current smoker');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'hrb_int', 'streptococcal', 'highrisklist', NULL, NULL, 'check_box', 'Intravenous drug user (IVDU)');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'hrb_alc', 'streptococcal', 'highrisklist', NULL, NULL, 'check_box', 'Alcohol abuse');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'hrb_oth', 'streptococcal', 'highrisklist', NULL, NULL, 'check_box', 'Other');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'hrb_other', 'streptococcal', 'describeother', NULL, NULL, NULL, NULL);

--vaccination
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'vh_imm', 'streptococcal', 'vaxhistorysource', NULL, NULL, 'check_box', 'ImmTrac');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'vh_par', 'streptococcal', 'vaxhistorysource', NULL, NULL, 'check_box', 'Parent');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'vh_doc', 'streptococcal', 'vaxhistorysource', NULL, NULL, 'check_box', 'Doctor');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'vh_sch', 'streptococcal', 'vaxhistorysource', NULL, NULL, 'check_box', 'School');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'vh_oth', 'streptococcal', 'vaxhistorysource', NULL, NULL, 'check_box', 'Other');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'pne_vac', 'streptococcal', 'pneuvax', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'year_vac', 'streptococcal', 'vaxyear', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'vac_name', 'streptococcal', 'vaxname', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'vac_other', 'streptococcal', 'specifyothervaxname', NULL, NULL, NULL, NULL);

--Hospitalization info
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'hospitalized', 'core', 'disease_event|hospitalized_id', 'yesno', NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'hospital', 'sql lookup',
    'SELECT concat(pl.name, '', '', a.street_number, '' '', a.street_name, '' '', a.city) FROM places pl '
    '  INNER JOIN participations p ON p.secondary_entity_id=pl.entity_id AND p.type=''HospitalizationFacility'' AND p.event_id=<%=event_id%> '
    '  LEFT JOIN addresses a ON a.entity_id=pl.entity_id '
    '  ORDER BY p.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'date_admit', 'core', 'hospitalization_facilities|hospitals_participation|admission_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'date_discharge', 'core', 'hospitalization_facilities|hospitals_participation|discharge_date', NULL, NULL, 'date_m_d_y', NULL);
   
--laboratory
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'lab_coll_1', 'sql lookup',
    'SELECT lr.collection_date FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY lr.id ASC OFFSET 0 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'lab_test_1', 'sql lookup',
  'SELECT '
        'CASE ' 
          'WHEN ctt.common_name ILIKE ''%culture%'' THEN ''cul'' '
          'WHEN ctt.common_name ILIKE ''%antigen%'' THEN ''atg'' '
          'WHEN ctt.common_name ILIKE ''%pcr%'' THEN ''pcr'' '
          'WHEN ctt.common_name ILIKE ''%antibody%'' THEN ''atb'' '
          'ELSE ''oth'' '
        'END ' 
    'FROM lab_results lr '  
      'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
      'LEFT JOIN common_test_types ctt on ctt.id=lr.test_type_id '
      'ORDER BY lr.id ASC OFFSET 0 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'lab_test_1_other', 'sql lookup',
  'SELECT '
        'CASE ' 
          'WHEN ctt.common_name ILIKE ''%culture%'' OR ctt.common_name ILIKE ''%antigen%'' '
          'OR ctt.common_name ILIKE ''%pcr%'' OR ctt.common_name ILIKE ''%antibody%'' THEN '''' '
          'ELSE ctt.common_name '
        'END ' 
    'FROM lab_results lr '  
      'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
      'LEFT JOIN common_test_types ctt on ctt.id=lr.test_type_id '
      'ORDER BY lr.id ASC OFFSET 0 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'wsc_1', 'streptococcal', 'surgicalspecimen', NULL, NULL, 'filler,0', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'lab_spec_s_1', 'streptococcal', 'sterile', NULL, NULL, 'filler,0', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'lab_spec_s_1_other', 'streptococcal', 'othersterile', NULL, NULL, 'filler,0', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'lab_spec_u_1', 'streptococcal', 'nonsterile', NULL, NULL, 'filler,0', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'lab_spec_u_1_other', 'streptococcal', 'othernonsterile', NULL, NULL, 'filler,0', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'lab_org_1', 'sql lookup',
  'SELECT '
       'CASE ' 
          'WHEN o.organism_name ILIKE ''%group a strep%'' THEN ''A'' '
          'WHEN o.organism_name ILIKE ''%group b strep%'' THEN ''B'' '
          'WHEN o.organism_name=''Streptococcus pneumoniae'' THEN ''P'' '
          'ELSE ''O'' '
        'END '
    'FROM lab_results lr '  
      'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
      'LEFT JOIN organisms o on o.id=lr.organism_id '
      'ORDER BY lr.id ASC OFFSET 0 LIMIT 1', 
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'lab_org_1_other', 'sql lookup',
  'SELECT '
       'CASE ' 
          'WHEN o.organism_name ILIKE ''%group a strep%'' OR o.organism_name ILIKE ''%group b strep%'' '
          'OR o.organism_name=''Streptococcus pneumoniae'' THEN '''' '
          'ELSE o.organism_name '
        'END '
    'FROM lab_results lr '  
      'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
      'LEFT JOIN organisms o on o.id=lr.organism_id '
      'ORDER BY lr.id ASC OFFSET 0 LIMIT 1', 
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'lab_coll_2', 'sql lookup',
    'SELECT lr.collection_date FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY lr.id ASC OFFSET 1 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'lab_test_2', 'sql lookup',
  'SELECT '
        'CASE ' 
          'WHEN ctt.common_name ILIKE ''%culture%'' THEN ''cul'' '
          'WHEN ctt.common_name ILIKE ''%antigen%'' THEN ''atg'' '
          'WHEN ctt.common_name ILIKE ''%pcr%'' THEN ''pcr'' '
          'WHEN ctt.common_name ILIKE ''%antibody%'' THEN ''atb'' '
          'ELSE ''oth'' '
        'END ' 
    'FROM lab_results lr '  
      'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
      'LEFT JOIN common_test_types ctt on ctt.id=lr.test_type_id '
      'ORDER BY lr.id ASC OFFSET 1 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'lab_test_2_other', 'sql lookup',
  'SELECT '
        'CASE ' 
          'WHEN ctt.common_name ILIKE ''%culture%'' OR ctt.common_name ILIKE ''%antigen%'' '
          'OR ctt.common_name ILIKE ''%pcr%'' OR ctt.common_name ILIKE ''%antibody%'' THEN '''' '
          'ELSE ctt.common_name '
        'END ' 
    'FROM lab_results lr '  
      'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
      'LEFT JOIN common_test_types ctt on ctt.id=lr.test_type_id '
      'ORDER BY lr.id ASC OFFSET 1 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'wsc_2', 'streptococcal', 'surgicalspecimen', NULL, NULL, 'filler,1', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'lab_spec_s_2', 'streptococcal', 'sterile', NULL, NULL, 'filler,1', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'lab_spec_s_2_other', 'streptococcal', 'othersterile', NULL, NULL, 'filler,1', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'lab_spec_u_2', 'streptococcal', 'nonsterile', NULL, NULL, 'filler,1', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'lab_spec_u_2_other', 'streptococcal', 'othernonsterile', NULL, NULL, 'filler,1', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'lab_org_2', 'sql lookup',
  'SELECT '
       'CASE ' 
          'WHEN o.organism_name ILIKE ''%group a strep%'' THEN ''A'' '
          'WHEN o.organism_name ILIKE ''%group b strep%'' THEN ''B'' '
          'WHEN o.organism_name=''Streptococcus pneumoniae'' THEN ''P'' '
          'ELSE ''O'' '
        'END '
    'FROM lab_results lr '  
      'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
      'LEFT JOIN organisms o on o.id=lr.organism_id '
      'ORDER BY lr.id ASC OFFSET 1 LIMIT 1', 
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Streptococcal', 'lab_org_2_other', 'sql lookup',
  'SELECT '
       'CASE ' 
          'WHEN o.organism_name ILIKE ''%group a strep%'' OR o.organism_name ILIKE ''%group b strep%'' '
          'OR o.organism_name=''Streptococcus pneumoniae'' THEN '''' '
          'ELSE o.organism_name '
        'END '
    'FROM lab_results lr '  
      'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
      'LEFT JOIN organisms o on o.id=lr.organism_id '
      'ORDER BY lr.id ASC OFFSET 1 LIMIT 1', 
  NULL, NULL, NULL, NULL);
