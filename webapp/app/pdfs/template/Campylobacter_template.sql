﻿--If you are replacing all the mappings for this disease, you must first delete the existing mapping.
--This is commented out so as not be run inadvertently
--clear existing
--DELETE FROM generate_pdf_mappings WHERE template_pdf_name='Campylobacter';

--new
--investigation summary
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'investigator', 'core', 'investigator|best_name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'rept_to_ph_dt', 'core', 'first_reported_PH_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'date_assigned', 'core', 'investigation_started_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'date_init', 'core', 'investigation_started_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'date_completed', 'core', 'investigation_completed_LHD_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'date_reported_dshs', 'core', 'review_completed_by_state_date', NULL, NULL, 'date_now', NULL);

--reporting source
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'reporting_agency', 'core', 'reporting_agency|place_entity|place|name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'reported_by', 'sql lookup', 
    'SELECT pe.first_name || '' '' || pe.last_name FROM events e '
      'INNER JOIN participations p ON p.event_id = e.id AND p.type = ''Reporter'' '
      'INNER JOIN people pe ON pe.entity_id = p.secondary_entity_id '
      'WHERE e.id = <%=event_id%> '
      'ORDER BY p.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'rep_phone', 'core', 'reporting_agency|place_entity|telephones|area_code', NULL, '-', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'rep_phone', 'core', 'reporting_agency|place_entity|telephones|phone_number', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'rep_phone', 'sql lookup', 
    'SELECT t.area_code || ''-''  || t.phone_number FROM events e '
      'INNER JOIN participations p ON p.event_id = e.id AND p.type = ''Reporter'' '
      'INNER JOIN telephones t on t.entity_id = p.secondary_entity_id '
      'WHERE e.id = <%=event_id%> '
      'ORDER BY p.id LIMIT 1 ',
    NULL, NULL, 'replace_if_src_not_blank', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('hars', 'facility_name', 'core', 'clinicians|person_entity|person|first_name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'hcp_name', 'sql lookup', 
    'SELECT pe.first_name || '' '' || pe.last_name FROM events e '
      'INNER JOIN participations p ON p.event_id = e.id AND p.type = ''Clinician'' '
      'INNER JOIN people pe ON pe.entity_id = p.secondary_entity_id '
      'WHERE e.id = <%=event_id%> '
      'ORDER BY p.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'hcp_phone', 'sql lookup', 
    'SELECT t.area_code || ''-''  || t.phone_number FROM events e '
      'INNER JOIN participations p ON p.event_id = e.id AND p.type = ''Clinician'' '
      'INNER JOIN telephones t on t.entity_id = p.secondary_entity_id '
      'WHERE e.id = <%=event_id%> '
      'ORDER BY p.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);

--patient information
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', NULL, 'core','interested_party|person_entity|person|last_name', NULL, ', ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter','p_name', 'core','interested_party|person_entity|person|first_name', NULL, NULL, NULL, NULL);
--address
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', NULL, 'core','address|street_number', NULL, ' ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', NULL, 'core','address|street_name', NULL, ' #', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'p_addr', 'core','address|unit_number', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', NULL,'core', 'address|city', NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', NULL,'core', 'address|state_id', 'state', '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', NULL,'core', 'address|postal_code', NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter','p_city_state_zip_county', 'sql lookup', 'select code_description from addresses t0 join external_codes t1 on t1.id=t0.county_id where t0.event_id=<%=event_id%>', 
  NULL, NULL, NULL, NULL);
--phone number
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', NULL, 'core','interested_party|person_entity|telephones|area_code', NULL, '-', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', NULL, 'core','interested_party|person_entity|telephones|phone_number', NULL, ' x', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter','p_phone', 'core','interested_party|person_entity|telephones|extension', NULL, NULL, NULL, NULL);
--parent
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter','parent_guardian', 'core','parent_guardian', NULL, NULL, NULL, NULL);
--gender
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'gender', 'core', 'interested_party|person_entity|person|birth_gender_id', 'gender', NULL, NULL, NULL);
--race
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'race', 'core', 'interested_party|person_entity|race_ids', 'race', NULL, NULL, NULL);
--ethnicity
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'ethnicity', 'core', 'interested_party|person_entity|person|ethnicity_id', 'ethnicity', NULL, NULL, NULL);
--age 
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Campylobacter', 'p_age', 'sql lookup', 
    'SELECT CASE '
    '    WHEN pe.birth_date IS NOT NULL THEN date_part(''year'', age(pe.birth_date)) '
    '    WHEN pe.approximate_age_no_birthday IS NOT NULL AND pe.age_type_id=173 THEN pe.approximate_age_no_birthday '
    '    ELSE NULL '
    '  END '
    '  FROM participations p '
    '    INNER JOIN people pe ON pe.entity_id = p.primary_entity_id '
    '  WHERE p.type=''InterestedParty'' AND p.event_id=<%=event_id%> ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter','p_dob', 'core','interested_party|person_entity|person|birth_date', NULL, NULL, 'date_m_d_y', NULL);

--signs and symptoms
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter','diarrhea', 'Ent_Sym' ,'diarrhea', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter','num_stool_24', 'Ent_Sym' ,'max_number_stool', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter','bl_dia', 'Ent_Sym' ,'blood_diarrhea', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter','abd_cramp', 'Ent_Sym' ,'cramp', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter','nausea', 'Ent_Sym' ,'nausea', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter','vomit', 'Ent_Sym' ,'vomit', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter','fever', 'Ent_Sym' ,'fever', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter','hi_temp', 'Ent_Sym' ,'Highest_temp', NULL, NULL, NULL, NULL);

--clinical findings
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter','gbs', 'campylobacteriosis' ,'Guillain_Barre_Syndrome', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter','rea_art', 'campylobacteriosis' ,'Reactive_arthritis', NULL, NULL, NULL, NULL);

--patient treatment
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'ant_med', 'sql lookup', 
    'SELECT ''Yes'' FROM participations_treatments pt '
      'INNER JOIN participations p ON p.id=pt.participation_id AND p.type=''InterestedParty'' WHERE p.event_id=<%=event_id%> LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'ant_med_1', 'sql lookup', 
    'SELECT t.treatment_name FROM participations_treatments pt '
      'INNER JOIN participations p ON p.id=pt.participation_id AND p.type=''InterestedParty'' AND p.event_id=<%=event_id%> '
      'INNER JOIN treatments t ON pt.treatment_id=t.id '
      'INNER JOIN external_codes ec ON ec.id=pt.treatment_given_yn_id AND ec.the_code=''Y'' '
      'ORDER BY pt.treatment_date DESC LIMIT 1 OFFSET 0 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'ant_med_2', 'sql lookup', 
    'SELECT t.treatment_name FROM participations_treatments pt '
      'INNER JOIN participations p ON p.id=pt.participation_id AND p.type=''InterestedParty'' AND p.event_id=<%=event_id%> '
      'INNER JOIN treatments t ON pt.treatment_id=t.id '
      'INNER JOIN external_codes ec ON ec.id=pt.treatment_given_yn_id AND ec.the_code=''Y'' '
      'ORDER BY pt.treatment_date DESC LIMIT 1 OFFSET 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'ant_med_3', 'sql lookup', 
    'SELECT t.treatment_name FROM participations_treatments pt '
      'INNER JOIN participations p ON p.id=pt.participation_id AND p.type=''InterestedParty'' AND p.event_id=<%=event_id%> '
      'INNER JOIN treatments t ON pt.treatment_id=t.id '
      'INNER JOIN external_codes ec ON ec.id=pt.treatment_given_yn_id AND ec.the_code=''Y'' '
      'ORDER BY pt.treatment_date DESC LIMIT 1 OFFSET 2 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'ant_med_4', 'sql lookup', 
    'SELECT t.treatment_name FROM participations_treatments pt '
      'INNER JOIN participations p ON p.id=pt.participation_id AND p.type=''InterestedParty'' AND p.event_id=<%=event_id%> '
      'INNER JOIN treatments t ON pt.treatment_id=t.id '
      'INNER JOIN external_codes ec ON ec.id=pt.treatment_given_yn_id AND ec.the_code=''Y'' '
      'ORDER BY pt.treatment_date DESC LIMIT 1 OFFSET 3 ',
    NULL, NULL, NULL, NULL);
--hospitalization
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter','hosp_ill', 'core' ,'disease_event|hospitalized_id', 'yesno', NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter','hosp_name', 'sql lookup',
  'select t1.name from (select secondary_entity_id from participations where event_id=<%=event_id%> and type=''HospitalizationFacility'' order by id asc limit 1) t0 '
  'join places t1 on t0.secondary_entity_id=t1.entity_id', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'adm_date', 'core', 'hospitalization_facilities|hospitals_participation|admission_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'dis_date', 'core', 'hospitalization_facilities|hospitals_participation|discharge_date', NULL, NULL, 'date_m_d_y', NULL);

-- death information
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter','die_ill', 'Death_details' ,'Died_of_this_illness', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter','autop', 'Death_details' ,'Autopsy_performed', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter','date_death', 'core', 'interested_party|person_entity|person|date_of_death', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter','place_death', 'Death_details' ,'Place_of_Death', NULL, NULL, NULL, NULL);

--laboratory
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'coll_date', 'sql lookup', 
      'SELECT lr.collection_date FROM lab_results lr '
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'ORDER BY lr.collection_date DESC LIMIT 1',
    NULL, NULL, NULL, NULL);INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'spec_src', 'sql lookup', 
      'SELECT ec.code_description FROM lab_results lr '
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN external_codes ec ON lr.specimen_source_id=ec.id AND ec.code_name=''specimen'' '
        'ORDER BY lr.collection_date DESC LIMIT 1',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'species', 'sql lookup', 
      'SELECT o.organism_name FROM lab_results lr '
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN organisms o ON lr.organism_id=o.id '
        'ORDER BY lr.collection_date DESC LIMIT 1',
    NULL, NULL, NULL, NULL);    

-- infection timeline
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter','onset_date', 'core', 'disease_event|disease_onset_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'onset_minus_10', 'sql lookup', 'SELECT event_onset_date-10 from events WHERE id=<%=event_id%> ', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'onset_minus_1', 'sql lookup', 'SELECT event_onset_date-1 from events WHERE id=<%=event_id%> ', NULL, NULL, 'date_m_d_y', NULL);

--Exposure
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'cko', 'campylobacteriosis', 'Case_knows_others', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'elc', 'campylobacteriosis', 'Epidemiologic_link', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', NULL, 'campylobacteriosis', 'Name_Relation_to_case', NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'name_rel_outb', 'core', 'outbreak_name',  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'cwd', 'campylobacteriosis', 'Contact_with_diapered_incontinent', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'cl', 'campylobacteriosis', 'Congregate_living', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'cl_type', 'campylobacteriosis', 'Congregate_living_type', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'poultry', 'campylobacteriosis', 'Poultry', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'up', 'campylobacteriosis', 'Undercooked_poultry', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'hrp', 'campylobacteriosis', 'Handled_raw_poultry', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'umc', 'campylobacteriosis', 'unpasteurizedmilk', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'udp', 'campylobacteriosis', 'unpasteurizeddairy', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'gm', 'campylobacteriosis', 'group_meal', NULL, NULL, NULL, NULL);

--Restaurant exposures
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'ffr', 'sql lookup',
    'SELECT ''Yes'' from answers a '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name = ''Restaurant_exposures'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.short_name = ''restaurantname'' '
      'AND q.id = a.question_id WHERE a.event_id=<%=event_id%> LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', NULL, 'Restaurant_exposures', 'restaurantname', NULL, '/', 'replace,0', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', NULL, 'Restaurant_exposures', 'restaurantdate', NULL, '/', 'date_m_d_y,0', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', NULL, 'Restaurant_exposures', 'foodeaten', NULL, '/', 'replace,0', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', NULL, 'Restaurant_exposures', 'restaurantname', NULL, '/', 'replace,1', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', NULL, 'Restaurant_exposures', 'restaurantdate', NULL, '/', 'date_m_d_y,1', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'food_name_date', 'Restaurant_exposures', 'foodeaten', NULL, NULL, 'replace,1', NULL);
  

--exposure  
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'exp_food', 'enteric_events', 'How_exposed_enteric', NULL, NULL, 'check_box', 'Food');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'exp_dw', 'enteric_events', 'How_exposed_enteric', NULL, NULL, 'check_box', 'Drinking water');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'exp_rw', 'enteric_events', 'How_exposed_enteric', NULL, NULL, 'check_box', 'Recreational water');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'exp_person', 'enteric_events', 'How_exposed_enteric', NULL, NULL, 'check_box', 'Person');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'exp_animal', 'enteric_events', 'How_exposed_enteric', NULL, NULL, 'check_box', 'Animal');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'exp_env', 'enteric_events', 'How_exposed_enteric', NULL, NULL, 'check_box', 'Environment');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'exp_unk', 'enteric_events', 'How_exposed_enteric', NULL, NULL, 'check_box', 'Unknown');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'oth_state', 'enteric_events', 'State', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'oth_country', 'enteric_events', 'country_region', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'exp_loc', 'enteric_events', 'where_exposure', NULL, NULL, NULL, NULL);
  
--public health issues
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'efw', 'enteric_events', 'Employed_as_food_worker', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'nfh', 'enteric_events', 'non_occ_food_handling', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'ehc', 'enteric_events', 'Employed_as_health_care_worker', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'ecc', 'enteric_events', 'Employed_in_child_care', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'acs', 'enteric_events', 'Attends_child_care_school', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'hh_mem', 'enteric_events', 'HH_member', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'hhe', 'enteric_events', 'Public_Health_Actions', NULL, NULL, 'check_box', 'Hand hygiene education provided');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'fbi', 'enteric_events', 'Public_Health_Actions', NULL, NULL, 'check_box', 'FBI/restaurant info sent to Environmental HS');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'ccf', 'enteric_events', 'Public_Health_Actions', NULL, NULL, 'check_box', 'Child care facility info sent to Environmental HS');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'exclusion', 'enteric_events', 'Public_Health_Actions', NULL, NULL, 'check_box', 'Child care/ school exclusion until no longer has diarrhea');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'oth_action', 'sql lookup',
    'SELECT ''On'' from answers a '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name = ''enteric_events'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.short_name = ''Other_public_health_actions'' '
      'AND q.id = a.question_id WHERE a.event_id=<%=event_id%> LIMIT 1 ',
  NULL, NULL, NULL, NULL);

--water
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'swk', 'water_enteric', 'Source_of_drinking_water', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'sdw_i', 'water_enteric', 'Drinking_water_source', NULL, NULL, 'check_box', 'Private well');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'sdw_s', 'water_enteric', 'Drinking_water_source', NULL, NULL, 'check_box', 'Shared well');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'sdw_p', 'water_enteric', 'Drinking_water_source', NULL, NULL, 'check_box', 'Public water system');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'sdw_b', 'water_enteric', 'Drinking_water_source', NULL, NULL, 'check_box', 'Bottle water');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'duw', 'water_enteric', 'Drank_untreated_unchlorinated', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'rwe', 'water_enteric', 'Recreational_water_exposure', NULL, NULL, NULL, NULL);

--exposures
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'wfd', 'campylobacteriosis', 'works_farm_dairy', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'etp', 'campylobacteriosis', 'Exposure_to_pets', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'wps', 'campylobacteriosis', 'Was_pet_sick', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'rpf', 'campylobacteriosis', 'Raw_pet_food_or_dried_pet_treats', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'zfp', 'campylobacteriosis', 'Zoo_farm_fair', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'lpf', 'campylobacteriosis', 'Livestock_or_poultry_farm', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'lpf_c', 'campylobacteriosis', 'type_of_farm', NULL, NULL, 'check_box', 'Chickens');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'lpf_d', 'campylobacteriosis', 'type_of_farm', NULL, NULL, 'check_box', 'Ducks');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'lpf_o', 'campylobacteriosis', 'type_of_farm', NULL, NULL, 'check_box', 'Other');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'other_lpf', 'campylobacteriosis', 'other_farm_type', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'ora', 'campylobacteriosis', 'Outdoor_or_recreational_activity', NULL, NULL, NULL, NULL);

-- notes
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', NULL, 'sql lookup', 
    'SELECT to_char(pe.encounter_date, ''YYYY-MM-DD''), pe.description FROM participations_encounters pe '
      'INNER JOIN events e ON e.type=''EncounterEvent'' and e.parent_id=<%=event_id%> and pe.id = e.participations_encounter_id '
      'ORDER BY pe.encounter_date',
    NULL, '\n', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', NULL, 'sql lookup', 
    'SELECT to_char(created_at, ''YYYY-MM-DD''), note FROM notes WHERE note_type=''clinical'' AND struckthrough != TRUE AND event_id=<%=event_id%> ORDER BY id',
    NULL, '\n', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Campylobacter', 'comments_notes', 'enteric_events', 'Other_public_health_actions', NULL, NULL, NULL, NULL);
