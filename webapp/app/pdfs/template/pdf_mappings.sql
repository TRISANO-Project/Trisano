INSERT INTO pdf_mappings(template_pdf_name, display_name, disease_name_regex) 
  VALUES ('ooj', 'OOJ/FR', 'chlamy|gono|syph|hiv|aids|std|tuber');
INSERT INTO pdf_mappings(template_pdf_name, display_name, disease_name_regex) 
  VALUES ('hars', 'HARS', 'hiv|aids');
INSERT INTO pdf_mappings(template_pdf_name, display_name, disease_name_regex) 
  VALUES ('hars_pediatric', 'Pediatric HARS', 'hiv|aids');

