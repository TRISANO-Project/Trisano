﻿--If you are replacing all the mappings for this disease, you must first delete the existing mapping.
--This is commented out so as not be run inadvertently
--clear existing
--DELETE FROM generate_pdf_mappings WHERE template_pdf_name='Salmonella';

--new
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'record_number', 'sql lookup', 
    'SELECT record_number from events where id=<%=event_id%> ',
  NULL, NULL, NULL, NULL);

--investigation summary 
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'investigator', 'core', 'investigator|best_name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'date_rept_ph', 'core', 'first_reported_PH_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'date_init', 'core', 'investigation_started_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'date_completed', 'core', 'investigation_completed_LHD_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'date_assigned', 'core', 'investigation_started_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'date_reported_dshs', 'core', 'review_completed_by_state_date', NULL, NULL, 'date_today', NULL);

--reporting source
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'reporting_agency', 'core', 'reporting_agency|place_entity|place|name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', NULL, 'core', 'reporting_agency|place_entity|telephones|area_code', NULL, '-', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'ra_phone', 'core', 'reporting_agency|place_entity|telephones|phone_number', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'reported_by', 'sql lookup', 
    'SELECT pe.first_name || '' '' || pe.last_name FROM people pe '
      'INNER JOIN participations p ON p.type = ''Reporter'' AND pe.entity_id=p.secondary_entity_id AND p.event_id=<%=event_id%>  '
      'ORDER BY p.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'hcp_name', 'sql lookup', 
    'SELECT pe.first_name || '' '' || pe.last_name FROM events e '
      'INNER JOIN participations p ON p.event_id = e.id AND p.type = ''Clinician'' '
      'INNER JOIN people pe ON pe.entity_id = p.secondary_entity_id '
      'WHERE e.id = <%=event_id%> '
      'ORDER BY p.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'hcp_phone', 'sql lookup', 
    'SELECT t.area_code || ''-''  || t.phone_number FROM events e '
      'INNER JOIN participations p ON p.event_id = e.id AND p.type = ''Clinician'' '
      'INNER JOIN telephones t on t.entity_id = p.secondary_entity_id '
      'WHERE e.id = <%=event_id%> '
      'ORDER BY p.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
    
--patient information
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', NULL, 'core','interested_party|person_entity|person|last_name', NULL, ', ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella','p_name', 'core','interested_party|person_entity|person|first_name', NULL, NULL, NULL, NULL);
--address
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', NULL, 'core','address|street_number', NULL, ' ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', NULL, 'core','address|street_name', NULL, ' #', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'p_addr', 'core','address|unit_number', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', NULL,'core', 'address|city', NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', NULL,'core', 'address|state_id', 'state', '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', NULL,'core', 'address|postal_code', NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella','p_city_state_zip_county', 'sql lookup', 'select code_description from addresses t0 join external_codes t1 on t1.id=t0.county_id where t0.event_id=<%=event_id%>', 
  NULL, NULL, NULL, NULL);
--phone number
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', NULL, 'core','interested_party|person_entity|telephones|area_code', NULL, '-', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', NULL, 'core','interested_party|person_entity|telephones|phone_number', NULL, ' x', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'p_phone', 'core','interested_party|person_entity|telephones|extension', NULL, NULL, NULL, NULL);

INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'p_parent_guardian', 'core','parent_guardian', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'p_occupation', 'core', 'interested_party|risk_factor|occupation', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'p_employer', 'sql lookup', 
    'SELECT pl.name FROM codes c '
    '  INNER JOIN events e on e.parent_id=<%=event_id%> AND e.type=''PlaceEvent'' '
    '  INNER JOIN participations p ON p.event_id=e.id and p.type=''InterestedPlace'' '
    '  INNER JOIN places pl ON pl.entity_id=p.primary_entity_id '
    '  INNER JOIN places_types pt ON pt.place_id=pl.id '
    '  WHERE c.id=pt.type_id AND c.code_description=''Employer'' ORDER BY p.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'p_school', 'daycare', 'schoolname', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'p_daycare', 'daycare', 'attendfacility_name', NULL, NULL, NULL, NULL);

--age/gender
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'p_dob', 'core', 'interested_party|person_entity|person|birth_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Salmonella', 'p_age', 'sql lookup',
    'SELECT CASE '
    '    WHEN pe.birth_date IS NOT NULL THEN date_part(''year'', age(pe.birth_date)) '
    '    WHEN pe.approximate_age_no_birthday IS NOT NULL AND pe.age_type_id=173 THEN pe.approximate_age_no_birthday '
    '    ELSE NULL '
    '  END '
    '  FROM participations p '
    '    INNER JOIN people pe ON pe.entity_id = p.primary_entity_id '
    '  WHERE p.type=''InterestedParty'' AND p.event_id=<%=event_id%> ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'p_gender', 'core', 'interested_party|person_entity|person|birth_gender_id', 'gender', NULL, NULL, NULL);
  
--race
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'race_w', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'W');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'race_b', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'B');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'race_a', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'A');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'race_h', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'H');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'race_ai_an', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'AI_AN');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'race_u', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'UNK');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'race_o', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'Other');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'race_other', 'adddemog', 'otherracespecify', NULL, NULL, NULL, NULL);
  
--ethnicity
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'ethnicity', 'core', 'interested_party|person_entity|person|ethnicity_id', 'ethnicity', NULL, NULL, NULL);

--clinical information
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'date_onset', 'core', 'disease_event|disease_onset_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'date_diag', 'core', 'disease_event|date_diagnosed', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'dur_ill', 'sql lookup', 
    'SELECT  to_date(a.text_answer, ''YYYY-MM-DD'') - e.event_onset_date FROM events e '
    '  INNER JOIN answers a ON a.event_id=e.id '
    '  INNER JOIN form_references fr ON fr.event_id=e.id  '
    '  INNER JOIN forms f ON f.id = fr.form_id AND f.short_name= ''additional_clinical'' '
    '  INNER JOIN form_elements fe ON fe.form_id = f.id '
    '  INNER JOIN questions q ON q.form_element_id = fe.id AND q.short_name=''Illness_end'' '
    '  AND q.id = a.question_id '
    '  WHERE e.id=<%=event_id%> ORDER BY a.id DESC LIMIT 1 ', 
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'diarrhea', 'Ent_Sym', 'diarrhea', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'num_stool_24', 'Ent_Sym', 'max_num_stool', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'bl_dia', 'Ent_Sym', 'blood_diarrhea', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'abd_cramp', 'Ent_Sym', 'cramp', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'nausea', 'Ent_Sym', 'nausea', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'vomit', 'Ent_Sym', 'vomit', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'fever', 'Ent_Sym', 'fever', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'high_temp', 'Ent_Sym', 'Hightest_temp', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'bac', 'salmonellosis', 'Bacteremia', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'sepsis', 'salmonellosis', 'Sepsis_syndrome', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'septic', 'salmonellosis', 'Septic_arthritis', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'rea', 'salmonellosis', 'Reactive_arthritis', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'immuno', 'salmonellosis', 'Immunosuppressive', NULL, NULL, NULL, NULL);

INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'anti_med', 'sql lookup', 
    'SELECT '
    '  CASE '
    '    WHEN Count(*) > 0 THEN ''Yes'' '
    '    ELSE ''No'' '
    '  END '
    'FROM participations_treatments pt '
    'INNER JOIN participations p ON pt.participation_id=p.id AND p.event_id=<%=event_id%> ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'treat_1', 'sql lookup', 
    'SELECT t.treatment_name FROM treatments t '
    '  INNER JOIN participations_treatments pt ON pt.treatment_id=t.id '
    '  INNER JOIN participations p ON pt.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY p.id OFFSET 0 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'treat_2', 'sql lookup', 
    'SELECT t.treatment_name FROM treatments t '
    '  INNER JOIN participations_treatments pt ON pt.treatment_id=t.id '
    '  INNER JOIN participations p ON pt.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY p.id OFFSET 1 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'treat_3', 'sql lookup', 
    'SELECT t.treatment_name FROM treatments t '
    '  INNER JOIN participations_treatments pt ON pt.treatment_id=t.id '
    '  INNER JOIN participations p ON pt.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY p.id OFFSET 1 LIMIT 1 ',
  NULL, NULL, NULL, NULL);

--Hospitalization info
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'hospitalized', 'core', 'disease_event|hospitalized_id', 'yesno', NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'hospital', 'sql lookup',
    'SELECT concat(pl.name, '', '', a.street_number, '' '', a.street_name, '' '', a.city) FROM places pl '
    '  INNER JOIN participations p ON p.secondary_entity_id=pl.entity_id AND p.type=''HospitalizationFacility'' AND p.event_id=<%=event_id%> '
    '  LEFT JOIN addresses a ON a.entity_id=pl.entity_id '
    '  ORDER BY p.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'date_admit', 'core', 'hospitalization_facilities|hospitals_participation|admission_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'date_discharge', 'core', 'hospitalization_facilities|hospitals_participation|discharge_date', NULL, NULL, 'date_m_d_y', NULL);
   
--died
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'died', 'Death_details', 'Died_of_this_illness', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'date_death', 'core', 'interested_party|person_entity|person|date_of_death', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'autopsy', 'Death_details' ,'Autopsy_performed', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'place_death', 'Death_details' ,'Place_of_Death', NULL, NULL, NULL, NULL);

--laboratory
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'lab_coll', 'sql lookup',
    'SELECT lr.collection_date FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY lr.id  LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'lab_spec', 'sql lookup',
    'SELECT ec.code_description FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN external_codes ec on ec.id=lr.specimen_source_id '
    '  ORDER BY lr.id ASC LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'lab_org', 'sql lookup',
    'SELECT o.organism_name FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  INNER JOIN organisms o on o.id=lr.organism_id '
    '  ORDER BY lr.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'lab_sero', 'sql lookup',
    'SELECT replace(o.organism_name, ''Salmonella'', '''') FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  INNER JOIN organisms o on o.id=lr.organism_id '
    '  ORDER BY lr.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);

--confirmation
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'conf_method', 'acquired', 'Confirmation_Method', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'date_conf', 'core', 'disease_event|date_diagnosed', NULL, NULL, 'date_m_d_y', NULL);

--infection timeline
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'onset_minus_5', 'sql lookup', 'SELECT event_onset_date-5 from events WHERE id=<%=event_id%> ', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'onset_minus_1', 'sql lookup', 'SELECT event_onset_date-1 from events WHERE id=<%=event_id%> ', NULL, NULL, 'date_m_d_y', NULL);

--exposure
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'travel', 'repeatingtravel', 'travelprior', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', NULL, 'repeatingtravel', 'arrivaldate', NULL, '-', 'date_m_d_y,0', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', NULL, 'repeatingtravel', 'departuredate', NULL, ' | ', 'date_m_d_y,0', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', NULL, 'repeatingtravel', 'arrivaldate', NULL, '-', 'date_m_d_y,1', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', NULL, 'repeatingtravel', 'departuredate', NULL, ' | ', 'date_m_d_y,1', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', NULL, 'repeatingtravel', 'arrivaldate', NULL, '-', 'date_m_d_y,2', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'dest_dates', 'repeatingtravel', 'departuredate', NULL, NULL, 'date_m_d_y,2', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'cko', 'salmonellosis', 'Case_knows_others', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'epi_linked', 'salmonellosis', 'Contact_confirmed_case', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'cwl', 'salmonellosis', 'Contact_confirmed_case', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'name_cwl', 'salmonellosis', 'Name_Relation_to_Case', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'cwd', 'salmonellosis', 'Contact_with_diapered_incontinent', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'poultry', 'salmonellosis', 'Poultry', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'up', 'salmonellosis', 'Undercooked_poultry', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'hrp', 'salmonellosis', 'Handled_raw_poultry', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'eggs', 'salmonellosis', 'Eggs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'ror', 'salmonellosis', 'Raw_egg_products', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'rfo', 'salmonellosis', 'Raw_fruits_or_vegetables', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'types_fv', 'salmonellosis', 'Types_of_fruits_vegetables', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'sprouts', 'salmonellosis', 'Sprouts', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'udp', 'salmonellosis', 'Unpasteurized_dairy_products', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'joc', 'salmonellosis', 'Juices_or_ciders', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'gm', 'salmonellosis', 'Group_meal', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'ffr', 'sql lookup', 
    'SELECT '
    '    CASE '
    '      WHEN Count(*) > 0 THEN ''Yes'' '
    '      ELSE ''No'' '
    '    END '
    'FROM answers a '
    'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
    'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''Restaurant_exposures'' '
    'INNER JOIN form_elements fe ON fe.form_id = f.id '
    'INNER JOIN questions q ON q.form_element_id = fe.id AND q.short_name = ''restaurantname'' '
    'AND q.id = a.question_id ', 
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', NULL, 'Restaurant_exposures', 'restaurantname', NULL, '-', 'filler,0', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', NULL, 'Restaurant_exposures', 'foodeaten', NULL, '-', 'filler,0', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', NULL, 'Restaurant_exposures', 'restaurantdate', NULL, ' | ', 'date_m_d_y,0', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', NULL, 'Restaurant_exposures', 'restaurantname', NULL, '-', 'filler,1', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', NULL, 'Restaurant_exposures', 'foodeaten', NULL, '-', 'filler,1', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', NULL, 'Restaurant_exposures', 'restaurantdate', NULL, ' | ', 'date_m_d_y,1', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', NULL, 'Restaurant_exposures', 'restaurantname', NULL, '-', 'filler,2', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', NULL, 'Restaurant_exposures', 'foodeaten', NULL, '-', 'filler,2', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'foods_res', 'Restaurant_exposures', 'restaurantdate', NULL, NULL, 'date_m_d_y,2', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'sod', 'enteric_water', 'Source_of_drinking_water', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'sod_iw', 'enteric_water', 'Drinking_water_source', NULL, NULL, 'check_box', 'Private well');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'sod_sw', 'enteric_water', 'Drinking_water_source', NULL, NULL, 'check_box', 'Shared well');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'sod_pws', 'enteric_water', 'Drinking_water_source', NULL, NULL, 'check_box', 'Public water system');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'sod_bw', 'enteric_water', 'Drinking_water_source', NULL, NULL, 'check_box', 'Bottle water');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'duw', 'enteric_water', 'Drank_untreated_unchlorinated', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'rwe', 'enteric_water', 'Recreational_water_exposure', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'coh', 'Animals', 'farmdairy', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'wwa', 'Animals', 'animalproducts', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'types_animals', 'Animals', 'animalproductstype', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'acw', 'Animals', 'animalcontact', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', NULL, 'Animals', 'otherreptile', NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', NULL, 'Animals', 'otheramphibian', NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', NULL, 'Animals', 'othermammal', NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'acw_1', 'Animals', 'animalcontact', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'contact_animal', 'Animals', 'otheranimal', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'wps', 'Animals', 'animalsick', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'rpf', 'salmonellosis', 'Raw_pet_food_or_dried_pet_treats', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'zff', 'salmonellosis', 'Zoo_farm_fair', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'lop', 'salmonellosis', 'Livestock_or_poultry_farm', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'cdo_ch', 'salmonellosis', 'Type_of_farm', NULL, NULL, 'check_box', 'Chickens');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'cdo_du', 'salmonellosis', 'Type_of_farm', NULL, NULL, 'check_box', 'Ducks');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'cdo_o', 'salmonellosis', 'Type_of_farm', NULL, NULL, 'check_box', 'Other');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'type_poultry', 'salmonellosis', 'Specify_animal_type_other', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'exp_f', 'enteric_events', 'How_exposed_enteric', NULL, NULL, 'check_box', 'Food');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'exp_dw', 'enteric_events', 'How_exposed_enteric', NULL, NULL, 'check_box', 'Drinking water');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'exp_rw', 'enteric_events', 'How_exposed_enteric', NULL, NULL, 'check_box', 'Recreational water');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'exp_p', 'enteric_events', 'How_exposed_enteric', NULL, NULL, 'check_box', 'Person');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'exp_a', 'enteric_events', 'How_exposed_enteric', NULL, NULL, 'check_box', 'Animal');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'exp_e', 'enteric_events', 'How_exposed_enteric', NULL, NULL, 'check_box', 'Environment');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'exp_u', 'enteric_events', 'How_exposed_enteric', NULL, NULL, 'check_box', 'Unknown');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'wde', 'enteric_events', 'where_exposure', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'tx_ooj', 'enteric_events', 'county_city', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'state_not_tx', 'enteric_events', 'State', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'country', 'enteric_events', 'country_region', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'eaf', 'enteric_events', 'Employed_as_food_worker', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'nfh', 'enteric_events', 'non_occ_food_handling', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'eah', 'enteric_events', 'Employed_as_health_care_worker', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'eic', 'daycare', 'workdaycare', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'acc', 'enteric_events', 'Attends_child_care_school', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'hhm', 'enteric_events', 'HH_member', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'pha_e', 'enteric_events', 'Public_Health_Actions', NULL, NULL, 'check_box', 'Exclude probable and confirmed cases in sensitive occupations/situations (HCW, food, child care) until 2 negative stools');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'pha_hh', 'enteric_events', 'Public_Health_Actions', NULL, NULL, 'check_box', 'Hand hygiene education provided');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'pha_fbi', 'enteric_events', 'Public_Health_Actions', NULL, NULL, 'check_box', 'FBI/restaurant info sent to Environmental HS');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'pha_cc', 'enteric_events', 'Public_Health_Actions', NULL, NULL, 'check_box', 'Child care facility info sent to Environmental HS');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'pha_s', 'enteric_events', 'Public_Health_Actions', NULL, NULL, 'check_box', 'Child care/ school exclusion until no longer has diarrhea');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'pha_o', 'sql lookup', 
    'SELECT '
    '    CASE '
    '      WHEN Count(*) > 0 THEN ''On'' '
    '      ELSE ''Off'' '
    '    END '
    'FROM answers a '
    'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
    'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''enteric_events'' '
    'INNER JOIN form_elements fe ON fe.form_id = f.id '
    'INNER JOIN questions q ON q.form_element_id = fe.id AND q.short_name = ''Other_public_health_actions'' '
    'AND q.id = a.question_id ', 
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Salmonella', 'comments', 'enteric_events', 'Other_public_health_actions', NULL, NULL, NULL, NULL);
