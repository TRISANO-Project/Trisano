﻿--If you are replacing all the mappings for this disease, you must first delete the existing mapping.
--This is commented out so as not be run inadvertently
--clear existing
--DELETE FROM generate_pdf_mappings WHERE template_pdf_name='Hepatitis';

--new
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Hepatitis', 'NBS_PID', 'sql lookup', 
    'SELECT pe.nbs_id FROM people pe INNER JOIN participations pa ON pa.type = ''InterestedParty'' AND pa.event_id=<%=event_id%> AND pe.entity_id = pa.primary_entity_id', 
    NULL, NULL, NULL, NULL);

--Final Status
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Hepatitis', 'acute_hep_a', 'sql lookup', 
    'SELECT d.disease_name FROM diseases d '
    '  INNER JOIN events e ON e.id=<%=event_id%> '
    '  INNER JOIN external_codes ec ON ec.id = e.lhd_case_status_id AND ec.code_name=''case'' AND ec.the_code=''C'' '
    '  INNER JOIN disease_events de ON de.disease_id = d.id AND de.event_id=e.id ', 
    NULL, NULL, 'check_box', 'Hepatitis A');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Hepatitis', 'acute_hep_b', 'sql lookup', 
    'SELECT d.disease_name FROM diseases d '
    '  INNER JOIN events e ON e.id=<%=event_id%> '
    '  INNER JOIN external_codes ec ON ec.id = e.lhd_case_status_id AND ec.code_name=''case'' AND ec.the_code=''C'' '
    '  INNER JOIN disease_events de ON de.disease_id = d.id AND de.event_id=e.id ', 
    NULL, NULL, 'check_box', 'Hepatitis B, acute');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Hepatitis', 'acute_hep_c', 'sql lookup', 
    'SELECT d.disease_name FROM diseases d '
    '  INNER JOIN events e ON e.id=<%=event_id%> '
    '  INNER JOIN external_codes ec ON ec.id = e.lhd_case_status_id AND ec.code_name=''case'' AND ec.the_code=''C'' '
    '  INNER JOIN disease_events de ON de.disease_id = d.id AND de.event_id=e.id ', 
    NULL, NULL, 'check_box', 'Hepatitis C, acute');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Hepatitis', 'acute_hep_e', 'sql lookup', 
    'SELECT d.disease_name FROM diseases d '
    '  INNER JOIN events e ON e.id=<%=event_id%> '
    '  INNER JOIN external_codes ec ON ec.id = e.lhd_case_status_id AND ec.code_name=''case'' AND ec.the_code=''C'' '
    '  INNER JOIN disease_events de ON de.disease_id = d.id AND de.event_id=e.id ', 
    NULL, NULL, 'check_box', 'Hepatitis E, acute');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Hepatitis', 'susp_hep_c', 'sql lookup', 
    'SELECT d.disease_name FROM diseases d '
    '  INNER JOIN events e ON e.id=<%=event_id%> '
    '  INNER JOIN external_codes ec ON ec.id = e.lhd_case_status_id AND ec.code_name=''case'' AND ec.the_code=''S'' '
    '  INNER JOIN disease_events de ON de.disease_id = d.id AND de.event_id=e.id ', 
    NULL, NULL, 'check_box', 'Hepatitis C, acute');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Hepatitis', 'prob_hep_e', 'sql lookup', 
    'SELECT d.disease_name FROM diseases d '
    '  INNER JOIN events e ON e.id=<%=event_id%> '
    '  INNER JOIN external_codes ec ON ec.id = e.lhd_case_status_id AND ec.code_name=''case'' AND ec.the_code=''P'' '
    '  INNER JOIN disease_events de ON de.disease_id = d.id AND de.event_id=e.id ', 
    NULL, NULL, 'check_box', 'Hepatitis E, acute');

--patient details
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis','p_name', 'core','interested_party|person_entity|person|last_name', NULL, ', ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis','p_name', 'core','interested_party|person_entity|person|first_name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis','p_dob', 'core','interested_party|person_entity|person|birth_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Hepatitis', 'p_age', 'sql lookup', 
    'SELECT CASE '
    '    WHEN pe.birth_date IS NOT NULL THEN date_part(''year'', age(pe.birth_date)) '
    '    WHEN pe.approximate_age_no_birthday IS NOT NULL AND pe.age_type_id=173 THEN pe.approximate_age_no_birthday '
    '    ELSE NULL '
    '  END '
    '  FROM participations p '
    '    INNER JOIN people pe ON pe.entity_id = p.primary_entity_id '
    '  WHERE p.type=''InterestedParty'' AND p.event_id=<%=event_id%> ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Hepatitis', 'place_birth', 'hepatitis_RF', 'place_birth', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Hepatitis', 'other_place_birth', 'hepatitis_RF', 'specify_other', NULL, NULL, NULL, NULL);
    
    
--gender
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'p_gender', 'core', 'interested_party|person_entity|person|birth_gender_id', 'gender', NULL, NULL, NULL);
--race/ethnicity
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'race_w', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'W');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'race_b', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'B');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'race_a', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'A');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'race_h', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'H');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'race_ai_an', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'AI_AN');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'race_u', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'UNK');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'race_other', 'adddemog', 'otherracespecify', NULL, NULL, NULL, NULL);
  
--ethnicity
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'ethnicity', 'core', 'interested_party|person_entity|person|ethnicity_id', 'ethnicity', NULL, NULL, NULL);

--pregnant
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'pregnant', 'core', 'interested_party|risk_factor|pregnant_id', 'yesno', NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'date_due', 'core', 'interested_party|risk_factor|pregnancy_due_date', NULL, NULL, 'date_m_d_y', NULL);
  
--address
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis','p_addr', 'core','address|street_number', NULL, ' ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis','p_addr', 'core','address|street_name', NULL, ' #', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis','p_addr', 'core','address|unit_number', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis','p_city','core', 'address|city', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis','p_zip','core', 'address|postal_code', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis','p_county', 'sql lookup', 'select code_description from addresses t0 join external_codes t1 on t1.id=t0.county_id where t0.event_id=<%=event_id%>', 
  NULL, NULL, NULL, NULL);
  
--phone
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis','p_ac', 'core','interested_party|person_entity|telephones|area_code', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis','p_phone', 'core','interested_party|person_entity|telephones|phone_number', NULL, ' x', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis','p_phone', 'core','interested_party|person_entity|telephones|extension', NULL, NULL, NULL, NULL);

INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'guardian', 'core', 'parent_guardian', NULL, NULL, NULL, NULL);

--physician
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'physician', 'sql lookup',
    'SELECT pe.first_name || '' '' || pe.last_name FROM people pe '
    '  INNER JOIN participations p ON p.secondary_entity_id=pe.entity_id AND p.type = ''Clinician'' AND p.event_id=<%=event_id%> '
    '  ORDER BY p.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'phy_ac', 'sql lookup',
    'SELECT t.area_code FROM telephones t '
    '  INNER JOIN participations p ON p.secondary_entity_id=t.entity_id AND p.type = ''Clinician'' AND p.event_id=<%=event_id%> '
    '  ORDER BY p.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'phy_phone', 'sql lookup',
    'SELECT t.phone_number FROM telephones t '
    '  INNER JOIN participations p ON p.secondary_entity_id=t.entity_id AND p.type = ''Clinician'' AND p.event_id=<%=event_id%> '
    '  ORDER BY p.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'phy_addr', 'sql lookup',
    'SELECT a.street_number || '' '' || a.street_name FROM addresses a '
    '  INNER JOIN participations p ON p.secondary_entity_id=a.entity_id AND p.type = ''Clinician'' AND p.event_id=<%=event_id%> '
    '  ORDER BY p.id LIMIT 1 ',
  NULL, ' #', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'phy_addr', 'sql lookup',
    'SELECT a.unit_number FROM addresses a '
    '  INNER JOIN participations p ON p.secondary_entity_id=a.entity_id AND p.type = ''Clinician'' AND p.event_id=<%=event_id%> '
    '  ORDER BY p.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'phy_city_state_zip', 'sql lookup',
    'SELECT a.city FROM addresses a '
    '  INNER JOIN participations p ON p.secondary_entity_id=a.entity_id AND p.type = ''Clinician'' AND p.event_id=<%=event_id%> '
    '  ORDER BY p.id LIMIT 1 ',
  NULL, ' ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'phy_city_state_zip', 'sql lookup',
    'SELECT ec.the_code FROM addresses a '
    '  INNER JOIN participations p ON p.secondary_entity_id=a.entity_id AND p.type = ''Clinician'' AND p.event_id=<%=event_id%> '
    '  INNER JOIN external_codes ec ON ec.id=a.state_id AND ec.code_name = ''state'' '
    '  ORDER BY p.id LIMIT 1 ',
  NULL, ' ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'phy_city_state_zip', 'sql lookup',
    'SELECT a.postal_code FROM addresses a '
    '  INNER JOIN participations p ON p.secondary_entity_id=a.entity_id AND p.type = ''Clinician'' AND p.event_id=<%=event_id%> '
    '  ORDER BY p.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);
  
--reporting
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'reporting_agency', 'core', 'reporting_agency|place_entity|place|name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'reported_by', 'sql lookup', 
    'SELECT pe.first_name || '' '' || pe.last_name FROM people pe '
      'INNER JOIN participations p ON p.type = ''Reporter'' AND pe.entity_id=p.secondary_entity_id AND p.event_id=<%=event_id%>  '
      'ORDER BY p.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'ra_ac', 'core', 'reporting_agency|place_entity|telephones|area_code', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'ra_phone', 'core', 'reporting_agency|place_entity|telephones|phone_number', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'date_rept_ph', 'core', 'first_reported_PH_date', NULL, NULL, 'date_m_d_y', NULL);

--Hospitalization info
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'hospitalized', 'core' ,'disease_event|hospitalized_id', 'yesno', NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis','hospital', 'sql lookup',
    'SELECT pl.name FROM places pl '
    '  INNER JOIN participations p ON p.secondary_entity_id=pl.entity_id AND p.type=''HospitalizationFacility'' AND p.event_id=<%=event_id%> '
    '  ORDER BY p.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'date_admit', 'core', 'hospitalization_facilities|hospitals_participation|admission_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'date_discharge', 'core', 'hospitalization_facilities|hospitals_participation|discharge_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'duration', 'sql lookup', 
    'SELECT hp.discharge_date-hp.admission_date FROM hospitals_participations hp '
    '  INNER JOIN participations p ON p.type=''HospitalizationFacility'' '
    '  AND hp.participation_id=p.id AND p.event_id=<%=event_id%> ORDER BY p.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);

--Clinical Data
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'reason_testing', 'Hepatitis_Clinical', 'reason_testing', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'other_testing_reason', 'Hepatitis_Clinical', 'other_testing_reason', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'date_diag', 'core', 'disease_event|date_diagnosed', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'date_onset', 'core', 'disease_event|disease_onset_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'symptomatic', 'Hepatitis_Clinical', 'symptomatic', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'jaundiced', 'Hepatitis_Clinical', 'jaundiced', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'hospitalized_1', 'core' ,'disease_event|hospitalized_id', 'yesno', NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'died', 'core', 'disease_event|died_id', 'yesno', NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis','date_death', 'core', 'interested_party|person_entity|person|date_of_death', NULL, NULL, 'date_m_d_y', NULL);
 
--Diagnostic test
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'tot_hepa_anti', 'sql lookup', 
  'SELECT '
    'CASE WHEN ec.code_description ILIKE ''Positive%'' THEN ''P'' '
    '     WHEN ec.code_description ILIKE ''Negative%'' THEN ''N'' '
    '     ELSE ''U'' '
    'END '
    'FROM lab_results lr  '
    'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    'INNER JOIN organisms o ON o.id=lr.organism_id AND o.organism_name ILIKE ''Hepatitis A%'' '
    'INNER JOIN external_codes ec ON ec.id=lr.test_result_id '
    'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND ctt.common_name ILIKE ''Total Antibody%'' ORDER BY lr.id DESC LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'hepa_igm', 'sql lookup', 
  'SELECT '
    'CASE WHEN ec.code_description ILIKE ''Positive%'' THEN ''P'' '
    '     WHEN ec.code_description ILIKE ''Negative%'' THEN ''N'' '
    '     ELSE ''U'' '
    'END '
    'FROM lab_results lr  '
    'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    'INNER JOIN organisms o ON o.id=lr.organism_id AND o.organism_name ILIKE ''Hepatitis A%'' '
    'INNER JOIN external_codes ec ON ec.id=lr.test_result_id '
    'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND ctt.common_name ILIKE ''IgM%'' ORDER BY lr.id DESC LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'hepb_sag', 'sql lookup', 
  'SELECT '
    'CASE WHEN ec.code_description ILIKE ''Positive%'' THEN ''P'' '
    '     WHEN ec.code_description ILIKE ''Negative%'' THEN ''N'' '
    '     ELSE ''U'' '
    'END '
    'FROM lab_results lr  '
    'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    'INNER JOIN organisms o ON o.id=lr.organism_id AND o.organism_name ILIKE ''Hepatitis B%'' '
    'INNER JOIN external_codes ec ON ec.id=lr.test_result_id '
    'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND ctt.common_name ILIKE ''Surface Antigen%'' ORDER BY lr.id DESC LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'tot_hepb_anti', 'sql lookup', 
  'SELECT '
    'CASE WHEN ec.code_description ILIKE ''Positive%'' THEN ''P'' '
    '     WHEN ec.code_description ILIKE ''Negative%'' THEN ''N'' '
    '     ELSE ''U'' '
    'END '
    'FROM lab_results lr  '
    'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    'INNER JOIN organisms o ON o.id=lr.organism_id AND o.organism_name ILIKE ''Hepatitis B%'' '
    'INNER JOIN external_codes ec ON ec.id=lr.test_result_id '
    'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND ctt.common_name ILIKE ''Total Antibody%'' ORDER BY lr.id DESC LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'hbv_igm', 'sql lookup', 
  'SELECT '
    'CASE WHEN ec.code_description ILIKE ''Positive%'' THEN ''P'' '
    '     WHEN ec.code_description ILIKE ''Negative%'' THEN ''N'' '
    '     ELSE ''U'' '
    'END '
    'FROM lab_results lr  '
    'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    'INNER JOIN organisms o ON o.id=lr.organism_id AND o.organism_name ILIKE ''Hepatitis B%'' '
    'INNER JOIN external_codes ec ON ec.id=lr.test_result_id '
    'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND ctt.common_name ILIKE ''IgM%'' ORDER BY lr.id DESC LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'tot_hepc_anti', 'sql lookup', 
  'SELECT '
    'CASE WHEN ec.code_description ILIKE ''Positive%'' THEN ''P'' '
    '     WHEN ec.code_description ILIKE ''Negative%'' THEN ''N'' '
    '     ELSE ''U'' '
    'END '
    'FROM lab_results lr  '
    'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    'INNER JOIN organisms o ON o.id=lr.organism_id AND o.organism_name ILIKE ''Hepatitis C%'' '
    'INNER JOIN external_codes ec ON ec.id=lr.test_result_id '
    'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND ctt.common_name ILIKE ''Total Antibody%'' ORDER BY lr.id DESC LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'hcv_sig_cut', 'sql lookup', 
  'SELECT lr.result_value '
    'FROM lab_results lr  '
    'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    'INNER JOIN organisms o ON o.id=lr.organism_id AND o.organism_name ILIKE ''Hepatitis C%'' '
    'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND ctt.common_name ILIKE ''%signal/cutoff%'' ORDER BY lr.id DESC LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'hcv_rna', 'sql lookup', 
  'SELECT '
    'CASE WHEN ec.code_description ILIKE ''Positive%'' THEN ''P'' '
    '     WHEN ec.code_description ILIKE ''Negative%'' THEN ''N'' '
    '     ELSE ''U'' '
    'END '
    'FROM lab_results lr  '
    'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    'INNER JOIN organisms o ON o.id=lr.organism_id AND o.organism_name ILIKE ''Hepatitis C%'' '
    'INNER JOIN external_codes ec ON ec.id=lr.test_result_id '
    'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND ctt.common_name ILIKE ''PCR%'' ORDER BY lr.id DESC LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'hev_igm', 'sql lookup', 
  'SELECT '
    'CASE WHEN ec.code_description ILIKE ''Positive%'' THEN ''P'' '
    '     WHEN ec.code_description ILIKE ''Negative%'' THEN ''N'' '
    '     ELSE ''U'' '
    'END '
    'FROM lab_results lr  '
    'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    'INNER JOIN organisms o ON o.id=lr.organism_id AND o.organism_name ILIKE ''Hepatitis E%'' '
    'INNER JOIN external_codes ec ON ec.id=lr.test_result_id '
    'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND ctt.common_name ILIKE ''IGM%'' ORDER BY lr.id DESC LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'hev_rna', 'sql lookup', 
  'SELECT '
    'CASE WHEN ec.code_description ILIKE ''Positive%'' THEN ''P'' '
    '     WHEN ec.code_description ILIKE ''Negative%'' THEN ''N'' '
    '     ELSE ''U'' '
    'END '
    'FROM lab_results lr  '
    'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    'INNER JOIN organisms o ON o.id=lr.organism_id AND o.organism_name ILIKE ''Hepatitis E%'' '
    'INNER JOIN external_codes ec ON ec.id=lr.test_result_id '
    'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND ctt.common_name ILIKE ''PCR%'' ORDER BY lr.id DESC LIMIT 1 ',
  NULL, NULL, NULL, NULL);
  
--investigation summary
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'investigator', 'core', 'investigator|best_name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'date_init', 'core', 'investigation_started_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'date_completed', 'core', 'investigation_completed_LHD_date', NULL, NULL, 'date_m_d_y', NULL);

--Liver Enzyme
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'alt_result', 'Hepatitis_Clinical', 'alt_result', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'ast_result', 'Hepatitis_Clinical', 'AST_Result', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'date_alt', 'Hepatitis_Clinical', 'date_alt', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'date_ast', 'Hepatitis_Clinical', 'date_ast', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'upper_alt', 'Hepatitis_Clinical', 'ALT_Upper_limit', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'upper_ast', 'Hepatitis_Clinical', 'ast_upper', NULL, NULL, NULL, NULL);

----------Risk factors
-- Contact with Hep C case
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'contact_sexual', 'hep_c_additional', 'sex_contact', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'contact_hh', 'hep_c_additional', 'hh_contact', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'contact_o', 'hep_c_additional', 'other_contact', NULL, NULL, NULL, NULL);

--Blood/needle exposures
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'hemo', 'hepatitis_RF', 'hemodialysis', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'blood_acc', 'hepatitis_RF', 'blood_esposure', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'blood_trans', 'hepatitis_RF', 'blood_transfusion', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'date_trans', 'hepatitis_RF', 'spec', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'med_dent', 'hepatitis_RF', 'healthcare_worker', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'psw', 'hepatitis_RF', 'occupation', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'tattoo', 'hepatitis_RF', 'tattoo', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'loc_tattoo', 'hepatitis_RF', 'detail', NULL, NULL, NULL, NULL);

--Sex/Drugs
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'num_msp', 'hepatitis_RF', 'male_contacts', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'num_fsp', 'hepatitis_RF', 'female_contacts', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'treated_std', 'hepatitis_RF', 'STD', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'treatment_date', 'hepatitis_RF', 'treatment_date', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'inject_drugs', 'hepatitis_RF', 'IVDU', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'street_drugs', 'hepatitis_RF', 'drugs', NULL, NULL, NULL, NULL);

--Other risk factors
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'pierce_o', 'hepatitis_RF', 'piercing', NULL, NULL, 'check_box', 'Yes');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'loc_piercing', 'hepatitis_RF', 'details', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'dental', 'hepatitis_RF', 'dental', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'hospitalized_2', 'core' ,'disease_event|hospitalized_id', 'yesno', NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'surgery', 'hepatitis_RF', 'other_surgery', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'incarcerated', 'hepatitis_RF', 'incarcerated', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'year_prison', 'hepatitis_RF', 'when_incarcerated', NULL, NULL, NULL, NULL);

--Control measures
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'not_blc', 'hep_c_additional', 'control_measures', NULL, NULL, 'check_box', 'Notified blood center(s)');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'not_hosp', 'hep_c_additional', 'control_measures', NULL, NULL, 'check_box', 'Notified delivery hospital and obstretrician if woman is pregnant');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'not_dc', 'hep_c_additional', 'control_measures', NULL, NULL, 'check_box', 'Notified dialysis center, surgeon(s), acupuncture and/or tattoo parlor');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Hepatitis', 'disinfect_all', 'hep_c_additional', 'control_measures', NULL, NULL, 'check_box', 'Disinfected all equipment contaminated with blood or infectious body fluids');
