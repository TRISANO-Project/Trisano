﻿--If you are replacing all the mappings for this disease, you must first delete the existing mapping.
--This is commented out so as not be run inadvertently
--clear existing
--DELETE FROM generate_pdf_mappings WHERE template_pdf_name='Chickenpox';

--new
--patient information
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox','p_f_name', 'core','interested_party|person_entity|person|first_name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox','p_l_name', 'core','interested_party|person_entity|person|last_name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox','p_dob', 'core','interested_party|person_entity|person|birth_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Chickenpox', 'p_age', 'sql lookup', 
    'SELECT CASE '
    '    WHEN pe.birth_date IS NOT NULL THEN date_part(''year'', age(pe.birth_date)) '
    '    WHEN pe.approximate_age_no_birthday IS NOT NULL AND pe.age_type_id=173 THEN pe.approximate_age_no_birthday '
    '    ELSE NULL '
    '  END '
    '  FROM participations p '
    '    INNER JOIN people pe ON pe.entity_id = p.primary_entity_id '
    '  WHERE p.type=''InterestedParty'' AND p.event_id=<%=event_id%> ',
    NULL, NULL, NULL, NULL);
--gender
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'p_gender', 'core', 'interested_party|person_entity|person|birth_gender_id', 'gender', NULL, NULL, NULL);
  
--address
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox','p_addr', 'core','address|street_number', NULL, ' ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox','p_addr', 'core','address|street_name', NULL, ' #', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox','p_addr', 'core','address|unit_number', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox','p_city','core', 'address|city', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox','p_zip','core', 'address|postal_code', NULL, NULL, NULL, NULL);

--phone
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox','p_phone', 'core','interested_party|person_entity|telephones|area_code', NULL, '-', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox','p_phone', 'core','interested_party|person_entity|telephones|phone_number', NULL, ' x', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox','p_phone', 'core','interested_party|person_entity|telephones|extension', NULL, NULL, NULL, NULL);

--race/ethnicity
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'race_w', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'W');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'race_b', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'B');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'race_a', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'A');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'race_h', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'H');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'race_ai_an', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'AI_AN');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'race_u', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'UNK');
--ethnicity
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'ethnicity', 'core', 'interested_party|person_entity|person|ethnicity_id', 'ethnicity', NULL, NULL, NULL);

--reporting information
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'reported_by', 'sql lookup', 
    'SELECT pe.first_name || '' '' || pe.last_name FROM events e '
      'INNER JOIN participations p ON p.event_id = e.id AND p.type = ''Reporter'' '
      'INNER JOIN people pe ON pe.entity_id = p.secondary_entity_id '
      'WHERE e.id = <%=event_id%> '
      'ORDER BY p.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'reporting_agency', 'core', 'reporting_agency|place_entity|place|name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'ra_phone', 'core', 'reporting_agency|place_entity|telephones|area_code', NULL, '-', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'ra_phone', 'core', 'reporting_agency|place_entity|telephones|phone_number', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'ra_address', 'sql lookup',
    'SELECT a.street_name FROM participations p '
      'INNER JOIN addresses a ON a.entity_id = p.secondary_entity_id '
      'WHERE p.type=''ReportingAgency'' AND p.event_id=<%=event_id%> '
      'ORDER by p.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'ra_city', 'sql lookup',
    'SELECT a.city FROM participations p '
      'INNER JOIN addresses a ON a.entity_id = p.secondary_entity_id '
      'WHERE p.type=''ReportingAgency'' AND p.event_id=<%=event_id%> '
      'ORDER by p.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'ra_zip', 'sql lookup',
    'SELECT a.postal_code FROM participations p '
      'INNER JOIN addresses a ON a.entity_id = p.secondary_entity_id '
      'WHERE p.type=''ReportingAgency'' AND p.event_id=<%=event_id%> '
      'ORDER by p.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'ra_county', 'sql lookup',
    'SELECT ec.code_description  FROM participations p '
      'INNER JOIN addresses a ON a.entity_id = p.secondary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=a.county_id AND ec.code_name=''county'' '
      'WHERE p.type=''ReportingAgency'' AND p.event_id=<%=event_id%> '
      'ORDER by p.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'date_rept_ph', 'core', 'first_reported_PH_date', NULL, NULL, 'date_m_d_y', NULL);

--Hospitalization info
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox','hospitalized', 'core' ,'disease_event|hospitalized_id', 'yesno', NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox','hospital', 'sql lookup',
  'select t1.name from (select secondary_entity_id from participations where event_id=<%=event_id%> and type=''HospitalizationFacility'' order by id asc limit 1) t0 '
  'join places t1 on t0.secondary_entity_id=t1.entity_id', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'date_admit', 'core', 'hospitalization_facilities|hospitals_participation|admission_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'date_discharge', 'core', 'hospitalization_facilities|hospitals_participation|discharge_date', NULL, NULL, 'date_m_d_y', NULL);

--clinical data
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'date_onset', 'core', 'disease_event|disease_onset_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'date_onset_rash', 'chickenpox', 'rash_dt', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'rash_location', 'chickenpox', 'rashlocation', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'dermatome', 'chickenpox', 'dematome', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'num_lesions', 'chickenpox', 'totallesions', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'total_lesions', 'chickenpox', 'nlesions', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'rash_crust', 'chickenpox', 'crust', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'n_days_pre_crust', 'chickenpox', 'crustdays', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'n_days_no_crust', 'chickenpox', 'rashdays', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'fever', 'chickenpox', 'fever', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'date_fever', 'chickenpox', 'fever_dt', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'temp_fever', 'chickenpox', 'temp', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'macpap', 'chickenpox', 'macpap', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'vesicular', 'chickenpox', 'vesicular', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'hemo', 'chickenpox', 'hemorrhagic', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'itchy', 'chickenpox', 'itchy', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'scabs', 'chickenpox', 'scabs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'cropswaves', 'chickenpox', 'cropswaves', NULL, NULL, NULL, NULL);

--Laboratory data
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'lab_tests', 'sql lookup',  
    'SELECT ''Yes'' from participations p WHERE p.type=''Lab'' and p.event_id=<%=event_id%> LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'date_lab', 'sql lookup',  
      'SELECT lr.lab_test_date FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'AND lr.lab_test_date IS NOT NULL ORDER BY lr.id ASC LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'dfa', 'sql lookup',  
      'SELECT ''On'' FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND ctt.common_name=''Antigen by DFA/IF'' ORDER BY lr.id DESC LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'result_dfa', 'sql lookup', 
      'SELECT ec.code_description FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN external_codes ec ON ec.id=lr.test_result_id '
        'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND ctt.common_name=''Antigen by DFA/IF'' ORDER BY lr.id DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'pcr', 'sql lookup',  
      'SELECT ''On'' FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND ctt.common_name=''PCR/Amplification'' ORDER BY lr.id DESC LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'result_pcr', 'sql lookup', 
      'SELECT ec.code_description FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN external_codes ec ON ec.id=lr.test_result_id '
        'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND ctt.common_name=''PCR/Amplification'' ORDER BY lr.id DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'culture', 'sql lookup',  
      'SELECT ''On'' FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND ctt.common_name=''Culture'' ORDER BY lr.id DESC LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'result_culture', 'sql lookup', 
      'SELECT ec.code_description FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN external_codes ec ON ec.id=lr.test_result_id '
        'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND ctt.common_name=''Culture'' ORDER BY lr.id DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'igm', 'sql lookup',  
      'SELECT ''On'' FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND ctt.common_name=''IgM Antibody'' ORDER BY lr.id DESC LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'result_igm', 'sql lookup', 
      'SELECT ec.code_description FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN external_codes ec ON ec.id=lr.test_result_id '
        'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND ctt.common_name=''IgM Antibody'' ORDER BY lr.id DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'igg', 'sql lookup',  
      'SELECT ''On'' FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND ctt.common_name=''IgG Antibody'' ORDER BY lr.id DESC LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'result_igg', 'sql lookup', 
      'SELECT ec.code_description FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN external_codes ec ON ec.id=lr.test_result_id '
        'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND ctt.common_name=''IgG Antibody'' ORDER BY lr.id DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
    
--history/vaccination
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'hist_chickenpox', 'chickenpox', 'priordiagnosis', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'vaccinated', 'chickenpox', 'varicellavax', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'doses', 'chickenpox', 'ndoses1st', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'date_vacc_1', 'chickenpox', 'vaxdate', NULL, NULL, 'date_m_d_y,0', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'type_vacc_1', 'chickenpox', 'vaxtype', NULL, NULL, 'date_m_d_y,0', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'date_vacc_2', 'chickenpox', 'vaxdate', NULL, NULL, 'date_m_d_y,1', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Chickenpox', 'type_vacc_2', 'chickenpox', 'vaxtype', NULL, NULL, 'date_m_d_y,1', NULL);

