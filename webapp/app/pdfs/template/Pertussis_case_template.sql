--If you are replacing all the mappings for this disease, you must first delete the existing mapping.
--This is commented out so as not be run inadvertently
--clear existing
--DELETE FROM generate_pdf_mappings WHERE template_pdf_name='Pertussis_case';

--new
--event id
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Pertussis_case', 'NBS_EID', 'sql lookup', 'SELECT nbs_id FROM events WHERE id=<%=event_id%>', NULL, NULL, NULL, NULL);
--event status
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','Confirmed', 'core', 'lhd_case_status_id', 'case', NULL, 'check_box', 'C');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','Probable', 'core', 'lhd_case_status_id', 'case', NULL, 'check_box', 'P');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','Not_a_case', 'core', 'lhd_case_status_id', 'case', NULL, 'check_box', 'NC');
--reporter details
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'reported_by', 'sql lookup', 
    'SELECT pe.first_name || '' '' || pe.last_name FROM events e '
      'INNER JOIN participations p ON p.event_id = e.id AND p.type = ''Reporter'' '
      'INNER JOIN people pe ON pe.entity_id = p.secondary_entity_id '
      'WHERE e.id = <%=event_id%> '
      'ORDER BY p.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'reporting_agency', 'core', 'reporting_agency|place_entity|place|name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'rep_ac', 'core', 'reporting_agency|place_entity|telephones|area_code', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'rep_phone', 'core', 'reporting_agency|place_entity|telephones|phone_number', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'rept_to_clin_dt', 'core', 'results_reported_to_clinician_date', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'Reported_to', 'core', 'investigator|best_name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'rept_to_ph_dt', 'core', 'first_reported_PH_date', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'outbreak_name', 'core', 'outbreak_name', NULL, NULL, NULL, NULL);

--person details
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Pertussis_case', 'NBS_PID', 'sql lookup', 
    'SELECT pe.nbs_id FROM people pe INNER JOIN participations pa ON pa.type = ''InterestedParty'' AND pa.event_id=<%=event_id%> AND pe.entity_id = pa.primary_entity_id', 
    NULL, NULL, NULL, NULL);

INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','FirstName', 'core','interested_party|person_entity|person|first_name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','LastName', 'core','interested_party|person_entity|person|last_name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','dob', 'core','interested_party|person_entity|person|birth_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Pertussis_case', 'Age', 'sql lookup', 
    'SELECT CASE '
    '    WHEN pe.birth_date IS NOT NULL THEN date_part(''year'', age(pe.birth_date)) '
    '    WHEN pe.approximate_age_no_birthday IS NOT NULL AND pe.age_type_id=173 THEN pe.approximate_age_no_birthday '
    '    ELSE NULL '
    '  END '
    '  FROM participations p '
    '    INNER JOIN people pe ON pe.entity_id = p.primary_entity_id '
    '  WHERE p.type=''InterestedParty'' AND p.event_id=<%=event_id%> ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Pertussis_case', 'Infant', 'sql lookup', 
    'SELECT case when ec.code_description in (''months'',''weeks'',''days'') then ''I'' else null end '
    'FROM events e left JOIN external_codes ec ON e.age_type_id = ec.id AND e.id=<%=event_id%> ', 
    NULL, NULL, 'check_box', 'I');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', NULL, 'core','interested_party|person_entity|person|first_name', NULL,' ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','NamePage2', 'core','interested_party|person_entity|person|last_name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', NULL, 'core','interested_party|person_entity|person|first_name', NULL,' ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','NamePage3', 'core','interested_party|person_entity|person|last_name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', NULL, 'core','interested_party|person_entity|person|first_name', NULL,' ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','NamePage4', 'core','interested_party|person_entity|person|last_name', NULL, NULL, NULL, NULL);
  
  
--gender
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','Female', 'core', 'interested_party|person_entity|person|birth_gender_id', 'gender', NULL, 'check_box', 'F');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','Male', 'core', 'interested_party|person_entity|person|birth_gender_id', 'gender', NULL, 'check_box', 'M');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','Unknown', 'core', 'interested_party|person_entity|person|birth_gender_id', 'gender', NULL, 'check_box', 'UNK');
--address
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', NULL, 'core','address|street_number', NULL, ' ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', NULL, 'core','address|street_name', NULL, ' #', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','P_addr', 'core','address|unit_number', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','P_city','core', 'address|city', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','P_zip','core', 'address|postal_code', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','P_county', 'sql lookup', 'select code_description from addresses t0 join external_codes t1 on t1.id=t0.county_id where t0.event_id=<%=event_id%>', 
  NULL, NULL, NULL, NULL);
--phone
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','P_ac', 'core','interested_party|person_entity|telephones|area_code', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', NULL, 'core','interested_party|person_entity|telephones|phone_number', NULL, ' x', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','P_phone', 'core','interested_party|person_entity|telephones|extension', NULL, NULL, NULL, NULL);
--parent
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','parent_guardian', 'core','parent_guardian', NULL, NULL, NULL, NULL);

--race
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'race_w', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'W');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'race_b', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'B');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'race_a', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'A');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'race_h', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'H');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'race_ai_an', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'AI_AN');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'race_u', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'UNK');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'race_o', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'Other');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'race_other', 'adddemog', 'otherracespecify', NULL, NULL, NULL, NULL);
  
--ethnicity
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'ethnicity', 'core', 'interested_party|person_entity|person|ethnicity_id', 'ethnicity', NULL, NULL, NULL);

--diagnostic
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', NULL, 'core', 'diagnostic_facilities|place_entity|addresses|street_number', NULL, ' ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', NULL, 'core', 'diagnostic_facilities|place_entity|addresses|street_name', NULL, ' #', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','MD_addr', 'core', 'diagnostic_facilities|place_entity|addresses|unit_number', NULL, NULL, NULL, NULL);
--clinicians
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', NULL, 'core', 'clinicians|person_entity|person|first_name', NULL, ' ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', NULL, 'core', 'clinicians|person_entity|person|middle_name', NULL, ' ', NULL, NULL);
 INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','MD_name', 'core', 'clinicians|person_entity|person|last_name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','MD_ac', 'core','clinicians|person_entity|telephones|area_code', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', NULL, 'core','clinicians|person_entity|telephones|phone_number', NULL, ' x', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','MD_phone', 'core','clinicians|person_entity|telephones|extension', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', NULL, 'core', 'diagnostic_facilities|place_entity|addresses|city', NULL, ',', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', NULL, 'core', 'diagnostic_facilities|place_entity|addresses|state_id', 'state', ' ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','MD_city_state_zip', 'core', 'diagnostic_facilities|place_entity|addresses|postal_code', NULL, NULL, NULL, NULL);
-- Clinical Data  
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','Final Cough', 'pertussis', 'cough', NULL, NULL, 'check_box', 'Yes');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','cough_onset_date', 'pertussis', 'cough_onset', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','cough_duration', 'pertussis', 'cough_duration', NULL, NULL, NULL, NULL);
  
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'Paroxysmal Cough', 'pertussis', 'paroxysmal', NULL, NULL, 'check_box', 'Yes');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'par_cough_onset_date', 'pertussis', 'parox_onset', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'Inspiratory Whoop', 'pertussis', 'inspiratory', NULL, NULL, 'check_box', 'Yes');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'Vomiting after Paroxysm', 'pertussis', 'vomiting', NULL, NULL, 'check_box', 'Yes');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'apnea', 'pertussis', 'apnea', NULL, NULL, 'check_box', 'Yes');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'encephalopathy', 'pertussis', 'encephalopathy', NULL, NULL, 'check_box', 'Yes');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'cyanosis', 'pertussis', 'cyanosis', NULL, NULL, 'check_box', 'Yes');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'seizures', 'pertussis', 'seizures', NULL, NULL, 'check_box', 'Yes');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'pneumonia', 'pertussis', 'pneumonia', NULL, NULL, 'check_box', 'Yes');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'xray_yes', 'pertussis', 'xrayresults', NULL, NULL, 'check_box', 'Positive');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'xray_no', 'pertussis', 'xrayresults', NULL, NULL, 'check_box', 'Negative');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'other_symptoms', 'pertussis', 'other', NULL, NULL, 'check_box', 'Yes');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','describe_other', 'pertussis', 'describeothersymptoms', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'asthma_hist_yes', 'pertussis', 'asthma_hist', NULL, NULL, 'check_box', 'Yes');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'asthma_hist_no', 'pertussis', 'asthma_hist', NULL, NULL, 'check_box', 'No');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'still_coughing_yes', 'pertussis', 'still_coughing', NULL, NULL, 'check_box', 'Yes');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'still_coughing_no', 'pertussis', 'still_coughing', NULL, NULL, 'check_box', 'No');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','final_interview_date', 'pertussis', 'final_interview_date', NULL, NULL, NULL, NULL);

INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hosp_yes', 'core','disease_event|hospitalized_id','yesno', NULL,'check_box','Y');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hosp_no', 'core','disease_event|hospitalized_id','yesno', NULL,'check_box','N');

INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','hosp_name', 'sql lookup',
  'select t1.name from (select secondary_entity_id from participations where event_id=<%=event_id%> AND type=''HospitalizationFacility'' order by id asc limit 1) t0 '
  'join places t1 on t0.secondary_entity_id=t1.entity_id', NULL, NULL, NULL, NULL);

INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'Admitted', 'core', 'hospitalization_facilities|hospitals_participation|admission_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'Discharged', 'core', 'hospitalization_facilities|hospitals_participation|discharge_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'Days', 'sql lookup', 
    'SELECT (hp.discharge_date - hp.admission_date)+1 FROM hospitals_participations hp INNER JOIN participations p ON hp.participation_id=p.id AND ' 
    'p.type=''HospitalizationFacility'' AND p.event_id=<%=event_id%>', 
    NULL, NULL, NULL, NULL);
-- Treatments
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'anti_yes', 'sql lookup', 
    'SELECT ''On'' FROM participations_treatments pt '
      'INNER JOIN participations p ON p.id=pt.participation_id AND p.type=''InterestedParty'' WHERE p.event_id=<%=event_id%> LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'anti_no', 'sql lookup', 
    'SELECT ''On'' FROM participations p WHERE p.event_id=<%=event_id%> AND p.type=''InterestedParty'' AND NOT EXISTS (SELECT NULL from participations_treatments pt WHERE p.id=pt.participation_id ) ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'azi_given', 'sql lookup', 
    'SELECT ''On'' FROM participations_treatments pt '
      'INNER JOIN participations p ON p.id=pt.participation_id AND p.type=''InterestedParty'' AND p.event_id=<%=event_id%> '
      'INNER JOIN treatments t ON pt.treatment_id=t.id '
      'INNER JOIN external_codes ec ON ec.id=pt.treatment_given_yn_id AND ec.the_code=''Y'' '
      'WHERE t.treatment_name = ''Azithromycin'' ORDER BY pt.treatment_date DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'date_azi', 'sql lookup', 
    'SELECT pt.treatment_date FROM participations_treatments pt '
      'INNER JOIN participations p ON p.id=pt.participation_id AND p.type=''InterestedParty'' AND p.event_id=<%=event_id%> '
      'INNER JOIN treatments t ON pt.treatment_id=t.id '
      'INNER JOIN external_codes ec ON ec.id=pt.treatment_given_yn_id AND ec.the_code=''Y'' '
      'WHERE t.treatment_name = ''Azithromycin'' ORDER BY pt.treatment_date DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'azi_days', 'sql lookup', 
    'SELECT (pt.stop_treatment_date - pt.treatment_date)+1 FROM participations_treatments pt '
      'INNER JOIN participations p ON p.id=pt.participation_id AND p.type=''InterestedParty'' AND p.event_id=<%=event_id%> '
      'INNER JOIN treatments t ON pt.treatment_id=t.id '
      'INNER JOIN external_codes ec ON ec.id=pt.treatment_given_yn_id AND ec.the_code=''Y'' '
      'WHERE t.treatment_name = ''Azithromycin'' ORDER BY pt.treatment_date DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'bac_given', 'sql lookup', 
    'SELECT ''On'' FROM participations_treatments pt '
      'INNER JOIN participations p ON p.id=pt.participation_id AND p.type=''InterestedParty'' AND p.event_id=<%=event_id%> '
      'INNER JOIN treatments t ON pt.treatment_id=t.id '
      'INNER JOIN external_codes ec ON ec.id=pt.treatment_given_yn_id AND ec.the_code=''Y'' '
      'WHERE t.treatment_name = ''Bactrim'' ORDER BY pt.treatment_date DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'date_bac', 'sql lookup', 
    'SELECT pt.treatment_date FROM participations_treatments pt '
      'INNER JOIN participations p ON p.id=pt.participation_id AND p.type=''InterestedParty'' AND p.event_id=<%=event_id%> '
      'INNER JOIN treatments t ON pt.treatment_id=t.id '
      'INNER JOIN external_codes ec ON ec.id=pt.treatment_given_yn_id AND ec.the_code=''Y'' '
      'WHERE t.treatment_name = ''Bactrim'' ORDER BY pt.treatment_date DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'bac_days', 'sql lookup', 
    'SELECT (pt.stop_treatment_date - pt.treatment_date+1) FROM participations_treatments pt '
      'INNER JOIN participations p ON p.id=pt.participation_id AND p.type=''InterestedParty'' AND p.event_id=<%=event_id%> '
      'INNER JOIN treatments t ON pt.treatment_id=t.id '
      'INNER JOIN external_codes ec ON ec.id=pt.treatment_given_yn_id AND ec.the_code=''Y'' '
      'WHERE t.treatment_name = ''Bactrim'' ORDER BY pt.treatment_date DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'cla_given', 'sql lookup', 
    'SELECT ''On'' FROM participations_treatments pt '
      'INNER JOIN participations p ON p.id=pt.participation_id AND p.type=''InterestedParty'' AND p.event_id=<%=event_id%> '
      'INNER JOIN treatments t ON pt.treatment_id=t.id '
      'INNER JOIN external_codes ec ON ec.id=pt.treatment_given_yn_id AND ec.the_code=''Y'' '
      'WHERE t.treatment_name = ''Clarithromycin'' ORDER BY pt.treatment_date DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'date_cla', 'sql lookup', 
    'SELECT pt.treatment_date FROM participations_treatments pt '
      'INNER JOIN participations p ON p.id=pt.participation_id AND p.type=''InterestedParty'' AND p.event_id=<%=event_id%> '
      'INNER JOIN treatments t ON pt.treatment_id=t.id '
      'INNER JOIN external_codes ec ON ec.id=pt.treatment_given_yn_id AND ec.the_code=''Y'' '
      'WHERE t.treatment_name = ''Clarithromycin'' ORDER BY pt.treatment_date DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'cla_days', 'sql lookup', 
    'SELECT (pt.stop_treatment_date - pt.treatment_date)+1 FROM participations_treatments pt '
      'INNER JOIN participations p ON p.id=pt.participation_id AND p.type=''InterestedParty'' AND p.event_id=<%=event_id%> '
      'INNER JOIN treatments t ON pt.treatment_id=t.id '
      'INNER JOIN external_codes ec ON ec.id=pt.treatment_given_yn_id AND ec.the_code=''Y'' '
      'WHERE t.treatment_name = ''Clarithromycin'' ORDER BY pt.treatment_date DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'ery_given', 'sql lookup', 
    'SELECT ''On'' FROM participations_treatments pt '
      'INNER JOIN participations p ON p.id=pt.participation_id AND p.type=''InterestedParty'' AND p.event_id=<%=event_id%> '
      'INNER JOIN treatments t ON pt.treatment_id=t.id '
      'INNER JOIN external_codes ec ON ec.id=pt.treatment_given_yn_id AND ec.the_code=''Y'' '
      'WHERE t.treatment_name = ''Erythromycin'' ORDER BY pt.treatment_date DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'date_ery', 'sql lookup', 
    'SELECT pt.treatment_date FROM participations_treatments pt '
      'INNER JOIN participations p ON p.id=pt.participation_id AND p.type=''InterestedParty'' AND p.event_id=<%=event_id%> '
      'INNER JOIN treatments t ON pt.treatment_id=t.id '
      'INNER JOIN external_codes ec ON ec.id=pt.treatment_given_yn_id AND ec.the_code=''Y'' '
      'WHERE t.treatment_name = ''Erythromycin'' ORDER BY pt.treatment_date DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'ery_days', 'sql lookup', 
    'SELECT (pt.stop_treatment_date - pt.treatment_date)+1 FROM participations_treatments pt '
      'INNER JOIN participations p ON p.id=pt.participation_id AND p.type=''InterestedParty'' AND p.event_id=<%=event_id%> '
      'INNER JOIN treatments t ON pt.treatment_id=t.id '
      'INNER JOIN external_codes ec ON ec.id=pt.treatment_given_yn_id AND ec.the_code=''Y'' '
      'WHERE t.treatment_name = ''Erythromycin'' ORDER BY pt.treatment_date DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'other_anti_1', 'sql lookup', 
     'SELECT ''On'' FROM answers a '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> AND a.event_id=fr.event_id '
      'INNER JOIN forms f ON f.short_name=''additional_clinical'' AND f.id=fr.form_id '
      'INNER JOIN form_elements fe ON fe.form_id=f.id '
      'INNER JOIN questions q ON q.short_name=''treatmentOther'' AND q.form_element_id=fe.id AND q.id=a.question_id AND a.text_answer != '''' AND a.text_answer is NOT NULL '
      'ORDER by q.id LIMIT 1',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'other_anti_name_1', 'sql lookup', 
     'SELECT a.text_answer FROM answers a '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> AND a.event_id=fr.event_id '
      'INNER JOIN forms f ON f.short_name=''additional_clinical'' AND f.id=fr.form_id '
      'INNER JOIN form_elements fe ON fe.form_id=f.id '
      'INNER JOIN questions q ON q.short_name=''treatmentOther'' AND q.form_element_id=fe.id AND q.id=a.question_id AND a.text_answer != '''' AND a.text_answer is NOT NULL '
      'ORDER by q.id LIMIT 1',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'other_date_1', 'sql lookup', 
    'SELECT pt.treatment_date FROM participations_treatments pt '
      'INNER JOIN participations p ON p.id=pt.participation_id AND p.type=''InterestedParty'' AND p.event_id=<%=event_id%> '
      'INNER JOIN treatments t ON pt.treatment_id=t.id AND t.treatment_name=''Other'' '
      'INNER JOIN external_codes ec ON ec.id=pt.treatment_given_yn_id AND ec.the_code=''Y'' ORDER BY pt.treatment_date DESC LIMIT 1',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'other_days_1', 'sql lookup', 
    'SELECT (pt.stop_treatment_date - pt.treatment_date)+1 FROM participations_treatments pt '
      'INNER JOIN participations p ON p.id=pt.participation_id AND p.type=''InterestedParty'' AND p.event_id=<%=event_id%> '
      'INNER JOIN treatments t ON pt.treatment_id=t.id AND t.treatment_name=''Other'' '
      'INNER JOIN external_codes ec ON ec.id=pt.treatment_given_yn_id AND ec.the_code=''Y'' ORDER BY pt.treatment_date DESC LIMIT 1',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'other_anti_2', 'sql lookup', 
     'SELECT ''On'' FROM answers a '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> AND a.event_id=fr.event_id '
      'INNER JOIN forms f ON f.short_name=''additional_clinical'' AND f.id=fr.form_id '
      'INNER JOIN form_elements fe ON fe.form_id=f.id '
      'INNER JOIN questions q ON q.short_name=''treatmentOther'' AND q.form_element_id=fe.id AND q.id=a.question_id AND a.text_answer != '''' AND a.text_answer is NOT NULL '
      'ORDER by q.id LIMIT 1 OFFSET 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'other_anti_name_2', 'sql lookup', 
     'SELECT a.text_answer FROM answers a '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> AND a.event_id=fr.event_id '
      'INNER JOIN forms f ON f.short_name=''additional_clinical'' AND f.id=fr.form_id '
      'INNER JOIN form_elements fe ON fe.form_id=f.id '
      'INNER JOIN questions q ON q.short_name=''treatmentOther'' AND q.form_element_id=fe.id AND q.id=a.question_id AND a.text_answer != '''' AND a.text_answer is NOT NULL '
      'ORDER by q.id LIMIT 1 OFFSET 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'other_date_2', 'sql lookup', 
    'SELECT pt.treatment_date FROM participations_treatments pt '
      'INNER JOIN participations p ON p.id=pt.participation_id AND p.type=''InterestedParty'' AND p.event_id=<%=event_id%> '
      'INNER JOIN treatments t ON pt.treatment_id=t.id AND t.treatment_name=''Other'' '
      'INNER JOIN external_codes ec ON ec.id=pt.treatment_given_yn_id AND ec.the_code=''Y'' ORDER BY pt.treatment_date DESC LIMIT 1 OFFSET 1',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'other_days_2', 'sql lookup', 
    'SELECT (pt.stop_treatment_date - pt.treatment_date)+1 FROM participations_treatments pt '
      'INNER JOIN participations p ON p.id=pt.participation_id AND p.type=''InterestedParty'' AND p.event_id=<%=event_id%> '
      'INNER JOIN treatments t ON pt.treatment_id=t.id AND t.treatment_name=''Other'' '
      'INNER JOIN external_codes ec ON ec.id=pt.treatment_given_yn_id AND ec.the_code=''Y'' ORDER BY pt.treatment_date DESC LIMIT 1 OFFSET 1',
    NULL, NULL, NULL, NULL);
-- Patient died?
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','not_dead', 'pertussis','outcome', NULL, NULL,'check_box','survived');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','died_unknown', 'pertussis','outcome', NULL, NULL,'check_box','unknown');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','died', 'pertussis','outcome', NULL, NULL,'check_box','died');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case','date_died', 'core','interested_party|person_entity|person|date_of_death', NULL, NULL, 'date_m_d_y', NULL);

--Laboratory Data
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'lab_testing_yes', 'sql lookup', 
      'SELECT ''On'' from participations where event_id=<%=event_id%> AND type = ''Lab'' LIMIT 1',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'lab_testing_no', 'sql lookup', 
      'SELECT ''On'' FROM participations WHERE NOT EXISTS (SELECT NULL from participations where event_id=<%=event_id%> AND type = ''Lab'' LIMIT 1) LIMIT 1',      
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'other_lab_name', 'sql lookup', 
      'SELECT pl.name FROM lab_results lr '
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN places pl ON pl.entity_id=p.secondary_entity_id ORDER BY lr.id DESC LIMIT 1',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'lab_pcr', 'sql lookup', 
      'SELECT ''On'' FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND ctt.common_name=''PCR/Amplification'' ORDER BY lr.id DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'date_pcr', 'sql lookup', 
      'SELECT lr.collection_date FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND ctt.common_name=''PCR/Amplification'' ORDER BY lr.id DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'result_pcr', 'sql lookup', 
      'SELECT ec.code_description FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN external_codes ec ON ec.id=lr.test_result_id '
        'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND ctt.common_name=''PCR/Amplification'' ORDER BY lr.id DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'pending_pcr', 'sql lookup', 
      'SELECT ''On'' FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN external_codes ec ON ec.code_description=''Pending'' AND ec.id=lr.test_status_id '
        'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND ctt.common_name=''PCR/Amplification'' ORDER BY lr.id DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'lab_culture', 'sql lookup', 
      'SELECT ''On'' FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND ctt.common_name=''Culture'' ORDER BY lr.id DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'date_culture', 'sql lookup', 
      'SELECT lr.collection_date FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND ctt.common_name=''Culture'' ORDER BY lr.id DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'result_culture', 'sql lookup', 
      'SELECT ec.code_description FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN external_codes ec ON ec.id=lr.test_result_id '
        'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND ctt.common_name=''Culture'' ORDER BY lr.id DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'pending_culture', 'sql lookup', 
      'SELECT ''On'' FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN external_codes ec ON ec.code_description=''Pending'' AND ec.id=lr.test_status_id '
        'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND ctt.common_name=''Culture'' ORDER BY lr.id DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'lab_other_1', 'sql lookup', 
      'SELECT ''On'' FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND (ctt.common_name!=''Culture'' AND ctt.common_name!=''PCR/Amplification'') ORDER BY lr.id DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'date_other_1', 'sql lookup', 
      'SELECT lr.collection_date FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND (ctt.common_name!=''Culture'' AND ctt.common_name!=''PCR/Amplification'') ORDER BY lr.id DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'result_other_1', 'sql lookup', 
      'SELECT ec.code_description FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN external_codes ec ON ec.id=lr.test_result_id '
        'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND (ctt.common_name!=''Culture'' AND ctt.common_name!=''PCR/Amplification'') ORDER BY lr.id DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'pending_other_1', 'sql lookup', 
      'SELECT ''On'' FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN external_codes ec ON ec.code_description=''Pending'' AND ec.id=lr.test_status_id '
        'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND (ctt.common_name!=''Culture'' AND ctt.common_name!=''PCR/Amplification'') ORDER BY lr.id DESC LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'lab_other_2', 'sql lookup', 
      'SELECT ''On'' FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND (ctt.common_name!=''Culture'' AND ctt.common_name!=''PCR/Amplification'') ORDER BY lr.id DESC LIMIT 1 OFFSET 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'date_other_2', 'sql lookup', 
      'SELECT lr.collection_date FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND (ctt.common_name!=''Culture'' AND ctt.common_name!=''PCR/Amplification'') ORDER BY lr.id DESC LIMIT 1 OFFSET 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'result_other_2', 'sql lookup', 
      'SELECT ec.code_description FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN external_codes ec ON ec.id=lr.test_result_id '
        'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND (ctt.common_name!=''Culture'' AND ctt.common_name!=''PCR/Amplification'') ORDER BY lr.id DESC LIMIT 1 OFFSET 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'pending_other_2', 'sql lookup', 
      'SELECT ''On'' FROM lab_results lr ' 
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN external_codes ec ON ec.code_description=''Pending'' AND ec.id=lr.test_status_id '
        'INNER JOIN common_test_types ctt ON ctt.id=lr.test_type_id AND (ctt.common_name!=''Culture'' AND ctt.common_name!=''PCR/Amplification'') ORDER BY lr.id DESC LIMIT 1 OFFSET 1 ',
    NULL, NULL, NULL, NULL);
--Vaccination History
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'vaccinated', 'pertussis', 'vaccinated', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'num_doses', 'pertussis', 'nodosesvax', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'date_v1', 'pertussis', 'vaxdate', NULL, NULL, 'replace,0', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'Type_1', 'pertussis', 'vaxtype', NULL, NULL, 'replace,0', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'Manufacturer_1', 'pertussis', 'manufacturer', NULL, NULL, 'replace,0', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'Lot_1', 'pertussis', 'lotno', NULL, NULL, 'replace,0', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'date_v2', 'pertussis', 'vaxdate', NULL, NULL, 'replace,1', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'Type_2', 'pertussis', 'vaxtype', NULL, NULL, 'replace,1', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'Manufacturer_2', 'pertussis', 'manufacturer', NULL, NULL, 'replace,1', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'Lot_2', 'pertussis', 'lotno', NULL, NULL, 'replace,1', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'date_v3', 'pertussis', 'vaxdate', NULL, NULL, 'replace,2', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'Type_3', 'pertussis', 'vaxtype', NULL, NULL, 'replace,2', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'Manufacturer_3', 'pertussis', 'manufacturer', NULL, NULL, 'replace,2', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'Lot_3', 'pertussis', 'lotno', NULL, NULL, 'replace,2', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'date_v4', 'pertussis', 'vaxdate', NULL, NULL, 'replace,3', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'Type_4', 'pertussis', 'vaxtype', NULL, NULL, 'replace,3', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'Manufacturer_4', 'pertussis', 'manufacturer', NULL, NULL, 'replace,3', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'Lot_4', 'pertussis', 'lotno', NULL, NULL, 'replace,3', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'date_v5', 'pertussis', 'vaxdate', NULL, NULL, 'replace,4', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'Type_5', 'pertussis', 'vaxtype', NULL, NULL, 'replace,4', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'Manufacturer_5', 'pertussis', 'manufacturer', NULL, NULL, 'replace,4', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'Lot_5', 'pertussis', 'lotno', NULL, NULL, 'replace,4', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'date_v6', 'pertussis', 'vaxdate', NULL, NULL, 'replace,5', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'Type_6', 'pertussis', 'vaxtype', NULL, NULL, 'replace,5', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'Manufacturer_6', 'pertussis', 'manufacturer', NULL, NULL, 'replace,5', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'Lot_6', 'pertussis', 'lotno', NULL, NULL, 'replace,5', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'nvax_rel', 'pertussis', 'nonvax_reason', NULL, NULL, 'check_box', 'Religious Exemption');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'nvax_med', 'pertussis', 'nonvax_reason', NULL, NULL, 'check_box', 'Medical Contraindication');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'nvax_und', 'pertussis', 'nonvax_reason', NULL, NULL, 'check_box', 'Under Age');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'nvax_par', 'pertussis', 'nonvax_reason', NULL, NULL, 'check_box', 'Parental Refusal');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'nvax_unk', 'pertussis', 'nonvax_reason', NULL, NULL, 'check_box', 'Unknown');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'nvax_oth', 'pertussis', 'nonvax_reason', NULL, NULL, 'check_box', 'Other');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'nvax_reason', 'pertussis', 'otherreason', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'tdap_del', 'pertussis', 'momvax', NULL, NULL, 'check_box', 'At Delivery');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'tdap_pos', 'pertussis', 'momvax', NULL, NULL, 'check_box', 'Postpartum');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'tdap_pre', 'pertussis', 'momvax', NULL, NULL, 'check_box', 'During pregnancy');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'tdap_unk', 'pertussis', 'momvax', NULL, NULL, 'check_box', 'Unknown');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'tdap_not', 'pertussis', 'momvax', NULL, NULL, 'check_box', 'Not vaccinated during or after pregnancy (within 1 month)');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'tdap_date', 'pertussis', 'momvaxdate', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'tdap_2nd', 'pertussis', 'momvaxdateunknown', NULL, NULL, 'check_box', 'Second Trimester');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'tdap_3rd', 'pertussis', 'momvaxdateunknown', NULL, NULL, 'check_box', 'Third Trimester');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'tdap_del_udate', 'pertussis', 'momvaxdateunknown', NULL, NULL, 'check_box', 'Vaccinated at delivery');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'tdap_aft', 'pertussis', 'momvaxdateunknown', NULL, NULL, 'check_box', 'Vaccinated more than 1 day after delivery');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'num_doses_2_weeks', 'pertussis', 'nodosesvax', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'date_last_vax', 'pertussis', 'lastvaxdate', NULL, NULL, NULL, NULL);

-- Source of Infection 
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'onset_cough', 'pertussis', 'cough_onset', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'onset_cough_minus_7', 'sql lookup', 
      'SELECT to_date(a.text_answer, ''YYYY-MM-DD'')-7 from answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> AND a.event_id=fr.event_id '
        'INNER JOIN forms f ON f.short_name = ''pertussis'' AND f.id = fr.form_id ' 
        'INNER JOIN form_elements fe ON fe.form_id = f.id  '
        'INNER JOIN questions q ON q.short_name = ''cough_onset'' AND q.form_element_id = fe.id AND q.id = a.question_id ORDER by a.id DESC ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'onset_cough_minus_21', 'sql lookup', 
      'SELECT to_date(a.text_answer, ''YYYY-MM-DD'')-21 from answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> AND a.event_id=fr.event_id '
        'INNER JOIN forms f ON f.short_name = ''pertussis'' AND f.id = fr.form_id ' 
        'INNER JOIN form_elements fe ON fe.form_id = f.id  '
        'INNER JOIN questions q ON q.short_name = ''cough_onset'' AND q.form_element_id = fe.id AND q.id = a.question_id ORDER by a.id DESC ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'onset_parox', 'pertussis', 'parox_onset', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'onset_parox_plus_21', 'sql lookup', 
      'SELECT to_date(a.text_answer, ''YYYY-MM-DD'')+21 from answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> AND a.event_id=fr.event_id '
        'INNER JOIN forms f ON f.short_name = ''pertussis'' AND f.id = fr.form_id ' 
        'INNER JOIN form_elements fe ON fe.form_id = f.id  '
        'INNER JOIN questions q ON q.short_name = ''parox_onset'' AND q.form_element_id = fe.id AND q.id = a.question_id ORDER by a.id DESC ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'no_exp', 'pertussis', 'source', NULL, NULL, 'check_box', 'Unknown');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'close_contact', 'pertussis', 'transmission', NULL, NULL, 'check_box', 'Close contact with known or suspected case');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'household_exp', 'pertussis', 'transmission', NULL, NULL, 'check_box', 'Household exposure');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'source_infection', 'sql lookup', 
    'SELECT ''HE'' FROM answers a '
    '  INNER JOIN events e ON e.parent_id=<%=event_id%> '
    '  INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
    '  INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
    '  INNER JOIN form_references fr ON fr.event_id=e.id '
    '  INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis_contacts'' '
    '  INNER JOIN form_elements fe ON fe.form_id=f.id '
    '  INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''source_of_infection'' '
    '  AND q.id=a.question_id AND a.event_id=e.id AND a.text_answer=''Yes'' LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'source_infection', 'sql lookup', 
    'SELECT ''CC'' FROM answers a '
    '  INNER JOIN events e ON e.parent_id=<%=event_id%> '
    '  INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
    '  INNER JOIN external_codes ec ON ec.code_description!=''Household'' AND ec.id=pc.contact_type_id '
    '  INNER JOIN form_references fr ON fr.event_id=e.id '
    '  INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis_contacts'' '
    '  INNER JOIN form_elements fe ON fe.form_id=f.id '
    '  INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''source_of_infection'' '
    '  AND q.id=a.question_id AND a.event_id=e.id AND a.text_answer=''Yes'' LIMIT 1 ',
  NULL, NULL, 'replace_if_dst_blank', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'source_infection', 'sql lookup', 
    'SELECT ''CC'' FROM answers a '
    '  INNER JOIN events e ON e.parent_id=<%=event_id%> AND e.type=''PlaceEvent'' '
    '  INNER JOIN form_references fr ON fr.event_id=e.id '
    '  INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''travel_places'' '
    '  INNER JOIN form_elements fe ON fe.form_id=f.id ' 
    '  INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''location_spread'' '
    '  AND q.id=a.question_id AND a.event_id=e.id AND a.text_answer=''Yes'' LIMIT 1 ',
  NULL, NULL, 'replace_if_dst_blank', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'source_infection', 'sql lookup', 'SELECT ''U'' ', NULL, NULL, 'replace_if_dst_blank', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'source_name', 'sql lookup', 
    'SELECT concat(pe.first_name, '' '', pe.last_name) AS full_name FROM events e '
    '  INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.parent_id '
    '  INNER JOIN people pe ON pe.entity_id=p.primary_entity_id '
    '  WHERE e.id=<%=event_id%> ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'source_name', 'sql lookup', 
    'SELECT concat(pe.first_name, '' '', pe.last_name) AS full_name FROM '
    '    (SELECT e.id FROM events e '
    '      INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
    '      INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
    '      INNER JOIN form_references fr ON fr.event_id=e.id '
    '      INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis_contacts'' '
    '      INNER JOIN form_elements fe ON fe.form_id=f.id '
    '      INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''source_of_infection'' '
    '      INNER JOIN answers a ON q.id=a.question_id ' 
    '      WHERE e.parent_id=<%=event_id%> AND a.event_id=e.id AND a.text_answer=''Yes'' ORDER BY e.id LIMIT 1 '
    '      ) e '
    '    INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
    '    INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
  NULL, NULL, 'replace_if_dst_blank', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'source_age', 'sql lookup', 
    'SELECT CASE '
    '    WHEN pe.birth_date IS NOT NULL THEN date_part(''year'', age(pe.birth_date)) '
    '    WHEN pe.approximate_age_no_birthday IS NOT NULL AND pe.age_type_id=173 THEN pe.approximate_age_no_birthday '
    '   ELSE NULL '
    '  END FROM events e '
    '  INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.parent_id '
    '  INNER JOIN people pe ON pe.entity_id=p.primary_entity_id '
    '  WHERE e.id=<%=event_id%> ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'source_age', 'sql lookup', 
    'SELECT CASE '
    '    WHEN pe.birth_date IS NOT NULL THEN date_part(''year'', age(pe.birth_date)) '
    '    WHEN pe.approximate_age_no_birthday IS NOT NULL AND pe.age_type_id=173 THEN pe.approximate_age_no_birthday '
    '    ELSE NULL '
    '  END FROM '
    '    (SELECT e.id FROM events e '
    '      INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
    '      INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
    '      INNER JOIN form_references fr ON fr.event_id=e.id '
    '      INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis_contacts'' '
    '      INNER JOIN form_elements fe ON fe.form_id=f.id '
    '      INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''source_of_infection'' '
    '      INNER JOIN answers a ON a.event_id=e.id AND q.id=a.question_id ' 
    '      WHERE e.parent_id=<%=event_id%> AND a.text_answer=''Yes'' ORDER BY e.id LIMIT 1 '
    '      ) e '
    '    INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
    '    INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
  NULL, NULL, 'replace_if_dst_blank', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'source_cough_onset', 'sql lookup', 
    'SELECT a.text_answer FROM events e '
    '  INNER JOIN form_references fr ON fr.event_id=e.parent_id '
    '  INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis'' '
    '  INNER JOIN form_elements fe ON fe.form_id=f.id '
    '  INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''cough_onset'' '
    '  INNER JOIN answers a ON a.event_id=e.id AND q.id=a.question_id '
    '  WHERE e.id=<%=event_id%> ORDER BY a.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'source_cough_onset', 'sql lookup', 
    'SELECT a.text_answer FROM '
    '  (SELECT e.id FROM events e '
    '    INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
    '    INNER JOIN form_references fr ON fr.event_id=e.id '
    '    INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis_contacts'' '
    '    INNER JOIN form_elements fe ON fe.form_id=f.id '
    '    INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''source_of_infection'' '
    '    INNER JOIN answers a ON q.id=a.question_id ' 
    '    WHERE e.parent_id=<%=event_id%> AND a.event_id=e.id AND a.text_answer=''Yes'' ORDER BY e.id LIMIT 1 ) e '
    '  INNER JOIN form_references fr ON fr.event_id=e.id '
    '  INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis_contacts'' '
    '  INNER JOIN form_elements fe ON fe.form_id=f.id '
    '  INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''cough_onset_date'' '
    '  INNER JOIN answers a ON a.event_id=e.id AND q.id=a.question_id '
    '  ORDER BY a.id LIMIT 1 ',     
  NULL, NULL, 'replace_if_dst_blank', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'source_num_doses', 'sql lookup', 
    'SELECT a.text_answer FROM events e '
    '  INNER JOIN form_references fr ON fr.event_id=e.parent_id '
    '  INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis'' '
    '  INNER JOIN form_elements fe ON fe.form_id=f.id '
    '  INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''nodosesvax'' '
    '  INNER JOIN answers a ON a.event_id=e.id AND q.id=a.question_id '
    '  WHERE e.id=<%=event_id%> ORDER BY a.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'source_num_doses', 'sql lookup', 
    'SELECT a.text_answer FROM '
    '  (SELECT e.id FROM events e '
    '    INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
    '    INNER JOIN form_references fr ON fr.event_id=e.id '
    '    INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis_contacts'' '
    '    INNER JOIN form_elements fe ON fe.form_id=f.id '
    '    INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''source_of_infection'' '
    '    INNER JOIN answers a ON a.event_id=e.id AND q.id=a.question_id ' 
    '    WHERE e.parent_id=<%=event_id%> AND a.text_answer=''Yes'' ORDER BY e.id LIMIT 1 ) e '
    '  INNER JOIN form_references fr ON fr.event_id=e.id '
    '  INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis_contacts'' '
    '  INNER JOIN form_elements fe ON fe.form_id=f.id '
    '  INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''ndoses_house'' '
    '  INNER JOIN answers a ON a.event_id=e.id AND q.id=a.question_id '
    '  ORDER BY a.id LIMIT 1 ',     
  NULL, NULL, 'replace_if_dst_blank', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'source_phone', 'sql lookup', 
    'SELECT (t.area_code || '' '' || t.phone_number) AS phone FROM telephones t '
    '  INNER JOIN events e ON e.id=<%=event_id%> '
    '  INNER JOIN participations pa ON pa.type = ''InterestedParty'' AND pa.event_id=e.parent_id '
    '  AND t.entity_id = pa.primary_entity_id ', 
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'source_phone', 'sql lookup', 
    'SELECT (t.area_code || '' '' || t.phone_number) AS phone FROM participations pa '
    '  INNER JOIN (SELECT e.id FROM events e '
    '    INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
    '    INNER JOIN form_references fr ON fr.event_id=e.id '
    '    INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis_contacts'' '
    '    INNER JOIN form_elements fe ON fe.form_id=f.id '
    '    INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''source_of_infection'' '
    '    INNER JOIN answers a ON a.event_id=e.id AND q.id=a.question_id ' 
    '    WHERE e.parent_id=<%=event_id%> AND a.text_answer=''Yes'' ORDER BY e.id LIMIT 1) e ON e.id=pa.event_id'
    '  INNER JOIN telephones t ON t.entity_id = pa.primary_entity_id '
    '  WHERE pa.type = ''InterestedParty'' ',
  NULL, NULL, 'replace_if_dst_blank', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'source_nbs_num', 'sql lookup', 
    'SELECT pe.nbs_id FROM events pe '
    '  INNER JOIN (SELECT e.parent_id FROM events e WHERE e.id=<%=event_id%>) e ON pe.id=e.parent_id ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'source_nbs_num', 'sql lookup', 
    'SELECT e.nbs_id FROM events e '
    '  INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
    '  INNER JOIN form_references fr ON fr.event_id=e.id '
    '  INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis_contacts'' '
    '  INNER JOIN form_elements fe ON fe.form_id=f.id '
    '  INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''source_of_infection'' '
    '  INNER JOIN answers a ON a.event_id=e.id AND q.id=a.question_id ' 
    '  WHERE e.parent_id=<%=event_id%> AND a.text_answer=''Yes'' AND a.event_id=e.id ORDER BY e.id LIMIT 1 ',
  NULL, NULL, 'replace_if_dst_blank', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'lab_linked', 'pertussis', 'epilinked', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'loc_acquired', 'sql lookup', 
    'SELECT c.the_code FROM codes c '
    '  INNER JOIN events e on e.parent_id=<%=event_id%> AND e.type=''PlaceEvent'' '
    '  INNER JOIN participations p ON p.event_id=e.id AND p.type=''InterestedPlace'' '
    '  INNER JOIN places pl ON pl.entity_id=p.primary_entity_id '
    '  INNER JOIN places_types pt ON pt.place_id=pl.id '
    '  INNER JOIN form_references fr ON fr.event_id=e.id '
    '  INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''travel_places'' '
    '  INNER JOIN form_elements fe ON fe.form_id=f.id  '
    '  INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''location_spread'' '
    '  INNER JOIN answers a ON q.id=a.question_id AND a.event_id=e.id AND a.text_answer=''Yes'' '
    '  WHERE c.id=pt.type_id ORDER BY e.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'loc_acquired', 'sql lookup', 
    'SELECT ''Home'' FROM events e '
    '  INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
    '  INNER JOIN form_references fr ON fr.event_id=e.id '
    '  INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis_contacts'' '
    '  INNER JOIN form_elements fe ON fe.form_id=f.id '
    '  INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''source_of_infection'' '
    '  INNER JOIN answers a ON a.event_id=e.id AND q.id=a.question_id ' 
    '  INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
    'WHERE e.parent_id=<%=event_id%> AND a.event_id=e.id AND a.text_answer=''Yes'' ORDER BY e.id LIMIT 1 ',
  NULL, NULL, 'replace_if_dst_blank', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'loc_acquired_1', 'sql lookup', 
    'SELECT c.the_code FROM codes c '
    '  INNER JOIN events e on e.parent_id=<%=event_id%> AND e.type=''PlaceEvent'' '
    '  INNER JOIN participations p ON p.event_id=e.id AND p.type=''InterestedPlace'' '
    '  INNER JOIN places pl ON pl.entity_id=p.primary_entity_id '
    '  INNER JOIN places_types pt ON pt.place_id=pl.id '
    '  INNER JOIN form_references fr ON fr.event_id=e.id '
    '  INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''travel_places'' '
    '  INNER JOIN form_elements fe ON fe.form_id=f.id  '
    '  INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''location_spread'' '
    '  INNER JOIN answers a ON q.id=a.question_id AND a.event_id=e.id AND a.text_answer=''Yes'' '
    '  WHERE c.id=pt.type_id ORDER BY e.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'loc_acquired_1', 'sql lookup', 
    'SELECT ''Home'' FROM events e '
    '  INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
    '  INNER JOIN form_references fr ON fr.event_id=e.id '
    '  INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis_contacts'' '
    '  INNER JOIN form_elements fe ON fe.form_id=f.id '
    '  INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''source_of_infection'' '
    '  INNER JOIN answers a ON a.event_id=e.id AND q.id=a.question_id ' 
    '  INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
    'WHERE e.parent_id=<%=event_id%> AND a.event_id=e.id AND a.text_answer=''Yes'' ORDER BY e.id LIMIT 1 ',
  NULL, NULL, 'replace_if_dst_blank', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'loc_acquired_1', 'sql lookup', 'SELECT ''99'' ', NULL, NULL, 'replace_if_dst_blank', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'loc_name_setting', 'sql lookup', 
    'SELECT pl.name FROM places pl '
    '  INNER JOIN events e on e.parent_id=<%=event_id%> AND e.type=''PlaceEvent'' '
    '  INNER JOIN participations p ON p.event_id=e.id AND p.type=''InterestedPlace'' '
    '  INNER JOIN form_references fr ON fr.event_id=e.id '
    '  INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''travel__places'' '
    '  INNER JOIN form_elements fe ON fe.form_id=f.id  '
    '  INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''location_spread'' '
    '  INNER JOIN answers a ON q.id=a.question_id AND a.event_id=e.id AND a.text_answer=''Yes'' '
    '  WHERE pl.entity_id=p.primary_entity_id ORDER BY e.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'travel_linked', 'pertussis', 'transmitter_travel', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'loc_travel', 'pertussis', 'transmittertravelloc', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'outbreak_linked', 'core', 'outbreak_associated_id', 'yesno', NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'loc_outbreak', 'core', 'outbreak_name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'attend_school', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
    '  INNER JOIN events e ON e.parent_id=<%=event_id%> AND e.type=''PlaceEvent'' '
    '  INNER JOIN form_references fr ON fr.event_id=e.id '
    '  INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis_places'' '
    '  INNER JOIN form_elements fe ON fe.form_id=f.id '
    '  INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''attend_school'' '
    '  WHERE q.id=a.question_id AND a.event_id=e.id AND a.text_answer=''Yes'' ORDER BY e.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'school_name', 'sql lookup', 
    'SELECT pl.name FROM '
    '  (SELECT e.id FROM events e '
    '    INNER JOIN form_references fr ON fr.event_id=e.id '
    '    INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis_places'' '
    '    INNER JOIN form_elements fe ON fe.form_id=f.id '
    '    INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''attend_school'' '
    '    INNER JOIN answers a ON a.event_id=e.id AND q.id=a.question_id AND a.text_answer=''Yes'' '
    '    WHERE e.parent_id=<%=event_id%> AND e.type=''PlaceEvent'' ORDER BY e.id LIMIT 1 ) e '
    '  INNER JOIN participations p ON p.event_id = e.id AND p.type=''InterestedPlace'' '
    '  INNER JOIN places pl ON pl.entity_id=p.primary_entity_id ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'sch_date_attend', 'sql lookup', 
    'SELECT a.text_answer FROM '
    '  (SELECT e.id FROM events e '
    '    INNER JOIN form_references fr ON fr.event_id=e.id '
    '    INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis_places'' '
    '    INNER JOIN form_elements fe ON fe.form_id=f.id '
    '    INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''attend_school'' '
    '    INNER JOIN answers a ON a.event_id=e.id AND q.id=a.question_id AND a.text_answer=''Yes'' '
    '    WHERE e.parent_id=<%=event_id%> AND e.type=''PlaceEvent'' ORDER BY e.id LIMIT 1 ) e '
    '  INNER JOIN form_references fr ON fr.event_id=e.id '
    '  INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis_places'' '
    '  INNER JOIN form_elements fe ON fe.form_id=f.id '
    '  INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''last_date'' '
    '  INNER JOIN answers a ON a.event_id=e.id AND q.id=a.question_id AND a.event_id=e.id ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'sch_date_return', 'sql lookup', 
    'SELECT a.text_answer FROM '
    '  (SELECT e.id FROM events e '
    '    INNER JOIN form_references fr ON fr.event_id=e.id '
    '    INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis_places'' '
    '    INNER JOIN form_elements fe ON fe.form_id=f.id '
    '    INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''attend_school'' '
    '    INNER JOIN answers a ON a.event_id=e.id AND q.id=a.question_id AND a.text_answer=''Yes'' '
    '    WHERE e.parent_id=<%=event_id%> AND e.type=''PlaceEvent'' ORDER BY e.id LIMIT 1 ) e '
    '  INNER JOIN form_references fr ON fr.event_id=e.id '
    '  INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis_places'' '
    '  INNER JOIN form_elements fe ON fe.form_id=f.id '
    '  INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''date_return'' '
    '  INNER JOIN answers a ON a.event_id=e.id AND q.id=a.question_id AND a.event_id=e.id ',
 NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'grade', 'sql lookup', 
    'SELECT a.text_answer FROM '
    '  (SELECT e.id FROM events e '
    '    INNER JOIN form_references fr ON fr.event_id=e.id '
    '    INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis_places'' '
    '    INNER JOIN form_elements fe ON fe.form_id=f.id '
    '    INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''attend_school'' '
    '    INNER JOIN answers a ON a.event_id=e.id AND q.id=a.question_id AND a.text_answer=''Yes'' '
    '    WHERE e.parent_id=<%=event_id%> AND e.type=''PlaceEvent'' ORDER BY e.id LIMIT 1 ) e '
    '  INNER JOIN form_references fr ON fr.event_id=e.id '
    '  INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis_places'' '
    '  INNER JOIN form_elements fe ON fe.form_id=f.id '
    '  INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''grade'' '
    '  INNER JOIN answers a ON a.event_id=e.id AND q.id=a.question_id AND a.event_id=e.id ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'teacher_name', 'sql lookup', 
    'SELECT a.text_answer FROM '
    '  (SELECT e.id FROM events e '
    '    INNER JOIN form_references fr ON fr.event_id=e.id '
    '    INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis_places'' '
    '    INNER JOIN form_elements fe ON fe.form_id=f.id '
    '    INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''attend_school'' '
    '    INNER JOIN answers a ON a.event_id=e.id AND q.id=a.question_id AND a.text_answer=''Yes'' '
    '    WHERE e.parent_id=<%=event_id%> AND e.type=''PlaceEvent'' ORDER BY e.id LIMIT 1 ) e '
    '  INNER JOIN form_references fr ON fr.event_id=e.id '
    '  INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis_places'' '
    '  INNER JOIN form_elements fe ON fe.form_id=f.id '
    '  INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''teacher_name'' '
    '  INNER JOIN answers a ON a.event_id=e.id AND q.id=a.question_id AND a.event_id=e.id ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'after_school', 'sql lookup', 
    'SELECT a.text_answer FROM '
    '  (SELECT e.id FROM events e '
    '    INNER JOIN form_references fr ON fr.event_id=e.id '
    '    INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis_places'' '
    '    INNER JOIN form_elements fe ON fe.form_id=f.id '
    '    INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''attend_school'' '
    '    INNER JOIN answers a ON a.event_id=e.id AND q.id=a.question_id AND a.text_answer=''Yes'' '
    '    WHERE e.parent_id=<%=event_id%> AND e.type=''PlaceEvent'' ORDER BY e.id LIMIT 1 ) e '
    '  INNER JOIN form_references fr ON fr.event_id=e.id '
    '  INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis_places'' '
    '  INNER JOIN form_elements fe ON fe.form_id=f.id '
    '  INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''after_school'' '
    '  INNER JOIN answers a ON a.event_id=e.id AND q.id=a.question_id AND a.event_id=e.id ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'other_after_school', 'sql lookup', 
    'SELECT a.text_answer FROM '
    '  (SELECT e.id FROM events e '
    '    INNER JOIN form_references fr ON fr.event_id=e.id '
    '    INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis_places'' '
    '    INNER JOIN form_elements fe ON fe.form_id=f.id '
    '    INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''attend_school'' '
    '    INNER JOIN answers a ON a.event_id=e.id AND q.id=a.question_id AND a.text_answer=''Yes'' '
    '    WHERE e.parent_id=<%=event_id%> AND e.type=''PlaceEvent'' ORDER BY e.id LIMIT 1 ) e '
    '  INNER JOIN form_references fr ON fr.event_id=e.id '
    '  INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis_places'' '
    '  INNER JOIN form_elements fe ON fe.form_id=f.id '
    '  INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''other_after_school'' '
    '  INNER JOIN answers a ON a.event_id=e.id AND q.id=a.question_id AND a.event_id=e.id ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'trans_school', 'sql lookup', 
    'SELECT a.text_answer FROM '
    '  (SELECT e.id FROM events e '
    '    INNER JOIN form_references fr ON fr.event_id=e.id '
    '    INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis_places'' '
    '    INNER JOIN form_elements fe ON fe.form_id=f.id '
    '    INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''attend_school'' '
    '    INNER JOIN answers a ON a.event_id=e.id AND q.id=a.question_id AND a.text_answer=''Yes'' '
    '    WHERE e.parent_id=<%=event_id%> AND e.type=''PlaceEvent'' ORDER BY e.id LIMIT 1 ) e '
    '  INNER JOIN form_references fr ON fr.event_id=e.id '
    '  INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis_places'' '
    '  INNER JOIN form_elements fe ON fe.form_id=f.id '
    '  INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''trans_school'' '
    '  INNER JOIN answers a ON a.event_id=e.id AND q.id=a.question_id AND a.event_id=e.id ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'bus_num', 'sql lookup', 
    'SELECT a.text_answer FROM '
    '  (SELECT e.id FROM events e '
    '    INNER JOIN form_references fr ON fr.event_id=e.id '
    '    INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis_places'' '
    '    INNER JOIN form_elements fe ON fe.form_id=f.id '
    '    INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''attend_school'' '
    '    INNER JOIN answers a ON a.event_id=e.id AND q.id=a.question_id AND a.text_answer=''Yes'' '
    '    WHERE e.parent_id=<%=event_id%> AND e.type=''PlaceEvent'' ORDER BY e.id LIMIT 1 ) e '
    '  INNER JOIN form_references fr ON fr.event_id=e.id '
    '  INNER JOIN forms f ON f.id=fr.form_id AND f.short_name=''pertussis_places'' '
    '  INNER JOIN form_elements fe ON fe.form_id=f.id '
    '  INNER JOIN questions q ON q.form_element_id=fe.id AND q.short_name=''bus_num'' '
    '  INNER JOIN answers a ON a.event_id=e.id AND q.id=a.question_id AND a.event_id=e.id ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'attend_s', 'pertussis', 'attend_symp', NULL, NULL, 'check_box', 'Sleepover');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'attend_ca', 'pertussis', 'attend_symp', NULL, NULL, 'check_box', 'Church activities');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'attend_b', 'pertussis', 'attend_symp', NULL, NULL, 'check_box', 'babysit');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'attend_vh', 'pertussis', 'attend_symp', NULL, NULL, 'check_box', 'Visit hospital');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_num_prop', 'pertussis', 'ncontactsantibiotics', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'date_init', 'core', 'investigation_started_date', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'date_completed', 'core', 'investigation_completed_LHD_date', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'date_reported_dshs', 'core', '', NULL, NULL, 'date_now', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'name_investigator', 'core', 'investigator|best_name', NULL, NULL, NULL, NULL);
--Household contacts
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_control', 'pertussis', 'control_activities', NULL, NULL, NULL, NULL); 
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_name_1', 'sql lookup', 
    'SELECT concat(pe.first_name, '' '', pe.last_name) AS full_name FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 0'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_relation_1', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 0'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''pat_relationship'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_age_1', 'sql lookup', 
    'SELECT GREATEST(EXTRACT(YEAR FROM age(cast(pe.birth_date as date))), pe.approximate_age_no_birthday) AS age_years FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 0'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_hx_1', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 0'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''up_to_date_vax'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_hx_1', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 0'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''describe_other'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_sym_date_1', 'sql lookup', 
    'SELECT ''cough/'' || a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 0'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''cough_onset_date'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_prop_date_1', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 0'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''Prophylaxis'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_name_2', 'sql lookup', 
    'SELECT concat(pe.first_name, '' '', pe.last_name) AS full_name FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 1'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_relation_2', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 1'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''pat_relationship'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_age_2', 'sql lookup', 
    'SELECT GREATEST(EXTRACT(YEAR FROM age(cast(pe.birth_date as date))), pe.approximate_age_no_birthday) AS age_years FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 1'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_hx_2', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 1'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''up_to_date_vax'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_hx_2', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 1'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''describe_other'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_sym_date_2', 'sql lookup', 
    'SELECT ''cough/'' || a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 1'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''cough_onset_date'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_prop_date_2', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 1'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''Prophylaxis'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_name_3', 'sql lookup', 
    'SELECT concat(pe.first_name, '' '', pe.last_name) AS full_name FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 2'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_relation_3', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 2'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''pat_relationship'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_age_3', 'sql lookup', 
    'SELECT GREATEST(EXTRACT(YEAR FROM age(cast(pe.birth_date as date))), pe.approximate_age_no_birthday) AS age_years FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 2'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_hx_3', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 2'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''up_to_date_vax'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_hx_3', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 2'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''describe_other'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_sym_date_3', 'sql lookup', 
    'SELECT ''cough/'' || a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 2'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''cough_onset_date'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_prop_date_3', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 2'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''Prophylaxis'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_name_4', 'sql lookup', 
    'SELECT concat(pe.first_name, '' '', pe.last_name) AS full_name FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 3'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_relation_4', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 3'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''pat_relationship'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_age_4', 'sql lookup', 
    'SELECT GREATEST(EXTRACT(YEAR FROM age(cast(pe.birth_date as date))), pe.approximate_age_no_birthday) AS age_years FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 3'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_hx_4', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 3'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''up_to_date_vax'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_hx_4', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 3'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''describe_other'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_sym_date_4', 'sql lookup', 
    'SELECT ''cough/'' || a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 3'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''cough_onset_date'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_prop_date_4', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 3'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''Prophylaxis'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_name_5', 'sql lookup', 
    'SELECT concat(pe.first_name, '' '', pe.last_name) AS full_name FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 4'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_relation_5', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 4'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''pat_relationship'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_age_5', 'sql lookup', 
    'SELECT GREATEST(EXTRACT(YEAR FROM age(cast(pe.birth_date as date))), pe.approximate_age_no_birthday) AS age_years FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 4'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_hx_5', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 4'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''up_to_date_vax'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_hx_5', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 4'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''describe_other'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_sym_date_5', 'sql lookup', 
    'SELECT ''cough/'' || a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 4'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''cough_onset_date'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_prop_date_5', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 4'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''Prophylaxis'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_name_6', 'sql lookup', 
    'SELECT concat(pe.first_name, '' '', pe.last_name) AS full_name FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 5'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_relation_6', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 5'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''pat_relationship'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_age_6', 'sql lookup', 
    'SELECT GREATEST(EXTRACT(YEAR FROM age(cast(pe.birth_date as date))), pe.approximate_age_no_birthday) AS age_years FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 5'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_hx_6', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 5'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''up_to_date_vax'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_hx_6', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 5'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''describe_other'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_sym_date_6', 'sql lookup', 
    'SELECT ''cough/'' || a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 5'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''cough_onset_date'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_prop_date_6', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 5'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''Prophylaxis'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_name_7', 'sql lookup', 
    'SELECT concat(pe.first_name, '' '', pe.last_name) AS full_name FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 6'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_relation_7', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 6'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''pat_relationship'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_age_7', 'sql lookup', 
    'SELECT GREATEST(EXTRACT(YEAR FROM age(cast(pe.birth_date as date))), pe.approximate_age_no_birthday) AS age_years FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 6'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_hx_7', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 6'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''up_to_date_vax'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_hx_7', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 6'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''describe_other'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_sym_date_7', 'sql lookup', 
    'SELECT ''cough/'' || a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 6'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''cough_onset_date'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_prop_date_7', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 6'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''Prophylaxis'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_name_8', 'sql lookup', 
    'SELECT concat(pe.first_name, '' '', pe.last_name) AS full_name FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 7'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_relation_8', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 7'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''pat_relationship'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_age_8', 'sql lookup', 
    'SELECT GREATEST(EXTRACT(YEAR FROM age(cast(pe.birth_date as date))), pe.approximate_age_no_birthday) AS age_years FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 7'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_hx_8', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 7'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''up_to_date_vax'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_hx_8', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 7'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''describe_other'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_sym_date_8', 'sql lookup', 
    'SELECT ''cough/'' || a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 7'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''cough_onset_date'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_prop_date_8', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 7'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''Prophylaxis'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_name_9', 'sql lookup', 
    'SELECT concat(pe.first_name, '' '', pe.last_name) AS full_name FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 8'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_relation_9', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 8'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''pat_relationship'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_age_9', 'sql lookup', 
    'SELECT GREATEST(EXTRACT(YEAR FROM age(cast(pe.birth_date as date))), pe.approximate_age_no_birthday) AS age_years FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 8'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_hx_9', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 8'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''up_to_date_vax'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_hx_9', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 8'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''describe_other'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_sym_date_9', 'sql lookup', 
    'SELECT ''cough/'' || a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 8'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''cough_onset_date'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_prop_date_9', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 8'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''Prophylaxis'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_name_10', 'sql lookup', 
    'SELECT concat(pe.first_name, '' '', pe.last_name) AS full_name FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 9'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_relation_10', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 9'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''pat_relationship'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_age_10', 'sql lookup', 
    'SELECT GREATEST(EXTRACT(YEAR FROM age(cast(pe.birth_date as date))), pe.approximate_age_no_birthday) AS age_years FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 9'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_hx_10', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 9'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''up_to_date_vax'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_hx_10', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 9'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''describe_other'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_sym_date_10', 'sql lookup', 
    'SELECT ''cough/'' || a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 9'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''cough_onset_date'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_prop_date_10', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 9'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''Prophylaxis'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_name_11', 'sql lookup', 
    'SELECT concat(pe.first_name, '' '', pe.last_name) AS full_name FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 10'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_relation_11', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 10'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''pat_relationship'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_age_11', 'sql lookup', 
    'SELECT GREATEST(EXTRACT(YEAR FROM age(cast(pe.birth_date as date))), pe.approximate_age_no_birthday) AS age_years FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 10'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_hx_11', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 10'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''up_to_date_vax'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_hx_11', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 10'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''describe_other'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_sym_date_11', 'sql lookup', 
    'SELECT ''cough/'' || a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 10'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''cough_onset_date'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'hhc_prop_date_11', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 10'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''Prophylaxis'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Pertussis_case', 'vacc_household', 'pertussis', 'informedvax', NULL, NULL, 'check_box', 'Yes');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Pertussis_case', 'transmission_person', 'pertussis', 'informedtransmission', NULL, NULL, 'check_box', 'Yes');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Pertussis_case', 'day_school_restrict', 'pertussis', 'informedschool', NULL, NULL, 'check_box', 'Yes');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Pertussis_case', 'pat_guard_other', 'pertussis', 'informedother', NULL, NULL, NULL, NULL);
--Possible spread contacts
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_name_1', 'sql lookup', 
    'SELECT concat(pe.first_name, '' '', pe.last_name) AS full_name FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 0'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_rel_1', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 0'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''pat_relationship'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_age_1', 'sql lookup', 
    'SELECT GREATEST(EXTRACT(YEAR FROM age(cast(pe.birth_date as date))), pe.approximate_age_no_birthday) AS age_years FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 0'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_hx_1', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 0'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''up_to_date_vax'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_hx_1', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 0'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''describe_other'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_sym_date_1', 'sql lookup', 
    'SELECT ''cough/'' || a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 0'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''cough_onset_date'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_prop_date_1', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 0'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''Prophylaxis'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_name_2', 'sql lookup', 
    'SELECT concat(pe.first_name, '' '', pe.last_name) AS full_name FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 1'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_rel_2', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 1'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''pat_relationship'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_age_2', 'sql lookup', 
    'SELECT GREATEST(EXTRACT(YEAR FROM age(cast(pe.birth_date as date))), pe.approximate_age_no_birthday) AS age_years FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 1'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_hx_2', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 1'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''up_to_date_vax'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_hx_2', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 1'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''describe_other'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_sym_date_2', 'sql lookup', 
    'SELECT ''cough/'' || a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 1'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''cough_onset_date'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_prop_date_2', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 1'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''Prophylaxis'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_name_3', 'sql lookup', 
    'SELECT concat(pe.first_name, '' '', pe.last_name) AS full_name FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 2'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_rel_3', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 2'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''pat_relationship'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_age_3', 'sql lookup', 
    'SELECT GREATEST(EXTRACT(YEAR FROM age(cast(pe.birth_date as date))), pe.approximate_age_no_birthday) AS age_years FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 2'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_hx_3', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 2'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''up_to_date_vax'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_hx_3', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 2'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''describe_other'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_sym_date_3', 'sql lookup', 
    'SELECT ''cough/'' || a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 2'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''cough_onset_date'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_prop_date_3', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 2'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''Prophylaxis'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_name_4', 'sql lookup', 
    'SELECT concat(pe.first_name, '' '', pe.last_name) AS full_name FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 3'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_rel_4', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 3'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''pat_relationship'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_age_4', 'sql lookup', 
    'SELECT GREATEST(EXTRACT(YEAR FROM age(cast(pe.birth_date as date))), pe.approximate_age_no_birthday) AS age_years FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 3'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_hx_4', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 3'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''up_to_date_vax'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_hx_4', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 3'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''describe_other'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_sym_date_4', 'sql lookup', 
    'SELECT ''cough/'' || a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 3'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''cough_onset_date'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_prop_date_4', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 3'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''Prophylaxis'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_name_5', 'sql lookup', 
    'SELECT concat(pe.first_name, '' '', pe.last_name) AS full_name FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 4'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_rel_5', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 4'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''pat_relationship'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_age_5', 'sql lookup', 
    'SELECT GREATEST(EXTRACT(YEAR FROM age(cast(pe.birth_date as date))), pe.approximate_age_no_birthday) AS age_years FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 4'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_hx_5', 'sql lookup', 
    'SELECT a.text_answer FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 4'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''up_to_date_vax'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_hx_5', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 4'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''describe_other'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_sym_date_5', 'sql lookup', 
    'SELECT ''cough/'' || a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 4'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''cough_onset_date'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'psc_prop_date_5', 'sql lookup', 
    'SELECT a.text_answer FROM '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_name = ''contact_type'' AND ec.code_description!=''Household'' AND (ec.id=pc.contact_type_id OR (pc.contact_type_id IS NULL AND ec.code_description=''Other'')) '
          'WHERE  e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 4'
          ') e '
          'INNER JOIN form_references fr ON fr.event_id=e.id '
          'INNER JOIN forms f ON f.short_name=''pertussis_contacts'' AND f.id=fr.form_id '
          'INNER JOIN form_elements fe ON fe.form_id=f.id '
          'INNER JOIN questions q ON q.short_name=''Prophylaxis'' AND q.form_element_id=fe.id '
          'INNER JOIN answers a ON a.event_id=e.id AND a.question_id=q.id ',
    NULL, NULL, NULL, NULL);
-- comments AND notes
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'comments_notes', 'sql lookup', 
    'SELECT to_char(pe.encounter_date, ''YYYY-MM-DD''), pe.description FROM participations_encounters pe '
      'INNER JOIN events e ON e.type=''EncounterEvent'' AND e.parent_id=<%=event_id%> AND pe.id = e.participations_encounter_id '
      'ORDER BY pe.encounter_date',
    NULL, '\n', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Pertussis_case', 'comments_notes', 'sql lookup', 
    'SELECT to_char(created_at, ''YYYY-MM-DD''), note FROM notes WHERE note_type=''clinical'' AND struckthrough != TRUE AND event_id=<%=event_id%> ORDER BY id',
    NULL, NULL, NULL, NULL);





