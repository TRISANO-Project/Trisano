--If you are replacing all the mappings for this disease, you must first delete the existing mapping.
--This is commented out so as not be run inadvertently
--clear existing
--DELETE FROM generate_pdf_mappings WHERE template_pdf_name='Enteric';

INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('STEC', 'nbs_eid', 'sql lookup', 'SELECT nbs_id FROM events WHERE id=<%=event_id%>', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'date_case_report', 'core', 'first_reported_PH_date', NULL, NULL, 'replace', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'date_reported_dshs', 'core', 'review_completed_by_state_date', NULL, NULL, 'date_now', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'record_number', 'sql lookup', 
    'SELECT record_number from events where id=<%=event_id%> ', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'outbreak_linked', 'core', 'outbreak_associated_id', 'yesno', NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'outbreak_name', 'core', 'outbreak_name', NULL, NULL, NULL, NULL);

--demographics
--patient information
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'p_l_name', 'core','interested_party|person_entity|person|last_name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC','p_f_name', 'core','interested_party|person_entity|person|first_name', NULL, NULL, NULL, NULL);
--address
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', NULL, 'core','address|street_number', NULL, ' ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', NULL, 'core','address|street_name', NULL, ' #', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'p_addr', 'core','address|unit_number', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'p_city', 'core', 'address|city', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'p_county', 'core', 'address|county_id', 'county', NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'p_zip','core', 'address|postal_code', NULL, NULL, NULL, NULL);
--phone number/email
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', NULL, 'core','interested_party|person_entity|telephones|area_code', NULL, '-', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', NULL, 'core','interested_party|person_entity|telephones|phone_number', NULL, ' x', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'p_phone', 'core','interested_party|person_entity|telephones|extension', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'p_email', 'core','interested_party|person_entity|email_addresses|email_address', NULL, NULL, NULL, NULL);
--age/gender
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'p_dob', 'core', 'interested_party|person_entity|person|birth_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('STEC', 'p_age', 'sql lookup',
    'SELECT CASE '
    '    WHEN pe.birth_date IS NOT NULL THEN date_part(''year'', age(pe.birth_date)) '
    '    WHEN pe.approximate_age_no_birthday IS NOT NULL AND pe.age_type_id=173 THEN pe.approximate_age_no_birthday '
    '    ELSE NULL '
    '  END '
    '  FROM participations p '
    '    INNER JOIN people pe ON pe.entity_id = p.primary_entity_id '
    '  WHERE p.type=''InterestedParty'' AND p.event_id=<%=event_id%> ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'p_gender', 'core', 'interested_party|person_entity|person|birth_gender_id', 'gender', NULL, NULL, NULL);
--race
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'race_w', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'W');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'race_b', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'B');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'race_a', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'A');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'race_h', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'H');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'race_ai_an', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'AI_AN');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'race_u', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'UNK');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'race_o', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'Other');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'race_other', 'adddemog', 'otherracespecify', NULL, NULL, NULL, NULL);
  
--ethnicity
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'ethnicity', 'core', 'interested_party|person_entity|person|ethnicity_id', 'ethnicity', NULL, NULL, NULL);

-- occupation/day care
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', NULL, 'core', 'interested_party|risk_factor|occupation', NULL, ' / ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', NULL, 'sql lookup', 
    'SELECT pl.name FROM codes c '
    '  INNER JOIN events e on e.parent_id=<%=event_id%> AND e.type=''PlaceEvent'' '
    '  INNER JOIN participations p ON p.event_id=e.id and p.type=''InterestedPlace'' '
    '  INNER JOIN places pl ON pl.entity_id=p.primary_entity_id '
    '  INNER JOIN places_types pt ON pt.place_id=pl.id '
    '  WHERE c.id=pt.type_id AND c.code_description=''Employer'' ORDER BY p.id LIMIT 1 ',
  NULL, ' / ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', NULL, 'daycare', 'attendfacility_name', NULL, ' / ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'occ_dc', 'daycare', 'daycareaddress', NULL, NULL, NULL, NULL);

--investigation summary 
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'fcs', 'core', 'lhd_case_status_id', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'reported_by', 'sql lookup', 
    'SELECT pe.first_name || '' '' || pe.last_name FROM people pe '
      'INNER JOIN participations p ON p.type = ''Reporter'' AND pe.entity_id=p.secondary_entity_id AND p.event_id=<%=event_id%>  '
      'ORDER BY p.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'ra_phone', 'core', 'reporting_agency|place_entity|telephones|phone_number', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'investigator', 'core', 'investigator|best_name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'date_reported', 'core', 'first_reported_PH_date', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'date_init', 'core', 'investigation_started_date', NULL, NULL, 'date_m_d_y', NULL);

--clinical information
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'date_onset', 'core', 'disease_event|disease_onset_date', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'bl_dia', 'Ent_Sym', 'blood_diarrhea', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'diarrhea', 'Ent_Sym', 'diarrhea', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'fever', 'Ent_Sym', 'fever', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'highest_temp', 'Ent_Sym', 'Highest_temp', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'vomit', 'Ent_Sym', 'vomit', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'nausea', 'Ent_Sym', 'nausea', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'abd_cramp', 'Ent_Sym', 'cramp', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'oth_sym', 'Ent_Sym', 'othersympt', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'other_symptoms', 'Ent_Sym', 'describeother', NULL, NULL, NULL, NULL);

--HUS
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'hus', 'shiga_toxin_producing_e_coli', 'hus_diag', NULL, NULL, 'check_box', 'Hemolytic uremic syndrome (HUS) (postdiarrheal)');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'acute_anemia', 'shiga_toxin_producing_e_coli', 'hus_diag', NULL, NULL, 'check_box', 'Acute anemia');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'renal', 'shiga_toxin_producing_e_coli', 'hus_diag', NULL, NULL, 'check_box', 'Renal (kidney) injury or failure');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'hematuria', 'shiga_toxin_producing_e_coli', 'hus_diag', NULL, NULL, 'check_box', 'Hematuria (blood in urine)');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'proteinuria', 'shiga_toxin_producing_e_coli', 'hus_diag', NULL, NULL, 'check_box', 'Proteinuria (excess protein in urine)');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'elevated_creatinine', 'shiga_toxin_producing_e_coli', 'hus_diag', NULL, NULL, 'check_box', 'Elevated creatinine');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'ec_value', 'shiga_toxin_producing_e_coli', 'ec_value', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'microangio', 'shiga_toxin_producing_e_coli', 'hus_diag', NULL, NULL, 'check_box', 'Microangiopathic changes on peripheral blood smear');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'dialysis', 'shiga_toxin_producing_e_coli', 'hus_diag', NULL, NULL, 'check_box', 'Dialysis required');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'coagulopathy', 'shiga_toxin_producing_e_coli', 'hus_diag', NULL, NULL, 'check_box', 'Coagulopathy (platelets < 100,000)');

--hospitialized
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'hospitalized', 'core', 'disease_event|hospitalized_id', 'yesno', NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'date_admit', 'core', 'hospitalization_facilities|hospitals_participation|admission_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'date_discharge', 'core', 'hospitalization_facilities|hospitals_participation|discharge_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'hospital', 'sql lookup',
    'SELECT pl.name FROM places pl '
    '  INNER JOIN participations p ON p.secondary_entity_id=pl.entity_id AND p.type=''HospitalizationFacility'' AND p.event_id=<%=event_id%> '
    '  LEFT JOIN addresses a ON a.entity_id=pl.entity_id '
    '  ORDER BY p.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'hospital_city', 'sql lookup',
    'SELECT a.city FROM places pl '
    '  INNER JOIN participations p ON p.secondary_entity_id=pl.entity_id AND p.type=''HospitalizationFacility'' AND p.event_id=<%=event_id%> '
    '  LEFT JOIN addresses a ON a.entity_id=pl.entity_id '
    '  ORDER BY p.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);

--died
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'died', 'Death_details', 'Died_of_this_illness', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'date_death', 'core', 'interested_party|person_entity|person|date_of_death', NULL, NULL, 'date_m_d_y', NULL);
  
--treatments
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'anti_med', 'sql lookup', 
    'SELECT ''Yes'' FROM treatments t '
    '  INNER JOIN participations_treatments pt ON pt.treatment_id=t.id '
    '  INNER JOIN participations p ON pt.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'treatment_1', 'sql lookup', 
    'SELECT t.treatment_name FROM treatments t '
    '  INNER JOIN participations_treatments pt ON pt.treatment_id=t.id '
    '  INNER JOIN participations p ON pt.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY p.id OFFSET 0 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'treatment_1_start', 'sql lookup', 
    'SELECT pt.treatment_date FROM treatments t '
    '  INNER JOIN participations_treatments pt ON pt.treatment_id=t.id '
    '  INNER JOIN participations p ON pt.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY p.id OFFSET 0 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'treatment_1_end', 'sql lookup', 
    'SELECT pt.stop_treatment_date FROM treatments t '
    '  INNER JOIN participations_treatments pt ON pt.treatment_id=t.id '
    '  INNER JOIN participations p ON pt.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY p.id OFFSET 0 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'treatment_2', 'sql lookup', 
    'SELECT t.treatment_name FROM treatments t '
    '  INNER JOIN participations_treatments pt ON pt.treatment_id=t.id '
    '  INNER JOIN participations p ON pt.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY p.id OFFSET 1 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'treatment_2_start', 'sql lookup', 
    'SELECT pt.treatment_date FROM treatments t '
    '  INNER JOIN participations_treatments pt ON pt.treatment_id=t.id '
    '  INNER JOIN participations p ON pt.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY p.id OFFSET 1 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'treatment_2_end', 'sql lookup', 
    'SELECT pt.stop_treatment_date FROM treatments t '
    '  INNER JOIN participations_treatments pt ON pt.treatment_id=t.id '
    '  INNER JOIN participations p ON pt.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY p.id OFFSET 1 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'treatment_3', 'sql lookup', 
    'SELECT t.treatment_name FROM treatments t '
    '  INNER JOIN participations_treatments pt ON pt.treatment_id=t.id '
    '  INNER JOIN participations p ON pt.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY p.id OFFSET 2 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'treatment_3_start', 'sql lookup', 
    'SELECT pt.treatment_date FROM treatments t '
    '  INNER JOIN participations_treatments pt ON pt.treatment_id=t.id '
    '  INNER JOIN participations p ON pt.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY p.id OFFSET 2 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'treatment_3_end', 'sql lookup', 
    'SELECT pt.stop_treatment_date FROM treatments t '
    '  INNER JOIN participations_treatments pt ON pt.treatment_id=t.id '
    '  INNER JOIN participations p ON pt.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY p.id OFFSET 2 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
  
--laboratory
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'spec_src', 'sql lookup',
    'SELECT ec.code_description FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  INNER JOIN external_codes ec ON ec.id = lr.specimen_source_id '
    '  WHERE lr.specimen_source_id IS NOT NULL ORDER BY lr.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lab_coll', 'sql lookup',
    'SELECT lr.collection_date FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  WHERE lr.collection_date IS NOT NULL ORDER BY lr.collection_date ASC LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lab_name', 'sql lookup', 
      'SELECT pl.name FROM lab_results lr '
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN places pl ON pl.entity_id=p.secondary_entity_id ORDER BY lr.id DESC LIMIT 1',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sent_dshs', 'sql lookup', 
      'SELECT lr.specimen_sent_to_state_id FROM lab_results lr '
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN places pl ON pl.entity_id=p.secondary_entity_id ORDER BY lr.id DESC LIMIT 1',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'ecoli_only', 'sql lookup', 
      'SELECT ''On'' FROM lab_results lr '
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN organisms o ON o.id = lr.organism_id '
        'WHERE o.organism_name=''Enterohemorrhagic Escherichia coli (STEC)'' LIMIT 1',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'o157', 'sql lookup', 
      'SELECT ''On'' FROM lab_results lr '
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN organisms o ON o.id = lr.organism_id '
        'WHERE o.organism_name=''Escherichia coli O157:non-motile'' LIMIT 1',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'o157h7', 'sql lookup', 
      'SELECT ''On'' FROM lab_results lr '
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN organisms o ON o.id = lr.organism_id '
        'WHERE o.organism_name=''Escherichia coli O157:H7'' LIMIT 1',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'not_o157', 'sql lookup', 
      'SELECT ''On'' FROM lab_results lr '
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN organisms o ON o.id = lr.organism_id '
        'WHERE o.organism_name=''Escherichia coli serogroup O121:H19'' LIMIT 1',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'serotype', 'sql lookup', 
      'SELECT ''O121:H19'' FROM lab_results lr '
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN organisms o ON o.id = lr.organism_id '
        'WHERE o.organism_name=''Escherichia coli serogroup O121:H19'' LIMIT 1',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'other_species', 'sql lookup', 
      'SELECT o.organism_name FROM lab_results lr '
        'INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id = p.id AND p.event_id=<%=event_id%> '
        'INNER JOIN organisms o ON o.id = lr.organism_id ORDER BY lr.id LIMIT 1',
    NULL, NULL, NULL, NULL);

--public health concerns
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'eaf', 'enteric_events', 'Employed_as_food_worker', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'eah', 'enteric_events', 'Employed_as_health_care_worker', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'eic', 'enteric_events', 'Employed_in_child_care', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'hhc', 'enteric_events', 'HH_member', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'gm', 'enteric_events', 'group_meal', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'gm_date', 'enteric_events', 'date_gm', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'gm_loc', 'enteric_events', 'location_gm', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'gm_food', 'enteric_events', 'foods_gm', NULL, NULL, NULL, NULL);

--public health actions  
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'pha_e', 'enteric_events', 'Public_Health_Actions', NULL, NULL, 'check_box', 'Exclude probable and confirmed cases in sensitive occupations/situations (HCW, food, child care) until 2 negative stools');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'pha_cc', 'enteric_events', 'Public_Health_Actions', NULL, NULL, 'check_box', 'Child care facility information provided to licensing as needed if facility is non-compliant');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'pha_fbi', 'enteric_events', 'Public_Health_Actions', NULL, NULL, 'check_box', 'FBI/restaurant info sent to sanitarian');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'pha_epr', 'enteric_events', 'Public_Health_Actions', NULL, NULL, 'check_box', 'Education provided on route of transmission');

--Contacts
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'num_contacts', 'sql lookup',
    'SELECT Count(*) '
      'FROM events e  '
      'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
      'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
      'WHERE  e.deleted_at IS NULL AND e.parent_id=<%=event_id%> ', 
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'hhc_last1', 'sql lookup', 
    'SELECT pe.last_name FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE e.deleted_at IS NULL AND e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 0'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'hhc_first1', 'sql lookup', 
    'SELECT pe.first_name FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE e.deleted_at IS NULL AND e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 0'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'hhc_dob1', 'sql lookup', 
    'SELECT pe.birth_date FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE e.deleted_at IS NULL AND e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 0'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'hhc_onset1', 'sql lookup', 
    'SELECT event_onset_date FROM events e '
      'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
      'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
      'WHERE e.deleted_at IS NULL AND e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 0 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'hhc_last2', 'sql lookup', 
    'SELECT pe.last_name FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE e.deleted_at IS NULL AND e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 1'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'hhc_first2', 'sql lookup', 
    'SELECT pe.first_name FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE e.deleted_at IS NULL AND e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 1'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'hhc_dob2', 'sql lookup', 
    'SELECT pe.birth_date FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE e.deleted_at IS NULL AND e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 1'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'hhc_onset2', 'sql lookup', 
    'SELECT event_onset_date FROM events e '
      'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
      'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
      'WHERE e.deleted_at IS NULL AND e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'hhc_last3', 'sql lookup', 
    'SELECT pe.last_name FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE e.deleted_at IS NULL AND e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 2'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'hhc_first3', 'sql lookup', 
    'SELECT pe.first_name FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE e.deleted_at IS NULL AND e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 2'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'hhc_dob3', 'sql lookup', 
    'SELECT pe.birth_date FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE e.deleted_at IS NULL AND e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 2'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'hhc_onset3', 'sql lookup', 
    'SELECT event_onset_date FROM events e '
      'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
      'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
      'WHERE e.deleted_at IS NULL AND e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 2 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'hhc_last4', 'sql lookup', 
    'SELECT pe.last_name FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE e.deleted_at IS NULL AND e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 3'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'hhc_first4', 'sql lookup', 
    'SELECT pe.first_name FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE e.deleted_at IS NULL AND e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 3'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'hhc_dob4', 'sql lookup', 
    'SELECT pe.birth_date FROM  '
        '(SELECT e.id, e.parent_id '
          'FROM events e  '
          'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
          'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
          'WHERE e.deleted_at IS NULL AND e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 3'
          ') e '
          'INNER JOIN participations p ON p.type=''InterestedParty'' AND p.event_id=e.id '
          'INNER JOIN people pe ON pe.entity_id=p.primary_entity_id ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'hhc_onset4', 'sql lookup', 
    'SELECT event_onset_date FROM events e '
      'INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
      'INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
      'WHERE e.deleted_at IS NULL AND e.parent_id=<%=event_id%> ORDER BY e.id LIMIT 1 OFFSET 3 ',
  NULL, NULL, NULL, NULL);
 
--infection timeline
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'onset_minus_7', 'sql lookup',
    'SELECT event_onset_date-7 '
    ' FROM events WHERE id=<%=event_id%> ',
  NULL, NULL, NULL, NULL);

--Travel
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'travel', 'repeatingtravel', 'travelprior', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', NULL, 'repeatingtravel', 'destination', NULL, ':', 'filler,0', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', NULL, 'repeatingtravel', 'arrivaldate', NULL, '-', 'date_m_d_y,0', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', NULL, 'repeatingtravel', 'departuredate', NULL, ' | ', 'date_m_d_y,0', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', NULL, 'repeatingtravel', 'destination', NULL, ':', 'filler,1', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', NULL, 'repeatingtravel', 'arrivaldate', NULL, '-', 'date_m_d_y,1', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', NULL, 'repeatingtravel', 'departuredate', NULL, ' | ', 'date_m_d_y,1', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', NULL, 'repeatingtravel', 'destination', NULL, ':', 'filler,2', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', NULL, 'repeatingtravel', 'arrivaldate', NULL, '-', 'date_m_d_y,2', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'travel_dates_locs', 'repeatingtravel', 'departuredate', NULL, NULL, 'date_m_d_y,2', NULL);

--water source
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'source_water', 'enteric_water', 'Drinking_water_source', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'rwe', 'enteric_water', 'Recreational_water_exposure', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'we_dates_locs', 'enteric_water', 'Locations_dates', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'wse', 'enteric_water', 'recwatertype', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'wse_other', 'enteric_water', 'otherrecwater', NULL, NULL, NULL, NULL);

--Food Exposures
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'pgb', 'shiga_toxin_producing_e_coli', 'pgb', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'pgb_dates_locs', 'shiga_toxin_producing_e_coli', 'pgb_dates_locs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'egb', 'shiga_toxin_producing_e_coli', 'egb', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'egb_dates_locs', 'shiga_toxin_producing_e_coli', 'egb_dates_locs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'pfgb', 'shiga_toxin_producing_e_coli', 'pfgb', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'pfgb_dates_locs', 'shiga_toxin_producing_e_coli', 'pfgb_dates_locs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'egbo', 'shiga_toxin_producing_e_coli', 'egbo', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'egbo_dates_locs', 'shiga_toxin_producing_e_coli', 'egbo_dates_locs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'pob', 'shiga_toxin_producing_e_coli', 'pob', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'pob_dates_locs', 'shiga_toxin_producing_e_coli', 'pob_dates_locs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'eob', 'shiga_toxin_producing_e_coli', 'eob', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'eob_dates_locs', 'shiga_toxin_producing_e_coli', 'eob_dates_locs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'eobo', 'shiga_toxin_producing_e_coli', 'eobo', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'eobo_dates_locs', 'shiga_toxin_producing_e_coli', 'eobo_dates_locs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'bis', 'shiga_toxin_producing_e_coli', 'bis', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'bis_dates_locs', 'shiga_toxin_producing_e_coli', 'bis_dates_locs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'vfb', 'shiga_toxin_producing_e_coli', 'vfb', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'vfb_dates_locs', 'shiga_toxin_producing_e_coli', 'vfb_dates_locs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'dfm', 'shiga_toxin_producing_e_coli', 'dfm', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'dfm_dates_locs', 'shiga_toxin_producing_e_coli', 'dfm_dates_locs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'omp', 'shiga_toxin_producing_e_coli', 'omp', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'omp_dates_locs', 'shiga_toxin_producing_e_coli', 'omp_dates_locs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'udp', 'shiga_toxin_producing_e_coli', 'udp', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'udp_dates_locs', 'shiga_toxin_producing_e_coli', 'udp_dates_locs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'cdp', 'shiga_toxin_producing_e_coli', 'cdp', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'cdp_dates_locs', 'shiga_toxin_producing_e_coli', 'cdp_dates_locs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'agc', 'shiga_toxin_producing_e_coli', 'agc', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'agc_dates_locs', 'shiga_toxin_producing_e_coli', 'agc_dates_locs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'ujc', 'shiga_toxin_producing_e_coli', 'ujc', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'ujc_dates_locs', 'shiga_toxin_producing_e_coli', 'ujc_dates_locs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'eil', 'shiga_toxin_producing_e_coli', 'eil', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'eil_dates_locs', 'shiga_toxin_producing_e_coli', 'eil_dates_locs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'erl', 'shiga_toxin_producing_e_coli', 'erl', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'erl_dates_locs', 'shiga_toxin_producing_e_coli', 'erl_dates_locs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'es', 'shiga_toxin_producing_e_coli', 'es', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'es_dates_locs', 'shiga_toxin_producing_e_coli', 'es_dates_locs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'eolg', 'shiga_toxin_producing_e_coli', 'eolg', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'eolg_dates_locs', 'shiga_toxin_producing_e_coli', 'eolg_dates_locs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'esp', 'shiga_toxin_producing_e_coli', 'esp', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'esp_dates_locs', 'shiga_toxin_producing_e_coli', 'esp_dates_locs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'vpz', 'shiga_toxin_producing_e_coli', 'vpz', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'vpz_dates_locs', 'shiga_toxin_producing_e_coli', 'vpz_dates_locs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'vwl', 'shiga_toxin_producing_e_coli', 'vwl', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'vwl_dates_locs', 'shiga_toxin_producing_e_coli', 'vwl_dates_locs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'vcs', 'shiga_toxin_producing_e_coli', 'vcs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'vcs_dates_locs', 'shiga_toxin_producing_e_coli', 'vcs_dates_locs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'cwa', 'shiga_toxin_producing_e_coli', 'cwa', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'cwa_dates_locs', 'shiga_toxin_producing_e_coli', 'cwa_dates_locs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'cwaf', 'shiga_toxin_producing_e_coli', 'cwaf', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'cwaf_dates_locs', 'shiga_toxin_producing_e_coli', 'cwaf_dates_locs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'cwap', 'shiga_toxin_producing_e_coli', 'cwap', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'cwap_dates_locs', 'shiga_toxin_producing_e_coli', 'cwap_dates_locs', NULL, NULL, NULL, NULL);

--animal exposure
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'vpz', 'shiga_toxin_producing_e_coli', 'vpz', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'vpz_dates_locs', 'shiga_toxin_producing_e_coli', 'vpz_dates_locs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'vwl', 'shiga_toxin_producing_e_coli', 'vwl', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'vwl_dates_locs', 'shiga_toxin_producing_e_coli', 'vwl_dates_locs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'vcs', 'shiga_toxin_producing_e_coli', 'vcs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'vcs_dates_locs', 'shiga_toxin_producing_e_coli', 'vcs_dates_locs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'cwa', 'shiga_toxin_producing_e_coli', 'cwa', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'cwa_dates_locs', 'shiga_toxin_producing_e_coli', 'cwa_dates_locs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'cwaf', 'shiga_toxin_producing_e_coli', 'cwaf', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'cwaf_dates_locs', 'shiga_toxin_producing_e_coli', 'cwaf_dates_locs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'cwap', 'shiga_toxin_producing_e_coli', 'cwap', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'cwap_dates_locs', 'shiga_toxin_producing_e_coli', 'cwap_dates_locs', NULL, NULL, NULL, NULL);

--Food History
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sl_gss', 'shiga_toxin_producing_e_coli', 'sl', NULL, NULL, 'check_box', 'Grocery store, supermarket');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sl_ws', 'shiga_toxin_producing_e_coli', 'sl', NULL, NULL, 'check_box', 'Warehouse store');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sl_smm', 'shiga_toxin_producing_e_coli', 'sl', NULL, NULL, 'check_box', 'Small/mini market');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sl_em', 'shiga_toxin_producing_e_coli', 'sl', NULL, NULL, 'check_box', 'Ethnic market');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sl_hfs', 'shiga_toxin_producing_e_coli', 'sl', NULL, NULL, 'check_box', 'Health food store, co-op');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sl_fm', 'shiga_toxin_producing_e_coli', 'sl', NULL, NULL, 'check_box', 'Fish market');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sl_b', 'shiga_toxin_producing_e_coli', 'sl', NULL, NULL, 'check_box', 'Butcher');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sl_fmrs', 'shiga_toxin_producing_e_coli', 'sl', NULL, NULL, 'check_box', 'Farmers market, roadside stand');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sl_o', 'shiga_toxin_producing_e_coli', 'sl', NULL, NULL, 'check_box', 'Other');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sl_o_name', 'shiga_toxin_producing_e_coli', 'sl_o_name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sl1', 'shiga_toxin_producing_e_coli', 'sl1', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sl2', 'shiga_toxin_producing_e_coli', 'sl2', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sl3', 'shiga_toxin_producing_e_coli', 'sl3', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sl4', 'shiga_toxin_producing_e_coli', 'sl4', NULL, NULL, NULL, NULL);

INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'fpo_ffc', 'shiga_toxin_producing_e_coli', 'fpo', NULL, NULL, 'check_box', 'National fast food chain');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'fpo_m', 'shiga_toxin_producing_e_coli', 'fpo', NULL, NULL, 'check_box', 'Mexican');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'fpo_i', 'shiga_toxin_producing_e_coli', 'fpo', NULL, NULL, 'check_box', 'Italian');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'fpo_s', 'shiga_toxin_producing_e_coli', 'fpo', NULL, NULL, 'check_box', 'Seafood');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'fpo_jcc', 'shiga_toxin_producing_e_coli', 'fpo', NULL, NULL, 'check_box', 'Jamaican, Cuban, Caribbean');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'fpo_sg', 'shiga_toxin_producing_e_coli', 'fpo', NULL, NULL, 'check_box', 'Steakhouse, grill');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'fpo_bbq', 'shiga_toxin_producing_e_coli', 'fpo', NULL, NULL, 'check_box', 'BBQ');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'fpo_b', 'shiga_toxin_producing_e_coli', 'fpo', NULL, NULL, 'check_box', 'Buffet');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'fpo_vv', 'shiga_toxin_producing_e_coli', 'fpo', NULL, NULL, 'check_box', 'Vegetarian, vegan');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'fpo_meaa', 'shiga_toxin_producing_e_coli', 'fpo', NULL, NULL, 'check_box', 'Middle Eastern, Arabic, African');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'fpo_dss', 'shiga_toxin_producing_e_coli', 'fpo', NULL, NULL, 'check_box', 'Deli, sandwich shop');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'fpo_dc', 'shiga_toxin_producing_e_coli', 'fpo', NULL, NULL, 'check_box', 'Diner, café');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'fpo_ce', 'shiga_toxin_producing_e_coli', 'fpo', NULL, NULL, 'check_box', 'Catered event');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'fpo_to', 'shiga_toxin_producing_e_coli', 'fpo', NULL, NULL, 'check_box', 'Take-out');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'fpo_a', 'shiga_toxin_producing_e_coli', 'fpo', NULL, NULL, 'check_box', 'Asian (Chinese, Japanese, Indian)');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'fpo_bb', 'shiga_toxin_producing_e_coli', 'fpo', NULL, NULL, 'check_box', 'Breakfast, brunch');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'fpo_si', 'shiga_toxin_producing_e_coli', 'fpo', NULL, NULL, 'check_box', 'School, institution');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'fpo_cws', 'shiga_toxin_producing_e_coli', 'fpo', NULL, NULL, 'check_box', 'Cafeteria (work, school)');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'fpo_o', 'shiga_toxin_producing_e_coli', 'fpo', NULL, NULL, 'check_box', 'Other');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'fpo_o_name', 'shiga_toxin_producing_e_coli', 'fpo_o_name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'fpo_o_details', 'shiga_toxin_producing_e_coli', 'fpo_o_details', NULL, NULL, NULL, NULL);

INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'time_onset', 'shiga_toxin_producing_e_coli', 'time_onset', NULL, NULL, NULL, NULL);

INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_loc_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''0'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_home'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_1_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''0'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_1_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''0'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_1_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''0'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_1_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''0'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_4'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_loc_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''0'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_home'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_1_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''0'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_1_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''0'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_1_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''0'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_1_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''0'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_4'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_loc_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''0'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_home'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_1_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''0'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_1_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''0'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_1_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''0'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_1_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''0'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_4'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_1_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''0'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_1_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''0'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_1_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''0'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_1_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''0'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_4'' ',
  NULL, NULL, NULL, NULL);  
  INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_1_5', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''0'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_5'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_loc_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''1'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_home'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_2_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''1'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_2_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''1'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_2_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''1'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_2_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''1'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_4'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_loc_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''1'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_home'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_2_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''1'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_2_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''1'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_2_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''1'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_2_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''1'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_4'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_loc_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''1'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_home'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_2_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''1'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_2_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''1'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_2_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''1'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_2_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''1'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_4'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_2_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''1'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_2_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''1'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_2_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''1'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_2_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''1'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_4'' ',
  NULL, NULL, NULL, NULL);  INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_2_5', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''1'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_5'' ',
  NULL, NULL, NULL, NULL);    
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_loc_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''2'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_home'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_3_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''2'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_3_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''2'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_3_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''2'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_3_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''2'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_4'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_loc_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''2'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_home'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_3_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''2'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_3_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''2'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_3_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''2'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_3_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''2'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_4'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_loc_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''2'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_home'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_3_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''2'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_3_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''2'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_3_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''2'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_3_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''2'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_4'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_3_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''2'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_3_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''2'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_3_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''2'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_3_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''2'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_4'' ',
  NULL, NULL, NULL, NULL);  
  INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_3_5', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''2'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_5'' ',
  NULL, NULL, NULL, NULL);  
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_loc_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''3'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_home'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_4_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''3'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_4_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''3'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_4_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''3'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_4_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''3'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_4'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_loc_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''3'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_home'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_4_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''3'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_4_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''3'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_4_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''3'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_4_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''3'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_4'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_loc_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''3'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_home'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_4_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''3'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_4_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''3'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_4_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''3'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_4_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''3'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_4'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_4_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''3'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_4_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''3'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_4_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''3'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_4_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''3'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_4'' ',
  NULL, NULL, NULL, NULL);  
  INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_4_5', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''3'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_5'' ',
  NULL, NULL, NULL, NULL);  
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_loc_5', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''4'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_home'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_5_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''4'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_5_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''4'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_5_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''4'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_5_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''4'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_4'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_loc_5', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''4'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_home'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_5_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''4'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_5_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''4'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_5_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''4'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_5_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''4'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_4'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_loc_5', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''4'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_home'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_5_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''4'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_5_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''4'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_5_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''4'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_5_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''4'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_4'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_5_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''4'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_5_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''4'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_5_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''4'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_5_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''4'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_4'' ',
  NULL, NULL, NULL, NULL);  
  INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_5_5', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''4'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_5'' ',
  NULL, NULL, NULL, NULL);  
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_loc_6', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''5'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_home'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_6_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''5'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_6_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''5'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_6_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''5'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_6_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''5'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_4'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_loc_6', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''5'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_home'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_6_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''5'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_6_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''5'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_6_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''5'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_6_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''5'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_4'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_loc_6', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''5'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_home'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_6_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''5'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_6_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''5'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_6_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''5'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_6_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''5'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_4'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_6_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''5'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_6_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''5'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_6_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''5'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_6_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''5'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_4'' ',
  NULL, NULL, NULL, NULL);  
  INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_6_5', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''5'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_5'' ',
  NULL, NULL, NULL, NULL);  
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_loc_7', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''6'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_home'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_7_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''6'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_7_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''6'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_7_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''6'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_7_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''6'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_4'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_loc_7', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''6'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_home'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_7_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''6'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_7_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''6'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_7_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''6'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_7_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''6'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_4'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_loc_7', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''6'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_home'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_7_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''6'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_7_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''6'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_7_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''6'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_7_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''6'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_4'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_7_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''6'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_7_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''6'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_7_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''6'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_7_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''6'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_4'' ',
  NULL, NULL, NULL, NULL);  
  INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_7_5', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''6'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_5'' ',
  NULL, NULL, NULL, NULL);  
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_loc_8', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''7'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_home'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_8_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''7'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_8_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''7'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_8_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''7'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'br_8_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''7'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''br_4'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_loc_8', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''7'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_home'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_8_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''7'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_8_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''7'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_8_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''7'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'lu_8_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''7'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''lu_4'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_loc_8', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''7'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_home'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_8_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''7'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_8_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''7'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_8_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''7'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'di_8_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''7'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''di_4'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_8_1', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''7'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_1'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_8_2', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''7'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_2'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_8_3', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''7'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_3'' ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_8_4', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''7'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_4'' ',
  NULL, NULL, NULL, NULL);  
  INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'sn_8_5', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN (SELECT a.repeater_form_object_id, fr.id AS form_reference_id FROM answers a '
        'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
        'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
        'INNER JOIN form_elements fe ON fe.form_id = f.id '
        'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''dbo''  '
        'WHERE a.event_id=<%=event_id%> AND a.text_answer=''7'' ORDER BY a.id ASC LIMIT 1) tmp ON tmp.repeater_form_object_id = a.repeater_form_object_id '
      'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
      'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''shiga_toxin_producing_e_coli'' '
      'INNER JOIN form_elements fe ON fe.form_id = f.id '
      'INNER JOIN questions q ON q.form_element_id = fe.id AND q.id = a.question_id AND q.short_name = ''sn_5'' ',
  NULL, NULL, NULL, NULL);  

--comments
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('STEC', 'comments', 'sql lookup', 
    'SELECT to_char(created_at, ''YYYY-MM-DD''), note FROM notes WHERE note_type=''clinical'' AND struckthrough != TRUE AND event_id=<%=event_id%> ORDER BY id',
    NULL, NULL, NULL, NULL);
