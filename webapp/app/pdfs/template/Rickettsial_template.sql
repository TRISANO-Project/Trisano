﻿--If you are replacing all the mappings for this disease, you must first delete the existing mapping.
--This is commented out so as not be run inadvertently
--clear existing
--DELETE FROM generate_pdf_mappings WHERE template_pdf_name='Rickettsial';

--new
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Rickettsial', 'nbs_pid', 'sql lookup', 
    'SELECT pe.nbs_id FROM people pe INNER JOIN participations pa ON pa.type = ''InterestedParty'' AND pa.event_id=<%=event_id%> AND pe.entity_id = pa.primary_entity_id', 
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Rickettsial', 'disease', 'sql lookup', 
    'SELECT '
    '  CASE '
    '    WHEN d.disease_name ILIKE ''%flea-borne%'' THEN ''FBT'' '
    '    WHEN d.disease_name ILIKE ''%spotted%'' THEN ''SFGR'' '
    '    WHEN d.disease_name=''Rickettsia, unspecified'' THEN ''RU'' '
    '    WHEN d.disease_name=''Ehrlichiosis/Anaplasmosis, undetermined'' THEN ''EA'' '
    '    ELSE ''O'' '
    '  END '
    '  FROM diseases d '
    '  INNER JOIN events e ON e.id=<%=event_id%> '
    '  INNER JOIN disease_events de ON de.event_id=e.id AND de.disease_id=d.id ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Rickettsial', 'disease_other', 'sql lookup', 
    'SELECT '
    '  CASE '
    '    WHEN d.disease_name ILIKE ''%flea-borne%'' OR d.disease_name ILIKE ''%spotted%'' OR '
    '      d.disease_name=''Rickettsia, unspecified'' OR '
    '      d.disease_name=''Ehrlichiosis/Anaplasmosis, undetermined'' THEN NULL '
    '    ELSE d.disease_name '
    '  END '
    '  FROM diseases d '
    '  INNER JOIN events e ON e.id=<%=event_id%> '
    '  INNER JOIN disease_events de ON de.event_id=e.id AND de.disease_id=d.id ',
  NULL, NULL, NULL, NULL);
--patient information
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'p_l_name', 'core', 'interested_party|person_entity|person|last_name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'p_f_name', 'core', 'interested_party|person_entity|person|first_name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', NULL, 'core', 'interested_party|person_entity|person|first_name', NULL, ' ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'p_name', 'core', 'interested_party|person_entity|person|last_name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'p_dob', 'core', 'interested_party|person_entity|person|birth_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Rickettsial', 'p_age', 'sql lookup',
    'SELECT CASE '
    '    WHEN pe.birth_date IS NOT NULL THEN date_part(''year'', age(pe.birth_date)) '
    '    WHEN pe.approximate_age_no_birthday IS NOT NULL AND pe.age_type_id=173 THEN pe.approximate_age_no_birthday '
    '    ELSE NULL '
    '  END '
    '  FROM participations p '
    '    INNER JOIN people pe ON pe.entity_id = p.primary_entity_id '
    '  WHERE p.type=''InterestedParty'' AND p.event_id=<%=event_id%> ',
  NULL, NULL, NULL, NULL);

--gender
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'p_gender', 'core', 'interested_party|person_entity|person|birth_gender_id', 'gender', NULL, NULL, NULL);
  
--address
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', NULL, 'core', 'address|street_number', NULL, ' ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', NULL, 'core', 'address|street_name', NULL, ' #', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'p_addr', 'core', 'address|unit_number', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', NULL,'core', 'address|city', NULL, ' ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', NULL,'core', 'address|state_id', 'state', ' ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'p_city_state_zip', 'core', 'address|postal_code', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'p_county', 'sql lookup', 
    'SELECT code_description FROM addresses a JOIN external_codes ec ON ec.id=a.county_id WHERE a.event_id=<%=event_id%>', 
  NULL, NULL, NULL, NULL);

--phone
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', NULL, 'core', 'interested_party|person_entity|telephones|area_code', NULL, '-', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', NULL, 'core', 'interested_party|person_entity|telephones|phone_number', NULL, ' x', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'p_phone', 'core', 'interested_party|person_entity|telephones|extension', NULL, NULL, NULL, NULL);
  
--race
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'race_w', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'W');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'race_b', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'B');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'race_a', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'A');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'race_h', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'H');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'race_ai_an', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'AI_AN');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'race_u', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'UNK');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'race_o', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'Other');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'race_other', 'adddemog', 'otherracespecify', NULL, NULL, NULL, NULL);
  
--ethnicity
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'ethnicity', 'core', 'interested_party|person_entity|person|ethnicity_id', 'ethnicity', NULL, NULL, NULL);

--clinical information
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'physician', 'sql lookup',
    'SELECT pe.first_name || '' '' || pe.last_name FROM people pe '
    '  INNER JOIN participations p ON p.secondary_entity_id=pe.entity_id AND p.type = ''Clinician'' AND p.event_id=<%=event_id%> '
    '  ORDER BY p.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', NULL, 'sql lookup',
    'SELECT a.street_number || '' '' || a.street_name FROM addresses a '
    '  INNER JOIN participations p ON p.secondary_entity_id=a.entity_id AND p.type = ''Clinician'' AND p.event_id=<%=event_id%> '
    '  ORDER BY p.id LIMIT 1 ',
  NULL, ' #', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'phy_addr', 'sql lookup',
    'SELECT a.unit_number FROM addresses a '
    '  INNER JOIN participations p ON p.secondary_entity_id=a.entity_id AND p.type = ''Clinician'' AND p.event_id=<%=event_id%> '
    '  ORDER BY p.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'phy_city_state_zip', 'sql lookup',
    'SELECT a.city FROM addresses a '
    '  INNER JOIN participations p ON p.secondary_entity_id=a.entity_id AND p.type = ''Clinician'' AND p.event_id=<%=event_id%> '
    '  ORDER BY p.id LIMIT 1 ',
  NULL, ' ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'phy_city_state_zip', 'sql lookup',
    'SELECT ec.the_code FROM addresses a '
    '  INNER JOIN participations p ON p.secondary_entity_id=a.entity_id AND p.type = ''Clinician'' AND p.event_id=<%=event_id%> '
    '  INNER JOIN external_codes ec ON ec.id=a.state_id AND ec.code_name = ''state'' '
    '  ORDER BY p.id LIMIT 1 ',
  NULL, ' ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'phy_city_state_zip', 'sql lookup',
    'SELECT a.postal_code FROM addresses a '
    '  INNER JOIN participations p ON p.secondary_entity_id=a.entity_id AND p.type = ''Clinician'' AND p.event_id=<%=event_id%> '
    '  ORDER BY p.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', NULL, 'sql lookup',
    'SELECT t.area_code FROM telephones t '
    '  INNER JOIN participations p ON p.secondary_entity_id=t.entity_id AND p.type = ''Clinician'' AND p.event_id=<%=event_id%> '
    '  ORDER BY p.id LIMIT 1 ',
  NULL, '-', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'phy_phone', 'sql lookup',
    'SELECT t.area_code || ''-'' || t.phone_number FROM telephones t '
    '  INNER JOIN participations p ON p.secondary_entity_id=t.entity_id AND p.type = ''Clinician'' AND p.event_id=<%=event_id%> '
    '  ORDER BY p.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);

--Hospitalization info
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'hospitalized', 'core', 'disease_event|hospitalized_id', 'yesno', NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'hospital', 'sql lookup',
    'SELECT concat(pl.name, '', '', a.street_number, '' '', a.street_name, '' '', a.city) FROM places pl '
    '  INNER JOIN participations p ON p.secondary_entity_id=pl.entity_id AND p.type=''HospitalizationFacility'' AND p.event_id=<%=event_id%> '
    '  LEFT JOIN addresses a ON a.entity_id=pl.entity_id '
    '  ORDER BY p.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'date_admit', 'core', 'hospitalization_facilities|hospitals_participation|admission_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'date_discharge', 'core', 'hospitalization_facilities|hospitals_participation|discharge_date', NULL, NULL, 'date_m_d_y', NULL);
   
--clinical information
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'date_onset', 'core', 'disease_event|disease_onset_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'chronic_ill', 'Rickettsial', 'chronicIll', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'imm_sup', 'Rickettsial', 'imm_sup', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'clincal_exp', 'Rickettsial', 'morelikely', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'likely_exp', 'Rickettsial', 'morelikelyexpl', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'died', 'core', 'disease_event|died_id', 'yesno', NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'date_death', 'core', 'interested_party|person_entity|person|date_of_death', NULL, NULL, 'date_m_d_y', NULL);

--clinical evidence  
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'fever', 'Rickettsial', 'fever', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'headache', 'Rickettsial', 'headache', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'anorexia', 'Rickettsial', 'anorexia', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'nausea', 'Rickettsial', 'nausVom', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'malaise', 'Rickettsial', 'malaise', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'myalgia', 'Rickettsial', 'myalgia', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'anemia', 'Rickettsial', 'anemia', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'leuk', 'Rickettsial', 'leukopenia', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'thromb', 'Rickettsial', 'thrombocytopenia', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'elt', 'Rickettsial', 'elevatedLFT', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'other_evidence', 'Rickettsial', 'otherevidence', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'rash', 'Rickettsial', 'rash', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'date_rash', 'Rickettsial', 'rashdate', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'rash_mac', 'Rickettsial', 'describerash', NULL, NULL, 'check_box', 'Macular');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'rash_pap', 'Rickettsial', 'describerash', NULL, NULL, 'check_box', 'Papular');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'rash_pet', 'Rickettsial', 'describerash', NULL, NULL, 'check_box', 'Petechial');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'rash_urt', 'Rickettsial', 'describerash', NULL, NULL, 'check_box', 'Urticarial');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'rash_pru', 'Rickettsial', 'describerash', NULL, NULL, 'check_box', 'Pruritic');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'rash_o', 'Rickettsial', 'describerash', NULL, NULL, 'check_box', 'Other');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'rash_other', 'Rickettsial', 'describeother', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'rash_fac', 'Rickettsial', 'rashwhere', NULL, NULL, 'check_box', 'Face');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'rash_arm', 'Rickettsial', 'rashwhere', NULL, NULL, 'check_box', 'Arms');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'rash_pal', 'Rickettsial', 'rashwhere', NULL, NULL, 'check_box', 'Palms of hands');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'rash_tru', 'Rickettsial', 'rashwhere', NULL, NULL, 'check_box', 'Trunk');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'rash_leg', 'Rickettsial', 'rashwhere', NULL, NULL, 'check_box', 'Legs');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'rash_sol', 'Rickettsial', 'rashwhere', NULL, NULL, 'check_box', 'Soles of feet');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'rash_spread_arm', 'Rickettsial', 'rashfrom', NULL, NULL, 'check_box', 'Arms/legs to trunk');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'rash_spread_tru', 'Rickettsial', 'rashfrom', NULL, NULL, 'check_box', 'Trunk to arms/legs');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lt_none', 'Rickettsial', 'complications', NULL, NULL, 'check_box', 'None');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lt_ards', 'Rickettsial', 'complications', NULL, NULL, 'check_box', 'Acute respiratory distress syndrome (ARDS)');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lt_men', 'Rickettsial', 'complications', NULL, NULL, 'check_box', 'Meningitis/encephalitis');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lt_dis', 'Rickettsial', 'complications', NULL, NULL, 'check_box', 'Disseminated intravascular coagulopathy (DIC)');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lt_ren', 'Rickettsial', 'complications', NULL, NULL, 'check_box', 'Renal failure');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lt_o', 'Rickettsial', 'complications', NULL, NULL, 'check_box', 'Other');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lt_other', 'Rickettsial', 'othercomplications', NULL, NULL, NULL, NULL);

--treatment
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'anti_treat', 'Rickettsial', 'antibiotics', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'tr_tet', 'sql lookup', 
    'SELECT ''On'' FROM treatments t '
    '  INNER JOIN participations_treatments pt ON pt.treatment_id=t.id '
    '  INNER JOIN participations p ON pt.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  WHERE t.treatment_name=''Tetracycline'' LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'tr_dox', 'sql lookup', 
    'SELECT ''On'' FROM treatments t '
    '  INNER JOIN participations_treatments pt ON pt.treatment_id=t.id '
    '  INNER JOIN participations p ON pt.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  WHERE t.treatment_name=''Doxycycline'' LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'tr_chl', 'sql lookup', 
    'SELECT ''On'' FROM treatments t '
    '  INNER JOIN participations_treatments pt ON pt.treatment_id=t.id '
    '  INNER JOIN participations p ON pt.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  WHERE t.treatment_name=''Chloramphenicol'' LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'tr_o', 'sql lookup', 
    'SELECT ''On'' FROM treatments t '
    '  INNER JOIN participations_treatments pt ON pt.treatment_id=t.id '
    '  INNER JOIN participations p ON pt.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  WHERE t.treatment_name!=''Tetracycline'' AND t.treatment_name!=''Doxycycline'' AND t.treatment_name!=''Chloramphenicol'' ORDER BY p.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'tr_other', 'sql lookup', 
    'SELECT t.treatment_name FROM treatments t '
    '  INNER JOIN participations_treatments pt ON pt.treatment_id=t.id '
    '  INNER JOIN participations p ON pt.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  WHERE t.treatment_name!=''Tetracycline'' AND t.treatment_name!=''Doxycycline'' AND t.treatment_name!=''Chloramphenicol'' ORDER BY p.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'respond_treat', 'Rickettsial', 'treatmentresponse', NULL, NULL, NULL, NULL);

--epidemiology  
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'fleas', 'Rickettsial', 'fleas', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'flea_bites', 'Rickettsial', 'fleabites', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'rodents', 'Rickettsial', 'rodents', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'wild_ani', 'Rickettsial', 'wildanimals', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'animal_kind', 'Rickettsial', 'animalkinds', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'dogs', 'Rickettsial', 'dogs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'cats', 'Rickettsial', 'cats', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'tick', 'Rickettsial', 'ticks', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'eng_tick', 'Rickettsial', 'engorgedtick', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'date_tick', 'Rickettsial', 'tickdate', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'hours_tick', 'Rickettsial', 'tickduration', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'de_tick', 'Rickettsial', 'detick', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', NULL, 'core', 'interested_party|risk_factor|occupation', NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'occupation_details', 'Rickettsial', 'occupation', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'travel', 'Rickettsial', 'travel', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'exp_outdoors', 'Rickettsial', 'outdoors', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'exp_res', 'Rickettsial', 'outdoor_exposure', NULL, NULL, 'check_box', 'Residence');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'exp_occ', 'Rickettsial', 'outdoor_exposure', NULL, NULL, 'check_box', 'Occupational Exposure');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'exp_rec', 'Rickettsial', 'outdoor_exposure', NULL, NULL, 'check_box', 'Recreational');

--laboratory findings  
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_coll_1', 'sql lookup',
    'SELECT lr.collection_date FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY lr.id ASC OFFSET 0 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_spec_1', 'sql lookup',
    'SELECT ec.code_description FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN external_codes ec on ec.id=lr.specimen_source_id '
    '  ORDER BY lr.id ASC OFFSET 0 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_test_1', 'sql lookup',
    'SELECT ctt.common_name FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN common_test_types ctt on ctt.id=lr.test_type_id '
    '  ORDER BY lr.id ASC OFFSET 0 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_cond_1', 'sql lookup',
    'SELECT o.organism_name FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN organisms o on o.id=lr.organism_id '
    '  ORDER BY lr.id ASC OFFSET 0 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_res_1', 'sql lookup',
    'SELECT ec.code_description FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN external_codes ec on ec.id=lr.test_result_id '
    '  ORDER BY lr.id ASC OFFSET 0 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_ref_1', 'sql lookup',
    'SELECT lr.reference_range FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY lr.id ASC OFFSET 0 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_coll_2', 'sql lookup',
    'SELECT lr.collection_date FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY lr.id ASC OFFSET 1 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_spec_2', 'sql lookup',
    'SELECT ec.code_description FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN external_codes ec on ec.id=lr.specimen_source_id '
    '  ORDER BY lr.id ASC OFFSET 1 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_test_2', 'sql lookup',
    'SELECT ctt.common_name FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN common_test_types ctt on ctt.id=lr.test_type_id '
    '  ORDER BY lr.id ASC OFFSET 1 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_cond_2', 'sql lookup',
    'SELECT o.organism_name FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN organisms o on o.id=lr.organism_id '
    '  ORDER BY lr.id ASC OFFSET 1 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_res_2', 'sql lookup',
    'SELECT ec.code_description FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN external_codes ec on ec.id=lr.test_result_id '
    '  ORDER BY lr.id ASC OFFSET 1 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_ref_2', 'sql lookup',
    'SELECT lr.reference_range FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY lr.id ASC OFFSET 1 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_coll_3', 'sql lookup',
    'SELECT lr.collection_date FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY lr.id ASC OFFSET 2 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_spec_3', 'sql lookup',
    'SELECT ec.code_description FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN external_codes ec on ec.id=lr.specimen_source_id '
    '  ORDER BY lr.id ASC OFFSET 2 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_test_3', 'sql lookup',
    'SELECT ctt.common_name FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN common_test_types ctt on ctt.id=lr.test_type_id '
    '  ORDER BY lr.id ASC OFFSET 2 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_cond_3', 'sql lookup',
    'SELECT o.organism_name FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN organisms o on o.id=lr.organism_id '
    '  ORDER BY lr.id ASC OFFSET 2 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_res_3', 'sql lookup',
    'SELECT ec.code_description FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN external_codes ec on ec.id=lr.test_result_id '
    '  ORDER BY lr.id ASC OFFSET 2 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_ref_3', 'sql lookup',
    'SELECT lr.reference_range FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY lr.id ASC OFFSET 2 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_coll_4', 'sql lookup',
    'SELECT lr.collection_date FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY lr.id ASC OFFSET 3 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_spec_4', 'sql lookup',
    'SELECT ec.code_description FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN external_codes ec on ec.id=lr.specimen_source_id '
    '  ORDER BY lr.id ASC OFFSET 3 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_test_4', 'sql lookup',
    'SELECT ctt.common_name FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN common_test_types ctt on ctt.id=lr.test_type_id '
    '  ORDER BY lr.id ASC OFFSET 3 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_cond_4', 'sql lookup',
    'SELECT o.organism_name FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN organisms o on o.id=lr.organism_id '
    '  ORDER BY lr.id ASC OFFSET 3 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_res_4', 'sql lookup',
    'SELECT ec.code_description FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN external_codes ec on ec.id=lr.test_result_id '
    '  ORDER BY lr.id ASC OFFSET 3 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_ref_4', 'sql lookup',
    'SELECT lr.reference_range FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY lr.id ASC OFFSET 3 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_coll_5', 'sql lookup',
    'SELECT lr.collection_date FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY lr.id ASC OFFSET 4 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_spec_5', 'sql lookup',
    'SELECT ec.code_description FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN external_codes ec on ec.id=lr.specimen_source_id '
    '  ORDER BY lr.id ASC OFFSET 4 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_test_5', 'sql lookup',
    'SELECT ctt.common_name FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN common_test_types ctt on ctt.id=lr.test_type_id '
    '  ORDER BY lr.id ASC OFFSET 4 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_cond_5', 'sql lookup',
    'SELECT o.organism_name FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN organisms o on o.id=lr.organism_id '
    '  ORDER BY lr.id ASC OFFSET 4 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_res_5', 'sql lookup',
    'SELECT ec.code_description FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN external_codes ec on ec.id=lr.test_result_id '
    '  ORDER BY lr.id ASC OFFSET 4 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_ref_5', 'sql lookup',
    'SELECT lr.reference_range FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY lr.id ASC OFFSET 4 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_coll_6', 'sql lookup',
    'SELECT lr.collection_date FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY lr.id ASC OFFSET 5 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_spec_6', 'sql lookup',
    'SELECT ec.code_description FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN external_codes ec on ec.id=lr.specimen_source_id '
    '  ORDER BY lr.id ASC OFFSET 5 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_test_6', 'sql lookup',
    'SELECT ctt.common_name FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN common_test_types ctt on ctt.id=lr.test_type_id '
    '  ORDER BY lr.id ASC OFFSET 5 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_cond_6', 'sql lookup',
    'SELECT o.organism_name FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN organisms o on o.id=lr.organism_id '
    '  ORDER BY lr.id ASC OFFSET 5 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_res_6', 'sql lookup',
    'SELECT ec.code_description FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN external_codes ec on ec.id=lr.test_result_id '
    '  ORDER BY lr.id ASC OFFSET 5 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_ref_6', 'sql lookup',
    'SELECT lr.reference_range FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY lr.id ASC OFFSET 5 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_coll_7', 'sql lookup',
    'SELECT lr.collection_date FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY lr.id ASC OFFSET 6 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_spec_7', 'sql lookup',
    'SELECT ec.code_description FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN external_codes ec on ec.id=lr.specimen_source_id '
    '  ORDER BY lr.id ASC OFFSET 6 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_test_7', 'sql lookup',
    'SELECT ctt.common_name FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN common_test_types ctt on ctt.id=lr.test_type_id '
    '  ORDER BY lr.id ASC OFFSET 6 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_cond_7', 'sql lookup',
    'SELECT o.organism_name FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN organisms o on o.id=lr.organism_id '
    '  ORDER BY lr.id ASC OFFSET 6 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_res_7', 'sql lookup',
    'SELECT ec.code_description FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN external_codes ec on ec.id=lr.test_result_id '
    '  ORDER BY lr.id ASC OFFSET 6 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_ref_7', 'sql lookup',
    'SELECT lr.reference_range FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY lr.id ASC OFFSET 6 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_coll_8', 'sql lookup',
    'SELECT lr.collection_date FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY lr.id ASC OFFSET 7 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_spec_8', 'sql lookup',
    'SELECT ec.code_description FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN external_codes ec on ec.id=lr.specimen_source_id '
    '  ORDER BY lr.id ASC OFFSET 7 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_test_8', 'sql lookup',
    'SELECT ctt.common_name FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN common_test_types ctt on ctt.id=lr.test_type_id '
    '  ORDER BY lr.id ASC OFFSET 7 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_cond_8', 'sql lookup',
    'SELECT o.organism_name FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN organisms o on o.id=lr.organism_id '
    '  ORDER BY lr.id ASC OFFSET 7 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_res_8', 'sql lookup',
    'SELECT ec.code_description FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN external_codes ec on ec.id=lr.test_result_id '
    '  ORDER BY lr.id ASC OFFSET 7 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_ref_8', 'sql lookup',
    'SELECT lr.reference_range FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY lr.id ASC OFFSET 7 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_coll_9', 'sql lookup',
    'SELECT lr.collection_date FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY lr.id ASC OFFSET 8 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_spec_9', 'sql lookup',
    'SELECT ec.code_description FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN external_codes ec on ec.id=lr.specimen_source_id '
    '  ORDER BY lr.id ASC OFFSET 8 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_test_9', 'sql lookup',
    'SELECT ctt.common_name FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN common_test_types ctt on ctt.id=lr.test_type_id '
    '  ORDER BY lr.id ASC OFFSET 8 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_cond_9', 'sql lookup',
    'SELECT o.organism_name FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN organisms o on o.id=lr.organism_id '
    '  ORDER BY lr.id ASC OFFSET 8 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_res_9', 'sql lookup',
    'SELECT ec.code_description FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN external_codes ec on ec.id=lr.test_result_id '
    '  ORDER BY lr.id ASC OFFSET 8 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_ref_9', 'sql lookup',
    'SELECT lr.reference_range FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY lr.id ASC OFFSET 8 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_coll_10', 'sql lookup',
    'SELECT lr.collection_date FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY lr.id ASC OFFSET 9 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_spec_10', 'sql lookup',
    'SELECT ec.code_description FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN external_codes ec on ec.id=lr.specimen_source_id '
    '  ORDER BY lr.id ASC OFFSET 9 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_test_10', 'sql lookup',
    'SELECT ctt.common_name FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN common_test_types ctt on ctt.id=lr.test_type_id '
    '  ORDER BY lr.id ASC OFFSET 9 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_cond_10', 'sql lookup',
    'SELECT o.organism_name FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN organisms o on o.id=lr.organism_id '
    '  ORDER BY lr.id ASC OFFSET 9 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_res_10', 'sql lookup',
    'SELECT ec.code_description FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN external_codes ec on ec.id=lr.test_result_id '
    '  ORDER BY lr.id ASC OFFSET 9 LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'lab_ref_10', 'sql lookup',
    'SELECT lr.reference_range FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY lr.id ASC OFFSET 9 LIMIT 1 ',
  NULL, NULL, NULL, NULL);

--travel
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', NULL, 'repeatingtravel', 'arrivaldate', NULL, '-', 'date_m_d_y,0', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'trav_date_1', 'repeatingtravel', 'departuredate', NULL, NULL, 'date_m_d_y,0', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', NULL, 'repeatingtravel', 'arrivaldate', NULL, '-', 'date_m_d_y,1', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'trav_date_2', 'repeatingtravel', 'departuredate', NULL, NULL, 'date_m_d_y,1', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', NULL, 'repeatingtravel', 'arrivaldate', NULL, '-', 'date_m_d_y,2', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'trav_date_3', 'repeatingtravel', 'departuredate', NULL, NULL, 'date_m_d_y,2', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', NULL, 'repeatingtravel', 'arrivaldate', NULL, '-', 'date_m_d_y,3', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'trav_date_4', 'repeatingtravel', 'departuredate', NULL, NULL, 'date_m_d_y,3', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', NULL, 'repeatingtravel', 'arrivaldate', NULL, '-', 'date_m_d_y,4', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'trav_date_5', 'repeatingtravel', 'departuredate', NULL, NULL, 'date_m_d_y,4', NULL);

--investigating agency
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'date_rept_ph', 'core', 'first_reported_PH_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'date_init', 'core', 'investigation_started_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'date_completed', 'core', 'investigation_completed_LHD_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'reporting_agency', 'core', 'reporting_agency|place_entity|place|name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'investigator', 'core', 'investigator|best_name', NULL, NULL, NULL, NULL);

-- comments AND notes
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'comments', 'sql lookup', 
    'SELECT to_char(pe.encounter_date, ''YYYY-MM-DD''), pe.description FROM participations_encounters pe '
      'INNER JOIN events e ON e.type=''EncounterEvent'' AND e.parent_id=<%=event_id%> AND pe.id = e.participations_encounter_id '
      'ORDER BY pe.encounter_date',
    NULL, '\n', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Rickettsial', 'comments', 'sql lookup', 
    'SELECT to_char(created_at, ''YYYY-MM-DD''), note FROM notes WHERE note_type=''clinical'' AND struckthrough != TRUE AND event_id=<%=event_id%> ORDER BY id',
    NULL, NULL, NULL, NULL);
