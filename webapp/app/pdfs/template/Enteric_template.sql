--If you are replacing all the mappings for this disease, you must first delete the existing mapping.
--This is commented out so as not be run inadvertently
--clear existing
--DELETE FROM generate_pdf_mappings WHERE template_pdf_name='Enteric';

INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'disease_name', 'sql lookup', 
    'SELECT d.disease_name FROM diseases d '
    '  INNER JOIN events e ON e.id=<%=event_id%> '
    '  INNER JOIN disease_events de ON de.disease_id = d.id AND de.event_id=e.id ', 
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'date_case_report', 'core', 'first_reported_PH_date', NULL, NULL, 'replace', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'date_reported_dshs', 'core', 'review_completed_by_state_date', NULL, NULL, 'date_now', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'record_number', 'sql lookup', 
    'SELECT record_number from events where id=<%=event_id%> ',
  NULL, NULL, NULL, NULL);

--investigation summary 
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'investigator', 'core', 'investigator|best_name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'date_init', 'core', 'investigation_started_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'date_completed', 'core', 'investigation_completed_LHD_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'date_assigned', 'core', 'investigation_started_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Enteric', 'tri_pid', 'sql lookup', 
    'SELECT pe.id FROM people pe INNER JOIN participations pa ON pa.type = ''InterestedParty'' AND pa.event_id=<%=event_id%> AND pe.entity_id = pa.primary_entity_id', 
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Enteric', 'nbs_pid', 'sql lookup', 
    'SELECT pe.nbs_id FROM people pe INNER JOIN participations pa ON pa.type = ''InterestedParty'' AND pa.event_id=<%=event_id%> AND pe.entity_id = pa.primary_entity_id', 
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Enteric', 'nbs_eid', 'sql lookup', 'SELECT nbs_id FROM events WHERE id=<%=event_id%>', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'outbreak_name', 'core', 'outbreak_name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'fcs', 'core', 'lhd_case_status_id', NULL, NULL, NULL, NULL);

--Report Source
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'reporting_agency', 'core', 'reporting_agency|place_entity|place|name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', NULL, 'core', 'reporting_agency|place_entity|telephones|area_code', NULL, '-', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'ra_phone', 'core', 'reporting_agency|place_entity|telephones|phone_number', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'reported_by', 'sql lookup', 
    'SELECT pe.first_name || '' '' || pe.last_name FROM people pe '
      'INNER JOIN participations p ON p.type = ''Reporter'' AND pe.entity_id=p.secondary_entity_id AND p.event_id=<%=event_id%>  '
      'ORDER BY p.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'hcp_name', 'sql lookup', 
    'SELECT pe.first_name || '' '' || pe.last_name FROM events e '
      'INNER JOIN participations p ON p.event_id = e.id AND p.type = ''Clinician'' '
      'INNER JOIN people pe ON pe.entity_id = p.secondary_entity_id '
      'WHERE e.id = <%=event_id%> '
      'ORDER BY p.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'hcp_phone', 'sql lookup', 
    'SELECT t.area_code || ''-''  || t.phone_number FROM events e '
      'INNER JOIN participations p ON p.event_id = e.id AND p.type = ''Clinician'' '
      'INNER JOIN telephones t on t.entity_id = p.secondary_entity_id '
      'WHERE e.id = <%=event_id%> '
      'ORDER BY p.id LIMIT 1 ',
    NULL, NULL, NULL, NULL);
  
--patient information
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'p_l_name', 'core','interested_party|person_entity|person|last_name', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric','p_f_name', 'core','interested_party|person_entity|person|first_name', NULL, NULL, NULL, NULL);
--age/gender
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'p_dob', 'core', 'interested_party|person_entity|person|birth_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value)
  VALUES ('Enteric', 'p_age', 'sql lookup',
    'SELECT CASE '
    '    WHEN pe.birth_date IS NOT NULL THEN date_part(''year'', age(pe.birth_date)) '
    '    WHEN pe.approximate_age_no_birthday IS NOT NULL AND pe.age_type_id=173 THEN pe.approximate_age_no_birthday '
    '    ELSE NULL '
    '  END '
    '  FROM participations p '
    '    INNER JOIN people pe ON pe.entity_id = p.primary_entity_id '
    '  WHERE p.type=''InterestedParty'' AND p.event_id=<%=event_id%> ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'p_gender', 'core', 'interested_party|person_entity|person|birth_gender_id', 'gender', NULL, NULL, NULL);
--address
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', NULL, 'core','address|street_number', NULL, ' ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', NULL, 'core','address|street_name', NULL, ' #', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'p_addr', 'core','address|unit_number', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'p_city','core', 'address|city', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', NULL,'core', 'address|state_id', 'state', ' ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'p_state_zip','core', 'address|postal_code', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric','p_county', 'sql lookup', 'select code_description from addresses t0 join external_codes t1 on t1.id=t0.county_id where t0.event_id=<%=event_id%>', 
  NULL, NULL, NULL, NULL);
--phone number
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', NULL, 'core','interested_party|person_entity|telephones|area_code', NULL, '-', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', NULL, 'core','interested_party|person_entity|telephones|phone_number', NULL, ' x', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'p_phone', 'core','interested_party|person_entity|telephones|extension', NULL, NULL, NULL, NULL);
-- parent/guardian
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'p_parent_guardian', 'core','parent_guardian', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', NULL, 'core', 'interested_party|risk_factor|occupation', NULL, ' / ', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'pg_occ_empl', 'sql lookup', 
    'SELECT pl.name FROM codes c '
    '  INNER JOIN events e on e.parent_id=<%=event_id%> AND e.type=''PlaceEvent'' '
    '  INNER JOIN participations p ON p.event_id=e.id and p.type=''InterestedPlace'' '
    '  INNER JOIN places pl ON pl.entity_id=p.primary_entity_id '
    '  INNER JOIN places_types pt ON pt.place_id=pl.id '
    '  WHERE c.id=pt.type_id AND c.code_description=''Employer'' ORDER BY p.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);
--race
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'race_w', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'W');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'race_b', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'B');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'race_a', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'A');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'race_h', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'H');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'race_ai_an', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'AI_AN');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'race_u', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'UNK');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'race_o', 'sql lookup',
    'SELECT ec.the_code FROM participations p '
      'INNER JOIN people_races pr ON pr.entity_id = p.primary_entity_id '
      'INNER JOIN external_codes ec ON ec.id=pr.race_id '
      'WHERE p.type= ''InterestedParty'' AND p.event_id=<%=event_id%> ', 
  NULL, NULL, 'check_box', 'Other');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'race_other', 'adddemog', 'otherracespecify', NULL, NULL, NULL, NULL);
  
--ethnicity
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'ethnicity', 'core', 'interested_party|person_entity|person|ethnicity_id', 'ethnicity', NULL, NULL, NULL);
  
--school / daycare information
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'attend_s', 'sql lookup', 
     'SELECT ''On'' FROM answers a '
      'INNER JOIN events pe ON pe.parent_id=<%=event_id%> AND pe.type = ''PlaceEvent'' '
      'INNER JOIN participations p ON pe.id = p.event_id AND p.type = ''InterestedPlace'' '
      'INNER JOIN places pl ON p.primary_entity_id = pl.entity_id '
      'INNER JOIN participations_places pp ON pp.id = pe.participations_place_id '
      'INNER JOIN places_types pt ON pl.id = pt.place_id '
      'INNER JOIN codes c ON c.code_name=''placetype'' AND c.code_description=''School'' AND c.id = pt.type_id '
      'INNER JOIN form_references fr ON fr.event_id=pe.id AND a.event_id=fr.event_id '
      'INNER JOIN forms f ON f.short_name=''daycare'' AND f.id=fr.form_id '
      'INNER JOIN form_elements fe ON fe.form_id=f.id '
      'INNER JOIN questions q ON q.short_name=''schoolcheck'' AND q.form_element_id=fe.id AND q.id=a.question_id AND a.text_answer = ''Yes'' '
      'ORDER BY pe.id LIMIT 1',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'p_school', 'sql lookup', 
    'SELECT pl.name FROM places pl '
      'INNER JOIN events pe ON pe.parent_id=<%=event_id%> AND pe.type = ''PlaceEvent'' '
      'INNER JOIN participations p ON pe.id = p.event_id AND p.type = ''InterestedPlace'' AND p.primary_entity_id = pl.entity_id '
      'INNER JOIN participations_places pp ON pp.id = pe.participations_place_id '
      'INNER JOIN places_types pt ON pl.id = pt.place_id '
      'INNER JOIN codes c ON c.code_name=''placetype'' AND c.code_description=''School'' AND c.id = pt.type_id '
      'ORDER BY pe.id LIMIT 1',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'p_school_contact', 'sql lookup', 
    'SELECT a.text_answer FROM answers a '
      'INNER JOIN events pe ON pe.parent_id=<%=event_id%> AND pe.type = ''PlaceEvent''  '
      'INNER JOIN participations p ON pe.id = p.event_id AND p.type = ''InterestedPlace''   '
      'INNER JOIN places pl ON p.primary_entity_id = pl.entity_id '
      'INNER JOIN participations_places pp ON pp.id = pe.participations_place_id  '
      'INNER JOIN places_types pt ON pl.id = pt.place_id  '
      'INNER JOIN codes c ON c.code_name=''placetype'' AND c.code_description=''School'' AND c.id = pt.type_id  '
      'INNER JOIN form_references fr ON fr.event_id=pe.id AND a.event_id=fr.event_id  '
      'INNER JOIN forms f ON f.short_name=''daycare'' AND f.id=fr.form_id  '
      'INNER JOIN form_elements fe ON fe.form_id=f.id  '
      'INNER JOIN questions q ON q.short_name=''schoolcontact'' AND q.form_element_id=fe.id AND q.id=a.question_id  '
      'ORDER BY pe.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'p_school_addr', 'sql lookup', 
    'SELECT ad.street_number || '' '' || ad.street_name || '' '' || ad.city || '' '' || ec.the_code || '' '' || ad.postal_code  FROM places pl '
      'INNER JOIN events pe ON pe.parent_id=<%=event_id%> AND pe.type = ''PlaceEvent'' '
      'INNER JOIN participations p ON pe.id = p.event_id AND p.type = ''InterestedPlace'' AND p.primary_entity_id = pl.entity_id '
      'INNER JOIN participations_places pp ON pp.id = pe.participations_place_id '
      'INNER JOIN places_types pt ON pl.id = pt.place_id '
      'INNER JOIN codes c ON c.code_name=''placetype'' AND c.code_description=''School'' AND c.id = pt.type_id '
      'LEFT JOIN addresses ad ON ad.entity_id = p.primary_entity_id '
      'LEFT JOIN external_codes ec ON ec.id = ad.state_id '
      'ORDER BY pe.id LIMIT 1',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'p_school_phone', 'sql lookup', 
     'SELECT t.area_code || '' '' || t.phone_number FROM places pl '
      'INNER JOIN events pe ON pe.parent_id=<%=event_id%> AND pe.type = ''PlaceEvent'' '
      'INNER JOIN participations p ON pe.id = p.event_id AND p.type = ''InterestedPlace'' AND p.primary_entity_id = pl.entity_id '
      'INNER JOIN participations_places pp ON pp.id = pe.participations_place_id '
      'INNER JOIN places_types pt ON pl.id = pt.place_id '
      'INNER JOIN codes c ON c.code_name=''placetype'' AND c.code_description=''School'' AND c.id = pt.type_id '
      'LEFT JOIN addresses ad ON ad.entity_id = p.primary_entity_id '
      'LEFT JOIN telephones t ON t.entity_id = p.primary_entity_id '
      'ORDER BY pe.id LIMIT 1',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'attend_d', 'sql lookup', 
     'SELECT ''On'' FROM answers a '
      'INNER JOIN events pe ON pe.parent_id=<%=event_id%> AND pe.type = ''PlaceEvent'' '
      'INNER JOIN participations p ON pe.id = p.event_id AND p.type = ''InterestedPlace'' '
      'INNER JOIN places pl ON p.primary_entity_id = pl.entity_id '
      'INNER JOIN participations_places pp ON pp.id = pe.participations_place_id '
      'INNER JOIN places_types pt ON pl.id = pt.place_id '
      'INNER JOIN codes c ON c.code_name=''placetype'' AND c.code_description=''Daycare'' AND c.id = pt.type_id '
      'INNER JOIN form_references fr ON fr.event_id=pe.id AND a.event_id=fr.event_id '
      'INNER JOIN forms f ON f.short_name=''daycare'' AND f.id=fr.form_id '
      'INNER JOIN form_elements fe ON fe.form_id=f.id '
      'INNER JOIN questions q ON q.short_name=''attenddaycare'' AND q.form_element_id=fe.id AND q.id=a.question_id AND a.text_answer = ''Yes'' '
      'ORDER BY pe.id LIMIT 1',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'p_daycare', 'sql lookup', 
    'SELECT pl.name FROM places pl '
      'INNER JOIN events pe ON pe.parent_id=<%=event_id%> AND pe.type = ''PlaceEvent'' '
      'INNER JOIN participations p ON pe.id = p.event_id AND p.type = ''InterestedPlace'' AND p.primary_entity_id = pl.entity_id '
      'INNER JOIN participations_places pp ON pp.id = pe.participations_place_id '
      'INNER JOIN places_types pt ON pl.id = pt.place_id '
      'INNER JOIN codes c ON c.code_name=''placetype'' AND c.code_description=''Daycare'' AND c.id = pt.type_id '
      'ORDER BY pe.id LIMIT 1',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'p_daycare_director', 'sql lookup', 
     'SELECT a.text_answer FROM answers a '
      'INNER JOIN events pe ON pe.parent_id=<%=event_id%> AND pe.type = ''PlaceEvent'' '
      'INNER JOIN participations p ON pe.id = p.event_id AND p.type = ''InterestedPlace'' '
      'INNER JOIN places pl ON p.primary_entity_id = pl.entity_id '
      'INNER JOIN participations_places pp ON pp.id = pe.participations_place_id '
      'INNER JOIN places_types pt ON pl.id = pt.place_id '
      'INNER JOIN codes c ON c.code_name=''placetype'' AND c.code_description=''Daycare'' AND c.id = pt.type_id '
      'INNER JOIN form_references fr ON fr.event_id=pe.id AND a.event_id=fr.event_id '
      'INNER JOIN forms f ON f.short_name=''daycare'' AND f.id=fr.form_id '
      'INNER JOIN form_elements fe ON fe.form_id=f.id '
      'INNER JOIN questions q ON q.short_name=''daycaredirector'' AND q.form_element_id=fe.id AND q.id=a.question_id '
      'ORDER BY pe.id LIMIT 1',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'p_daycare_addr', 'sql lookup', 
    'SELECT ad.street_number || '' '' || ad.street_name || '' '' || ad.city || '' '' || ec.the_code || '' '' || ad.postal_code  FROM places pl '
      'INNER JOIN events pe ON pe.parent_id=<%=event_id%> AND pe.type = ''PlaceEvent'' '
      'INNER JOIN participations p ON pe.id = p.event_id AND p.type = ''InterestedPlace'' AND p.primary_entity_id = pl.entity_id '
      'INNER JOIN participations_places pp ON pp.id = pe.participations_place_id '
      'INNER JOIN places_types pt ON pl.id = pt.place_id '
      'INNER JOIN codes c ON c.code_name=''placetype'' AND c.code_description=''Daycare'' AND c.id = pt.type_id '
      'LEFT JOIN addresses ad ON ad.entity_id = p.primary_entity_id '
      'LEFT JOIN external_codes ec ON ec.id = ad.state_id '
      'ORDER BY pe.id LIMIT 1',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'p_daycare_phone', 'sql lookup', 
     'SELECT t.area_code || '' '' || t.phone_number FROM places pl '
      'INNER JOIN events pe ON pe.parent_id=<%=event_id%> AND pe.type = ''PlaceEvent'' '
      'INNER JOIN participations p ON pe.id = p.event_id AND p.type = ''InterestedPlace'' AND p.primary_entity_id = pl.entity_id '
      'INNER JOIN participations_places pp ON pp.id = pe.participations_place_id '
      'INNER JOIN places_types pt ON pl.id = pt.place_id '
      'INNER JOIN codes c ON c.code_name=''placetype'' AND c.code_description=''Daycare'' AND c.id = pt.type_id '
      'LEFT JOIN addresses ad ON ad.entity_id = p.primary_entity_id '
      'LEFT JOIN telephones t ON t.entity_id = p.primary_entity_id '
      'ORDER BY pe.id LIMIT 1',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'aware_diag', 'sql lookup', 
     'SELECT a.text_answer FROM answers a '
      'INNER JOIN events pe ON pe.parent_id=<%=event_id%> AND pe.type = ''PlaceEvent'' '
      'INNER JOIN form_references fr ON fr.event_id=pe.id AND a.event_id=fr.event_id '
      'INNER JOIN forms f ON f.short_name=''daycare'' AND f.id=fr.form_id '
      'INNER JOIN form_elements fe ON fe.form_id=f.id '
      'INNER JOIN questions q ON q.short_name=''daycareaware'' AND q.form_element_id=fe.id AND q.id=a.question_id '
        'AND a.text_answer != '''' AND a.text_answer IS NOT NULL '
      'ORDER BY pe.id LIMIT 1',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'letter_prev', 'sql lookup', 
     'SELECT a.text_answer FROM answers a '
      'INNER JOIN events pe ON pe.parent_id=<%=event_id%> AND pe.type = ''PlaceEvent'' '
      'INNER JOIN form_references fr ON fr.event_id=pe.id AND a.event_id=fr.event_id '
      'INNER JOIN forms f ON f.short_name=''daycare'' AND f.id=fr.form_id '
      'INNER JOIN form_elements fe ON fe.form_id=f.id '
      'INNER JOIN questions q ON q.short_name=''letterhome'' AND q.form_element_id=fe.id AND q.id=a.question_id '
        'AND a.text_answer != '''' AND a.text_answer IS NOT NULL '
      'ORDER BY pe.id LIMIT 1',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'written_note', 'sql lookup', 
     'SELECT a.text_answer FROM answers a '
      'INNER JOIN events pe ON pe.parent_id=<%=event_id%> AND pe.type = ''PlaceEvent'' '
      'INNER JOIN form_references fr ON fr.event_id=pe.id AND a.event_id=fr.event_id '
      'INNER JOIN forms f ON f.short_name=''daycare'' AND f.id=fr.form_id '
      'INNER JOIN form_elements fe ON fe.form_id=f.id '
      'INNER JOIN questions q ON q.short_name=''writtennotification'' AND q.form_element_id=fe.id AND q.id=a.question_id '
        'AND a.text_answer != '''' AND a.text_answer IS NOT NULL '
      'ORDER BY pe.id LIMIT 1',
  NULL, NULL, NULL, NULL);

--clinical information
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'date_onset', 'core', 'disease_event|disease_onset_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'date_diag', 'core', 'disease_event|date_diagnosed', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'date_illness_end', 'additional_clinical', 'Illness_end', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'diarrhea', 'Ent_Sym', 'diarrhea', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'num_stool_24', 'Ent_Sym', 'max_num_stool', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'bl_dia', 'Ent_Sym', 'blood_diarrhea', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'abd_cramp', 'Ent_Sym', 'cramp', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'nausea', 'Ent_Sym', 'nausea', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'vomit', 'Ent_Sym', 'vomit', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'fever', 'Ent_Sym', 'fever', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'high_temp', 'Ent_Sym', 'Highest_temp', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'oth_sym', 'Ent_Sym', 'othersympt', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'other_symptoms', 'Ent_Sym', 'describeother', NULL, NULL, NULL, NULL);
  
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'underlying_conditions', 'Ent_Sym', 'underlying_conditions', NULL, NULL, NULL, NULL);
--treatments
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'anti_med', 'sql lookup', 
    'SELECT ''Yes'' FROM treatments t '
    '  INNER JOIN participations_treatments pt ON pt.treatment_id=t.id '
    '  INNER JOIN participations p ON pt.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'anti_med_list', 'sql lookup', 
    'SELECT t.treatment_name FROM treatments t '
    '  INNER JOIN participations_treatments pt ON pt.treatment_id=t.id '
    '  INNER JOIN participations p ON pt.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  ORDER BY p.id',
  NULL, NULL, NULL, NULL);
--Hospitalization info
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'hospitalized', 'core', 'disease_event|hospitalized_id', 'yesno', NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'hospital', 'sql lookup',
    'SELECT concat(pl.name, '', '', a.street_number, '' '', a.street_name, '' '', a.city) FROM places pl '
    '  INNER JOIN participations p ON p.secondary_entity_id=pl.entity_id AND p.type=''HospitalizationFacility'' AND p.event_id=<%=event_id%> '
    '  LEFT JOIN addresses a ON a.entity_id=pl.entity_id '
    '  ORDER BY p.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'date_admit', 'core', 'hospitalization_facilities|hospitals_participation|admission_date', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'date_discharge', 'core', 'hospitalization_facilities|hospitals_participation|discharge_date', NULL, NULL, 'date_m_d_y', NULL);
--died
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'died', 'Death_details', 'Died_of_this_illness', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'date_death', 'core', 'interested_party|person_entity|person|date_of_death', NULL, NULL, 'date_m_d_y', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'autopsy', 'Death_details' ,'Autopsy_performed', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'place_death', 'Death_details' ,'Place_of_Death', NULL, NULL, NULL, NULL);

--laboratory
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'lab_coll', 'sql lookup',
    'SELECT lr.collection_date FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  WHERE lr.collection_date IS NOT NULL ORDER BY lr.collection_date ASC LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'lab_date', 'sql lookup',
    'SELECT lr.lab_test_date FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  WHERE lr.collection_date IS NOT NULL ORDER BY lr.collection_date ASC LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'spec_src', 'sql lookup',
    'SELECT '
    '  CASE WHEN lr.specimen_source_id IN (123,124) THEN ''B'' '
    '    WHEN lr.specimen_source_id=137    THEN ''S'' '
    '    WHEN lr.specimen_source_id in (141,142)   THEN ''U'' '
    '    ELSE '''' '
    '  END '
    '  FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  WHERE lr.specimen_source_id IS NOT NULL ORDER BY lr.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'lab_species', 'sql lookup',
    'SELECT o.organism_name FROM lab_results lr ' 
    '  INNER JOIN participations p ON p.type=''Lab'' AND lr.participation_id=p.id AND p.event_id=<%=event_id%> '
    '  LEFT JOIN organisms o on o.id=lr.organism_id '
    '  WHERE lr.organism_id IS NOT NULL ORDER BY lr.id LIMIT 1 ',
  NULL, NULL, NULL, NULL);
--infection timeline
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'onset_minus_4weeks', 'sql lookup',
    'SELECT '
    '  CASE WHEN de.id=2 THEN to_char(e.event_onset_date-28, ''YYYY-MM-DD'') '
    '    ELSE '''' '
    '  END '
    ' FROM disease_events de '
    ' INNER JOIN events e ON de.event_id=<%=event_id%> AND e.id=de.event_id ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'onset_minus_12days', 'sql lookup',
    'SELECT '
    '  CASE WHEN de.id=2 THEN '''' '
    '    ELSE to_char(e.event_onset_date-12, ''YYYY-MM-DD'') '
    '  END '
    ' FROM disease_events de '
    ' INNER JOIN events e ON de.event_id=<%=event_id%> AND e.id=de.event_id ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'date_onset_ame', 'sql lookup',
    'SELECT '
    '  CASE WHEN de.id=2 THEN to_char(e.event_onset_date, ''YYYY-MM-DD'') '
    '    ELSE '''' '
    '  END '
    ' FROM disease_events de '
    ' INNER JOIN events e ON de.event_id=<%=event_id%> AND e.id=de.event_id ',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'date_onset_oth', 'sql lookup',
    'SELECT '
    '  CASE WHEN de.id=2 THEN '''' '
    '    ELSE to_char(e.event_onset_date, ''YYYY-MM-DD'') '
    '  END '
    ' FROM disease_events de '
    ' INNER JOIN events e ON de.event_id=<%=event_id%> AND e.id=de.event_id ',
  NULL, NULL, NULL, NULL);
--exposure
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', NULL, 'repeatingtravel', 'destination', NULL, ':', 'filler,0', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', NULL, 'repeatingtravel', 'arrivaldate', NULL, '-', 'date_m_d_y,0', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', NULL, 'repeatingtravel', 'departuredate', NULL, ' | ', 'date_m_d_y,0', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', NULL, 'repeatingtravel', 'destination', NULL, ':', 'filler,1', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', NULL, 'repeatingtravel', 'arrivaldate', NULL, '-', 'date_m_d_y,1', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', NULL, 'repeatingtravel', 'departuredate', NULL, ' | ', 'date_m_d_y,1', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', NULL, 'repeatingtravel', 'destination', NULL, ':', 'filler,2', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', NULL, 'repeatingtravel', 'arrivaldate', NULL, '-', 'date_m_d_y,2', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'dest_dates', 'repeatingtravel', 'departuredate', NULL, NULL, 'date_m_d_y,2', NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'how_travel', 'repeatingtravel', 'travel_mode', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'hh_contacts', 'sql lookup', 
    ' SELECT ''Yes'' FROM events e '
    ' INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
    ' INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
    ' WHERE  e.parent_id=<%=event_id%> LIMIT 1',
  NULL, NULL, NULL, NULL); 
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'num_contacts', 'sql lookup', 
    ' SELECT Count(*) FROM events e '
    ' INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
    ' INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
    ' WHERE  e.parent_id=<%=event_id%> ',
  NULL, NULL, NULL, NULL); 
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'num_ill', 'sql lookup', 
    ' SELECT Count(*) FROM events e '
    ' INNER JOIN participations_contacts pc ON pc.id=e.participations_contact_id '
    ' INNER JOIN external_codes ec ON ec.code_description=''Household'' AND ec.id=pc.contact_type_id '
    '   AND e.type != ''ContactEvent'' '
    ' WHERE  e.parent_id=<%=event_id%> ',
  NULL, NULL, NULL, NULL); 
--infant case
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'formula_details', 'enteric_events_v2', 'formulabrand', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'formula_purchased', 'enteric_events_v2', 'formula_purchased', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'formula_water', 'enteric_events_v2', 'formula_water', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'formula_bottle', 'enteric_events_v2', 'bottlebrand', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'formula_brand_type', 'enteric_events_v2', 'babyfoodbrand', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'ifp', 'enteric_events_v2', 'infantfeeding', NULL, NULL, NULL, NULL);
  
--food history
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'rfo', 'enteric_events_v2', 'raw_fv', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'types_fv', 'enteric_events_v2', 'types_fv', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'udp', 'enteric_events_v2', 'unpasteurized_dairy', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'types_udp', 'enteric_events_v2', 'types_dp', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'meats', 'enteric_events_v2', 'meats', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'types_meats', 'enteric_events_v2', 'types_meats', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'ror', 'enteric_events_v2', 'raw_eggs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'types_eggs', 'enteric_events_v2', 'types_Eggs', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'gm', 'enteric_events_v2', 'group_meal', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'gm_date', 'enteric_events_v2', 'date_gm', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'gm_loc', 'enteric_events_v2', 'location_gm', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'gm_food', 'enteric_events_v2', 'foods_gm', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'fds', 'enteric_events_v2', 'food_del', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'fds_name', 'enteric_events_v2', 'food_del_type', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'sfh_name_loc', 'enteric_events_v2', 'food_home', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'ffr', 'sql lookup', 
    'SELECT '
    '    CASE '
    '      WHEN Count(*) > 0 THEN ''Yes'' '
    '      ELSE ''No'' '
    '    END '
    'FROM answers a '
    'INNER JOIN form_references fr ON fr.event_id=<%=event_id%> '
    'INNER JOIN forms f ON f.id = fr.form_id AND f.short_name=''Restaurant_exposures'' '
    'INNER JOIN form_elements fe ON fe.form_id = f.id '
    'INNER JOIN questions q ON q.form_element_id = fe.id AND q.short_name = ''restaurantname'' '
    'AND q.id = a.question_id ', 
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'res_name_loc_1', 'sql lookup', 
    'SELECT pl.name FROM places pl '
      'INNER JOIN events pe ON pe.parent_id=<%=event_id%> AND pe.type = ''PlaceEvent'' '
      'INNER JOIN participations p ON p.type=''InterestedPlace'' AND p.event_id = pe.id '
      'INNER JOIN places_types pt ON pl.id = pt.place_id '
      'INNER JOIN codes c ON c.code_name=''placetype'' AND c.code_description=''Food Establishment'' AND c.id = pt.type_id '
      'WHERE pl.entity_id = p.primary_entity_id '
      'ORDER BY pe.id LIMIT 1 OFFSET 0', 
  NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'res_name_loc_1', 'sql lookup', 
    'SELECT ad.street_number || '' '' || ad.street_name || '' '' || ad.city || '' '' || ec.the_code || ad.postal_code FROM places pl '
      'INNER JOIN events pe ON pe.parent_id=<%=event_id%> AND pe.type = ''PlaceEvent'' '
      'INNER JOIN participations p ON p.type=''InterestedPlace'' AND p.event_id = pe.id '
      'INNER JOIN places_types pt ON pl.id = pt.place_id '
      'INNER JOIN codes c ON c.code_name=''placetype'' AND c.code_description=''Food Establishment'' AND c.id = pt.type_id '
      'LEFT JOIN addresses ad ON ad.entity_id = p.primary_entity_id '
      'LEFT JOIN external_codes ec ON ec.id = ad.state_id '
      'WHERE pl.entity_id = p.primary_entity_id '
      'ORDER BY pe.id LIMIT 1 OFFSET 0', 
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'res_date_1', 'sql lookup', 
    'SELECT pp.date_of_exposure FROM places pl '
      'INNER JOIN events pe ON pe.parent_id=<%=event_id%> AND pe.type = ''PlaceEvent'' '
      'INNER JOIN participations p ON p.type=''InterestedPlace'' AND p.event_id = pe.id '
      'INNER JOIN places_types pt ON pl.id = pt.place_id '
      'INNER JOIN codes c ON c.code_name=''placetype'' AND c.code_description=''Food Establishment'' AND c.id = pt.type_id '
      'INNER JOIN participations_places pp ON pp.id = pe.participations_place_id '
      'WHERE pl.entity_id = p.primary_entity_id '
      'ORDER BY pe.id LIMIT 1 OFFSET 0',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'res_food_1', 'sql lookup', 
     'SELECT a.text_answer FROM answers a '
      'INNER JOIN events pe ON pe.parent_id=<%=event_id%> AND pe.type = ''PlaceEvent'' '
      'INNER JOIN form_references fr ON fr.event_id=pe.id AND a.event_id=fr.event_id '
      'INNER JOIN forms f ON f.short_name=''Restaurant_exposures'' AND f.id=fr.form_id '
      'INNER JOIN form_elements fe ON fe.form_id=f.id '
      'INNER JOIN questions q ON q.short_name=''foodeaten'' AND q.form_element_id=fe.id AND q.id=a.question_id '
      'ORDER BY pe.id LIMIT 1 OFFSET 0',
  NULL, NULL, NULL, NULL);

INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'res_name_loc_2', 'sql lookup', 
    'SELECT pl.name FROM places pl '
      'INNER JOIN events pe ON pe.parent_id=<%=event_id%> AND pe.type = ''PlaceEvent'' '
      'INNER JOIN participations p ON p.type=''InterestedPlace'' AND p.event_id = pe.id '
      'INNER JOIN places_types pt ON pl.id = pt.place_id '
      'INNER JOIN codes c ON c.code_name=''placetype'' AND c.code_description=''Food Establishment'' AND c.id = pt.type_id '
      'WHERE pl.entity_id = p.primary_entity_id '
      'ORDER BY pe.id LIMIT 1 OFFSET 1', 
  NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'res_name_loc_2', 'sql lookup', 
    'SELECT ad.street_number || '' '' || ad.street_name || '' '' || ad.city || '' '' || ec.the_code || ad.postal_code FROM places pl '
      'INNER JOIN events pe ON pe.parent_id=<%=event_id%> AND pe.type = ''PlaceEvent'' '
      'INNER JOIN participations p ON p.type=''InterestedPlace'' AND p.event_id = pe.id '
      'INNER JOIN places_types pt ON pl.id = pt.place_id '
      'INNER JOIN codes c ON c.code_name=''placetype'' AND c.code_description=''Food Establishment'' AND c.id = pt.type_id '
      'LEFT JOIN addresses ad ON ad.entity_id = p.primary_entity_id '
      'LEFT JOIN external_codes ec ON ec.id = ad.state_id '
      'WHERE pl.entity_id = p.primary_entity_id '
      'ORDER BY pe.id LIMIT 1 OFFSET 1', 
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'res_date_2', 'sql lookup', 
    'SELECT pp.date_of_exposure FROM places pl '
      'INNER JOIN events pe ON pe.parent_id=<%=event_id%> AND pe.type = ''PlaceEvent'' '
      'INNER JOIN participations p ON p.type=''InterestedPlace'' AND p.event_id = pe.id '
      'INNER JOIN places_types pt ON pl.id = pt.place_id '
      'INNER JOIN codes c ON c.code_name=''placetype'' AND c.code_description=''Food Establishment'' AND c.id = pt.type_id '
      'INNER JOIN participations_places pp ON pp.id = pe.participations_place_id '
      'WHERE pl.entity_id = p.primary_entity_id '
      'ORDER BY pe.id LIMIT 1 OFFSET 1',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'res_food_2', 'sql lookup', 
     'SELECT a.text_answer FROM answers a '
      'INNER JOIN events pe ON pe.parent_id=<%=event_id%> AND pe.type = ''PlaceEvent'' '
      'INNER JOIN form_references fr ON fr.event_id=pe.id AND a.event_id=fr.event_id '
      'INNER JOIN forms f ON f.short_name=''Restaurant_exposures'' AND f.id=fr.form_id '
      'INNER JOIN form_elements fe ON fe.form_id=f.id '
      'INNER JOIN questions q ON q.short_name=''foodeaten'' AND q.form_element_id=fe.id AND q.id=a.question_id '
      'ORDER BY pe.id LIMIT 1 OFFSET 1',
  NULL, NULL, NULL, NULL);

INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'res_name_loc_3', 'sql lookup', 
    'SELECT pl.name FROM places pl '
      'INNER JOIN events pe ON pe.parent_id=<%=event_id%> AND pe.type = ''PlaceEvent'' '
      'INNER JOIN participations p ON p.type=''InterestedPlace'' AND p.event_id = pe.id '
      'INNER JOIN places_types pt ON pl.id = pt.place_id '
      'INNER JOIN codes c ON c.code_name=''placetype'' AND c.code_description=''Food Establishment'' AND c.id = pt.type_id '
      'WHERE pl.entity_id = p.primary_entity_id '
      'ORDER BY pe.id LIMIT 1 OFFSET 2', 
  NULL, '/', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'res_name_loc_3', 'sql lookup', 
    'SELECT ad.street_number || '' '' || ad.street_name || '' '' || ad.city || '' '' || ec.the_code || ad.postal_code FROM places pl '
      'INNER JOIN events pe ON pe.parent_id=<%=event_id%> AND pe.type = ''PlaceEvent'' '
      'INNER JOIN participations p ON p.type=''InterestedPlace'' AND p.event_id = pe.id '
      'INNER JOIN places_types pt ON pl.id = pt.place_id '
      'INNER JOIN codes c ON c.code_name=''placetype'' AND c.code_description=''Food Establishment'' AND c.id = pt.type_id '
      'LEFT JOIN addresses ad ON ad.entity_id = p.primary_entity_id '
      'LEFT JOIN external_codes ec ON ec.id = ad.state_id '
      'WHERE pl.entity_id = p.primary_entity_id '
      'ORDER BY pe.id LIMIT 1 OFFSET 2', 
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'res_date_3', 'sql lookup', 
    'SELECT pp.date_of_exposure FROM places pl '
      'INNER JOIN events pe ON pe.parent_id=<%=event_id%> AND pe.type = ''PlaceEvent'' '
      'INNER JOIN participations p ON p.type=''InterestedPlace'' AND p.event_id = pe.id '
      'INNER JOIN places_types pt ON pl.id = pt.place_id '
      'INNER JOIN codes c ON c.code_name=''placetype'' AND c.code_description=''Food Establishment'' AND c.id = pt.type_id '
      'INNER JOIN participations_places pp ON pp.id = pe.participations_place_id '
      'WHERE pl.entity_id = p.primary_entity_id '
      'ORDER BY pe.id LIMIT 1 OFFSET 2',
  NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'res_food_3', 'sql lookup', 
     'SELECT a.text_answer FROM answers a '
      'INNER JOIN events pe ON pe.parent_id=<%=event_id%> AND pe.type = ''PlaceEvent'' '
      'INNER JOIN form_references fr ON fr.event_id=pe.id AND a.event_id=fr.event_id '
      'INNER JOIN forms f ON f.short_name=''Restaurant_exposures'' AND f.id=fr.form_id '
      'INNER JOIN form_elements fe ON fe.form_id=f.id '
      'INNER JOIN questions q ON q.short_name=''foodeaten'' AND q.form_element_id=fe.id AND q.id=a.question_id '
      'ORDER BY pe.id LIMIT 1 OFFSET 2',
  NULL, NULL, NULL, NULL);

--water source
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'sod', 'enteric_water', 'Source_of_drinking_water', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'sod_iw', 'enteric_water', 'Drinking_water_source', NULL, NULL, 'check_box', 'Private well');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'sod_sw', 'enteric_water', 'Drinking_water_source', NULL, NULL, 'check_box', 'Shared well');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'sod_pws', 'enteric_water', 'Drinking_water_source', NULL, NULL, 'check_box', 'Public water system');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'sod_bw', 'enteric_water', 'Drinking_water_source', NULL, NULL, 'check_box', 'Bottle water');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'wat_filt', 'enteric_water', 'water_treated', NULL, NULL, 'check_box', 'Bottle water');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'duw', 'enteric_water', 'Drank_untreated_unchlorinated', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'rwe', 'enteric_water', 'Recreational_water_exposure', NULL, NULL, NULL, NULL);
--animal exposure
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'coh', 'Animals', 'farmdairy', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'wwa', 'Animals', 'animalproducts', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'types_animals', 'Animals', 'animaltype2', NULL, ',', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'types_animals', 'Animals', 'otherreptile2', NULL, ',', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'types_animals', 'Animals', 'otheramphibian2', NULL, ',', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'types_animals', 'Animals', 'othermammal2', NULL, ',', NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'types_animals', 'Animals', 'other2', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'acw', 'Animals', 'anyanimalcontact', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'contact_animal', 'Animals', 'specifyanimalloc', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'wps', 'Animals', 'animalsick', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'pro', 'Animals', 'animalrecent', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'pft_name', 'Animals', 'petfood', NULL, NULL, NULL, NULL);
--exposure location
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'wde', 'enteric_events_v2', 'where_exposure', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'tx_ooj', 'enteric_events_v2', 'county_city', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'state_not_tx', 'enteric_events_v2', 'State', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'country', 'enteric_events_v2', 'country_region', NULL, NULL, NULL, NULL);
--public health concerns
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'eaf', 'enteric_events_v2', 'Employed_as_food_worker', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'nfh', 'enteric_events_v2', 'non_occ_food_handling', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'eah', 'enteric_events_v2', 'Employed_as_health_care_worker', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'eic', 'enteric_events_v2', 'Employed_in_child_care', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'acc', 'enteric_events_v2', 'Attends_child_care_school', NULL, NULL, NULL, NULL);
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'hhm', 'enteric_events_v2', 'HH_member', NULL, NULL, NULL, NULL);
--public health actions  
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'pha_e', 'enteric_events_v2', 'Public_Health_Actions', NULL, NULL, 'check_box', 'Exclude probable and confirmed cases in sensitive occupations/situations (HCW, food, child care) until 2 negative stools');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'pha_epr', 'enteric_events_v2', 'Public_Health_Actions', NULL, NULL, 'check_box', 'Education provided on route of transmission');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'pha_fbi', 'enteric_events_v2', 'Public_Health_Actions', NULL, NULL, 'check_box', 'FBI/restaurant info sent to sanitarian');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'pha_cc', 'enteric_events_v2', 'Public_Health_Actions', NULL, NULL, 'check_box', 'Child care facility information provided to licensing as needed if facility is non-compliant');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'pha_rmd', 'enteric_events_v2', 'Public_Health_Actions', NULL, NULL, 'check_box', 'Raw milk/dairy information sent to M&D Group');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'pha_ccs', 'enteric_events_v2', 'Public_Health_Actions', NULL, NULL, 'check_box', 'Child care/school exclusion criteris provided');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'pha_cca', 'enteric_events_v2', 'Public_Health_Actions', NULL, NULL, 'check_box', 'Child care/school awareness letter provided');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'pha_lrr', 'enteric_events_v2', 'Public_Health_Actions', NULL, NULL, 'check_box', 'Licensing reporting requirement provided to child care center');
INSERT INTO generate_pdf_mappings(template_pdf_name, template_field_name, form_short_name, form_field_name, code_name, concat_string, operation, match_value) 
  VALUES ('Enteric', 'comments', 'enteric_events_v2', 'Other_public_health_actions', NULL, NULL, NULL, NULL);
  
