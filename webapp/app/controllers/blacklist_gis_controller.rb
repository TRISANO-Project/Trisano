require 'dbi'
require 'dbd/Jdbc'
require 'jdbc/postgres'

class BlacklistGisController < ApplicationController
  include ApplicationHelper


  def blacklistgis
    if params[:address_id] != '' && params[:address_id] != nil

      address_id = params[:address_id]
      config = Rails.configuration.database_configuration
      host = config[Rails.env]["host"]
      database = config[Rails.env]["database"]
      username = config[Rails.env]["username"]
      pass = config[Rails.env]["password"]
      port = config[Rails.env]["port"]
      conn = DBI.connect("DBI:Jdbc:postgresql:" + "//" + host + ":" + port.to_s +
        "/" + database, username, pass, 'driver' => 'org.postgresql.Driver')
      
      sql = "SELECT public.blacklist_address_gis(" + address_id + ")"
      begin
        conn.execute(sql)
      rescue
      end
     
    end
    redirect_to '/'
  end
end