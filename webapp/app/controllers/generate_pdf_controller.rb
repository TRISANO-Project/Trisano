require 'date'
require 'dbi'
require 'dbd/Jdbc'
require 'jdbc/postgres'
require 'pdf_forms'

class GeneratePdfController < ApplicationController
  include ApplicationHelper

  def generatepdf
    if params[:evnt_id] != '' && params[:evnt_id] != nil
      event_id = params[:evnt_id]
                  
      output_fields = {}
      out_filename = ''
      pdftk_path = '/usr/bin/pdftk'
      event = Event.find(event_id)
      template_pdf_name = params[:pdf_name]
      
      pdf_path = Rails.root.to_s + "/app/pdfs/"
      template = pdf_path + "template/" + template_pdf_name + "_template.pdf"

      mappings = GeneratePdfMapping.find(:all, 
        :conditions => ["template_pdf_name = ?", template_pdf_name], :order => "id ASC")

      stack = []
      accumulated_value = ''
      concat_string = ''
      skip_this = false
      
      mappings.each do |mapping|
        
        fetched_value = ''
        insert_value = ''
        end_of_concat = true
        prior_concat_string = concat_string
        
        if(mapping['concat_string'] != nil)
          end_of_concat = false
          concat_string = mapping['concat_string']
        end

        #handle case when mapping.operation = nil
        if(mapping.operation.blank?)
          mapping.operation = ''
        end
        
        # figure out if this is a repeating custom field, and which item row we want.
        item_index=0
        multi_select=false
        if(mapping.operation.include? ",")
          ops = mapping.operation.split(',')
          item_index=ops[1].to_i
          mapping.operation=ops[0]
          multi_select=true
        end
        
        # figure out if we are getting text answer or corresponding code from a form question
        use_code = (mapping.operation == 'answer_code')
        
        # if data from a core field
        if(mapping.form_short_name == "core")
          core_field_path = mapping['form_field_name']
          if(!core_field_path.blank?)
            elements = core_field_path.split("|")
            obj = event
            elements.each do |element|
              if(obj.blank?)
                break
              end
              obj = obj.try(element)
              if (obj != nil && obj.is_a?(Array))
                if(item_index < obj.length)
                  obj = obj[item_index]
                else
                  obj = nil
                end
              end
            end
            if(obj != nil)
              fetched_value = obj.to_s.strip()
              if(!mapping['code_name'].blank? && !fetched_value.blank?)
                external_code = ExternalCode.find(:first, :conditions => ["code_name = ? AND external_codes.id = ? ", mapping['code_name'], fetched_value])
                begin
                  fetched_value = external_code.the_code.to_s
                rescue
                end
              end
            end
          end
        # mapping from an SQL lookup
        # clinical comments are not available as core fields for some reason
        elsif(mapping.form_short_name == "sql lookup")
          sql = ERB.new(mapping.form_field_name).result(binding)
          results = Event.find_by_sql(sql) 
          #copy all the fetched data, seperate fields by spaces and rows by newlines
          results.each do |row|
            instance_variables = row.instance_variables 
            rowHash = row.instance_variable_get(instance_variables[1])
            rowHash.each_value do |value|
              fetched_value += value.to_s + " "
            end
            fetched_value += "\n"
          end
          fetched_value = fetched_value.strip()
        # mapping from a form field
        elsif(mapping.form_short_name.length > 0) # form field
          which = :last
          if(multi_select)
            which = :all
          end
          answers = fetch_answers(which, event_id, mapping['form_short_name'], mapping['form_field_name'])
          answer = answers
          if (answers.is_a?(Array))
            answer = answers[item_index]
          end
          if(!answer.blank?)
            fetched_value = (use_code ? answer['code'].strip() : answer['text_answer'].strip())
          end
        end
 
        case mapping.operation
        when "push"
          stack.push(fetched_value)
        when "age_subtract"
          text_birth_date = stack.pop()
          if(!text_birth_date.blank? && !fetched_value.blank?)
            # we expect a date formatted like 2012-02-24
            begin
              birth_date = Date.parse(text_birth_date)
              calc_date = Date.parse(fetched_value)
              insert_value = calculate_age(birth_date, calc_date).to_s
            rescue
            end
          end
        when "date_m_d_y"
          if(fetched_value.length > 9)
            insert_value = fetched_value[5..6] + '/' + fetched_value[8..9] + '/' + fetched_value[0..3]
          end
        when "YYYY"
          if(fetched_value.length > 4)
            insert_value = fetched_value[0..3]
          end
        when "MM"
          if(fetched_value.length > 7)
            insert_value = fetched_value[5..6]
          end
        when "DD"
          if(fetched_value.length > 9)
            insert_value = fetched_value[8..9]
          end
        when "check_box"
          # handle multi-checkboxes
          values=fetched_value.split("\n")
          values.each do |value|
            if(value.downcase().strip() == mapping['match_value'].downcase().strip())
              insert_value = 'On'
              break
            end
          end
        when "check_box_invert"
          values=fetched_value.split("\n")
          insert_value='On'
          values.each do |value|
            if(value.downcase().strip() == mapping['match_value'].downcase().strip())
              insert_value = ''
              break
            end
          end
        when "fill_if_yes"
          test = stack.pop()
          if(test.length > 0 && test.downcase()[0,1] == "y")
            insert_value = 'On'
          else
            skip_this = true
          end
        when "fill_if_no"
          test = stack.pop()
          if(test.length > 0 && test.downcase()[0,1] == "n")
            insert_value = 'On'
          else
            skip_this = true
          end
        when "date_now"
          insert_value = Date.today().to_s
        when "date_plus_week"
          insert_value = (Date.today()+7).to_s
        when "replace_if_dst_blank"
          if(output_fields[mapping['template_field_name']].blank?)
            insert_value = fetched_value
          else
            skip_this = true
          end
        when "replace_if_dst_blank_strip_alpha"
          if(output_fields[mapping['template_field_name']].blank?)
            insert_value = fetched_value.gsub(/[^0-9\-]/, "")
          else
            skip_this = true
          end
        when /replace_if_src_not_blank|answer_code/
          if(!fetched_value.blank?)
            insert_value = fetched_value
          else
            skip_this = true
          end
        when "record_number"
          insert_value = event.record_number.to_s
        when "age"
          if(!birth_date.blank?)
            insert_value = calculate_age(birth_date).to_s
          end
        when "strip_alpha"
          insert_value = fetched_value.gsub(/[^0-9\-]/, "")
        else # treat as replace
          insert_value = fetched_value
        end

        # concatinate the data from this mapping if the concat string is not nil
        if(!insert_value.blank?)
          accumulated_value += prior_concat_string + insert_value 
        end
        if(end_of_concat)
          # we are done, this is the last value to concatinate
          if(!skip_this)
            output_fields[mapping['template_field_name']] = accumulated_value.strip()
          end
          accumulated_value = ''
          concat_string = ''
          skip_this = false
        end
      end

      # put the hash contents into the pdf
      fdf_filename = pdf_path + 'generate_' + event_id + '.fdf'
      fdf = PdfForms::Fdf.new output_fields
      fdf.save_to fdf_filename
      pdf_filename = pdf_path + 'generate_' + event_id + '.pdf'

      if(File.exists?(pdf_filename))
        begin
          File.delete(pdf_filename)
        rescue Exception => e
          logger.info(e.backtrace.inspect)
        end
      end
        
      # run the task in background, otherwise the jruby system call hangs
      system (pdftk_path + ' ' + template + ' fill_form ' + fdf_filename + ' output ' + pdf_filename + ' dont_ask' + ' &')
      i=0
      until i > 20 do
        break if(File.exists?(pdf_filename))
        sleep(0.5)
        i+=1
      end
      sleep(0.5)
      if(File.exists?(pdf_filename))
        send_file(pdf_filename, :filename => template_pdf_name + event_id + '.pdf', :type => "application/pdf")
      end
    end
  end
  
  def fetch_answers(which, event_id, form_short_name, form_field_name)
    return Answer.find(which, :conditions => ["answers.event_id = ?", event_id], :joins=> 
      "INNER JOIN form_references fr ON fr.event_id = " + event_id + 
      " INNER JOIN forms f ON f.id = fr.form_id AND f.short_name = '" + form_short_name + "'" +
      " INNER JOIN form_elements fe ON fe.form_id = f.id " +
      " INNER JOIN questions q ON q.form_element_id = fe.id AND q.short_name = '" + form_field_name + "'" +
      " AND q.id = answers.question_id", :order => "id ASC")
  end
end
  
