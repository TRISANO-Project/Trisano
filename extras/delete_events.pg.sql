﻿Create or replace function del_events()
  returns void as
$BODY$
DECLARE
  _event_id integer;
BEGIN
  for _event_id in 
    select id 
    from events e 
    where e.workflow_state='accepted_by_lhd' 
    and id in (select p.event_id 
               from participations p
               join places pl on p.secondary_entity_id=pl.entity_id
               where p.type='Jurisdiction' and pl.name='DELETE') 
  LOOP
    DELETE FROM access_records where event_id=_event_id;
    DELETE FROM actual_delivery_facilities_participations where participation_id in 
                                                           (select id from participations 
                                                            where event_id=_event_id );
    DELETE FROM addresses where event_id=_event_id;
    DELETE FROM answers where event_id=_event_id;
    DELETE FROM attachments where event_id=_event_id;
    DELETE FROM audit_user where event_id=_event_id;
    DELETE FROM dashboard_alerts where event_id=_event_id;
    DELETE FROM disease_events where event_id=_event_id;
    DELETE FROM encounters where event_id=_event_id;
    DELETE FROM event_type_transitions where event_id=_event_id;
    DELETE FROM form_references where event_id=_event_id;
    DELETE FROM hospitals_participations where participation_id in 
                                                           (select id from participations 
                                                            where event_id=_event_id);
    DELETE FROM investigator_form_sections where event_id=_event_id;
    DELETE FROM notes where event_id=_event_id;
    DELETE FROM participations_risk_factors where participation_id in 
                                                           (select id from participations 
                                                            where event_id=_event_id);
    DELETE FROM participations_treatments where participation_id in 
                                                           (select id from participations 
                                                            where event_id=_event_id);
    DELETE FROM lab_results where participation_id in (select id from participations 
                                                            where event_id=_event_id);                                                           
    DELETE FROM participations where event_id=_event_id;
    DELETE FROM tasks where event_id=_event_id;
    DELETE FROM events where parent_id=_event_id and type not in ('AssessmentEvent','MorbidityEvent');
    UPDATE events set parent_id = NULL where parent_id=_event_id;
    DELETE FROM events where id=_event_id;
  END LOOP;
END;
$BODY$
LANGUAGE plpgsql;