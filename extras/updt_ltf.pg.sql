﻿Create or replace function updt_ltf()
  returns void as
$BODY$
DECLARE
  _event_id integer;
  _admin_id integer;
BEGIN
  select users.id into _admin_id from users where uid='trisano_admin';
  for _event_id in
  select event_id 
  from notes n0
  where n0.note like 'Routed to queue EPI Lost to Follow-up letter sent.%'
  and not exists (select null from notes n1 where n1.event_id=n0.event_id and n1.note like 'Routed to queue%' and n1.created_at>n0.created_at)
  and exists (select null from events e join event_queues q on e.event_queue_id=q.id where e.id=n0.event_id and q.queue_name='EPI Lost to Follow-up letter sent')
  and date_part('day',now()::timestamp-n0.created_at)>=10
  loop
    Update events
      set workflow_state='approved_by_lhd', lhd_case_status_id=external_codes.id
    from external_codes 
    where external_codes.code_name='case' and external_codes.code_description='Lost to follow-up'
      and events.id=_event_id;
    insert into notes (note, struckthrough, user_id, created_at, updated_at, event_id, note_type)
               values ('Automatically approved as Lost to follow-up',FALSE,_admin_id,now()::timestamp,now()::timestamp,_event_id,'administrative');
  end loop;
END;
$BODY$
LANGUAGE plpgsql;
