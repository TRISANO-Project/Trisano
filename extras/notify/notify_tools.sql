﻿create schema notify_tools authorization trisano_rails;

create table notify_tools.case_assignment_email (
  id serial primary key,
  case_id integer,
  investigator_id integer,
  notice_sent boolean);

create table notify_tools.critical_diseases (
  disease_id integer primary key,
  disease_name varchar(100));

create or replace function case_assignment_email_loader()
returns trigger as $$
begin
  insert into notify_tools.case_assignment_email (
    case_id,
    investigator_id,
    notice_sent)
  values
    (NEW.id, NEW.investigator_id, FALSE);
  return NEW;
end;
$$ language plpgsql;

create trigger case_assignment_trigger
after update on events
for each row
when (OLD.investigator_id is distinct from NEW.investigator_id and NEW.investigator_id is not null)
execute procedure case_assignment_email_loader();
