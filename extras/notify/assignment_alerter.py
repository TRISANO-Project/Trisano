#!/usr/bin/python

import smtplib
import daemon
import psycopg2
import time
import signal
import lockfile
import logging
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.mime.text import MIMEText

WAITASEC = 300
SENDER = 'do_not_reply@trisano.tarrantcounty.com'
#SMTPADDR = 'smtp.tarrantcounty.com'
SMTPADDR = 'aspmx.l.google.com'
SMTPPORT = '25'
PGHOST = 'localhost'
PGDB = 'trisano_development'
PGUSER = 'trisano_rails'
PGPWD = 'trisano_rails'
WORKING = '/home/bzambarano/'
LOCKFILE = WORKING + 'assignment_alerter.pid'
USERCONTXT=1001

logger = logging.getLogger()
logger.setLevel(logging.INFO)
fh = logging.FileHandler("./assignment_alerter.log")
logger.addHandler(fh)

def pgconn():
    return psycopg2.connect(host=PGHOST, dbname=PGDB, user=PGUSER, password=PGPWD)
    
def get_reclist(pgsqlstr):
    db = pgconn()
    cur = db.cursor()
    cur.execute(pgsqlstr)
    pgreclist=cur.fetchall()
    cur.close()
    db.close()
    return pgreclist

def watch_case_assignment():
    while True:
        try:
            pgsqlstr = "SELECT case_id, investigator_id, id FROM notify_tools.case_assignment_email where notice_sent=FALSE" 
            emailreclist = get_reclist(pgsqlstr)
            for emailrec in emailreclist:
                pgsqlstr = "select ea.email_address, u.first_name, u.last_name, d.disease_name from email_addresses ea join users u on u.id=ea.owner_id join events e on u.id=e.investigator_id join disease_events de on de.event_id=e.id join notify_tools.critical_diseases d on d.disease_id=de.disease_id where ea.owner_type='User' and e.id={} and e.investigator_id={}".format(emailrec[0],emailrec[1])
                maildetails = get_reclist(pgsqlstr)
                if not maildetails:
                    db = pgconn()
                    cur = db.cursor()
                    pgsqlstr = "delete from notify_tools.case_assignment_email where id = {}".format(emailrec[2])
                    cur.execute(pgsqlstr)
                    db.commit()
                    cur.close()
                    db.close()
                else:
                    maildetailsrec=maildetails[0]
                    receiver = maildetailsrec[0]
                    subject = 'URGENT: You were assigned a {} case in TriSano'.format(maildetailsrec[3]) 
                    msgbody = MIMEMultipart('alternative')
                    msgbody.attach(MIMEText('{} {}, you were assigned a {} case in TriSano.  Please attend to this ASAP.'.format(maildetailsrec[1],maildetailsrec[2],maildetailsrec[3]), 'plain'))
                    msg = MIMEMultipart()
                    msg['Subject'] = subject
                    msg['From'] = SENDER
                    msg['To'] = receiver
                    msg.attach(msgbody)
                    smtpServ = smtplib.SMTP(SMTPADDR,SMTPPORT)
                    smtpServ.sendmail(msg['From'], msg['To'], msg.as_string())
                    logger.info('alert sent to ' + receiver)
                    db = pgconn()
                    cur = db.cursor()
                    pgsqlstr = "update notify_tools.case_assignment_email set notice_sent=TRUE where id={}".format(emailrec[2])
                    cur.execute(pgsqlstr)
                    db.commit()
                    cur.close()
                    db.close()
        except Exception, e:
            logger.error( "Something is broken: " + str(e) )
            break
        time.sleep(WAITASEC)
    sys.exit(0)


def run():
    with daemon.DaemonContext( pidfile=lockfile.FileLock(LOCKFILE),
                               files_preserve=[ fh.stream, ],
                               working_directory=WORKING,
                               stdout=fh.stream,
                               stderr=fh.stream,
                               uid=USERCONTXT ) :
        watch_case_assignment()
        
if __name__== "__main__":
    run()

