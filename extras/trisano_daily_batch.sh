#!/bin/bash
TRIDB=trisano_production
TRIUSR=trisano_rails
LOGFILE=~/trisano_daily_batch.log
#The rest of the file doesn't need to be modified
#*****************************************************
DATE=$(date +%Y-%m-%d)
exec 5>&1 6>&2 >>$LOGFILE.$DATE 2>&1
  psql -d $TRIDB -U $TRIUSR -w -c "select del_events()"
  psql -d $TRIDB -U $TRIUSR -w -c "select updt_ltf()"
exec 1>&5 2>&6
